package com.martjinni.userApp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class accountPage extends AppCompatActivity {
    volleyCall newClass = new volleyCall ();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_account_page);
        SharedPreferences sharedPreferences2 =  getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        try{
            if(sharedPreferences2.getString ("sessionInfo","").equals ("")){
                LinearLayout loginSection = findViewById (R.id.nosession);
                LinearLayout userDetails = findViewById (R.id.userDetails);
                CardView loggedMenu = findViewById (R.id.loggedMenu);
                loginSection.setVisibility (View.VISIBLE);
                userDetails.setVisibility (View.GONE);
                loggedMenu.setVisibility (View.GONE);
            }
            else{
                JSONObject sessionInfo = new JSONObject (sharedPreferences2.getString ("sessionInfo",""));
                TextView userName = findViewById (R.id.userName);
                userName.setText (sessionInfo.getString ("userName"));
                userName = findViewById (R.id.phoneNumber);
                userName.setText (sessionInfo.getString ("phoneNumber"));
                userName = findViewById (R.id.emailId);
                userName.setText (sessionInfo.getString ("emailId"));
                LinearLayout loginSection = findViewById (R.id.nosession);
                LinearLayout userDetails = findViewById (R.id.userDetails);
                CardView loggedMenu = findViewById (R.id.loggedMenu);
                loginSection.setVisibility (View.GONE);
                userDetails.setVisibility (View.VISIBLE);
                loggedMenu.setVisibility (View.VISIBLE);
            }
        }
        catch (JSONException jsonException){
            jsonException.printStackTrace ();
        }
        TextView shareapptext = findViewById(R.id.shareapptext);
        shareapptext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("text/plain");
                    i.putExtra(Intent.EXTRA_SUBJECT, "My application name");
                    String sAux = "Find all your local shops and service online\n";
                    sAux = sAux + "https://play.google.com/store/apps/details?id="+appPackageName;
                    i.putExtra(Intent.EXTRA_TEXT, sAux);
                    startActivity(Intent.createChooser(i, "choose one"));
                } catch(Exception e) {
                    //e.toString();
                }
            }
        });
        TextView rateapp = findViewById(R.id.rateapp);
        rateapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });
        ImageView backButton = findViewById (R.id.backButton);
        final Context mCtx = this;
        backButton.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mCtx, homePage.class);
                startActivity(intent);
                finish ();
            }
        });
        TextView loginOption = findViewById (R.id.loginAccount);
        loginOption.setOnClickListener (new View.OnClickListener (){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(mCtx, loginSignup.class);
                intent.putExtra("nextPage","account");
                startActivity(intent);
                finish ();
            }
        });
        loginOption = findViewById (R.id.signUpNew);
        loginOption.setOnClickListener (new View.OnClickListener (){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(mCtx, loginSignup.class);
                intent.putExtra("nextPage","account");
                startActivity(intent);
                finish ();
            }
        });
        TextView logOut = findViewById (R.id.logout);
        logOut.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder mBUilder = new AlertDialog.Builder(mCtx);
                View mView = ((Activity) mCtx).getLayoutInflater().inflate(R.layout.alert_dialog2, null);
                mBUilder.setView(mView);
                final AlertDialog dialog = mBUilder.create();
                dialog.show ();
                TextView msg = mView.findViewById(R.id.errormessage);
                msg.setText("Do you want to Logout?");
                TextView yes = mView.findViewById(R.id.modalContinue);
                TextView no = mView.findViewById(R.id.modalclose);
                no.setText ("Logout");
                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss ();
                    }
                });
                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        logout();
                        dialog.dismiss ();
                    }
                });
            }
        });
        TextView manageAddress = findViewById (R.id.manageAddress);
        manageAddress.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mCtx, manageAddress.class);
                intent.putExtra("nextPage","account");
                startActivity(intent);
                finish ();
            }
        });
        TextView orderList = findViewById (R.id.orderList);
        orderList.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mCtx, orderList.class);
                startActivity(intent);
                finish ();
            }
        });
        TextView subsList = findViewById (R.id.subsList);
        subsList.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mCtx, subsList.class);
                startActivity(intent);
                finish ();
            }
        });
        TextView changepass = findViewById(R.id.changepassword);
        changepass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), changepassword.class);
                intent.putExtra("nextPage","account");
                startActivity(intent);
                finish ();
            }
        });
        TextView contactbtn = findViewById(R.id.contactbtn);
        contactbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), contactus.class);
                startActivity(intent);
                finish ();

            }
        });
        TextView tandc = findViewById(R.id.tandc);
        final TextView policybtn = findViewById(R.id.policybtn);
        tandc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(accountPage.this, terms.class);
                startActivity(intent);
                finish ();

            }

        });

        policybtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(accountPage.this, privacy.class);
                startActivity(intent);
                finish ();
            }
        });
    }

    @Override
    public void onBackPressed(){
        Intent intent = new Intent(this, homePage.class);
        startActivity(intent);
        finish ();
    }

    public void logout(){
        disableNotif ();

    }

    public void logoutFinalSystem(){
        SharedPreferences sharedPreferences2 =  getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        try{
            final JSONObject sessionInfo = new JSONObject (sharedPreferences2.getString ("sessionInfo",""));
            JSONObject request = new JSONObject ();
            request.put ("userId", sessionInfo.getInt ("userId"));
            request.put("userType",sessionInfo.getString ("userType"));
            Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
            Network network = new BasicNetwork (new HurlStack ());
            RequestQueue mRequestQueue = new RequestQueue (cache, network);
            mRequestQueue.start();
            String finalUrl = newClass.baseUrl+"signout";
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, finalUrl, request,  new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    clearView();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace ();
                    clearView ();
                }
            }){
                @Override
                public Map getHeaders() throws AuthFailureError {
                    Map<String,String> params = new HashMap<>();
                    params.put("Content-Type", "application/json");
                    try{
                        params.put("basicAuthenticate", sessionInfo.getString ("basicAuthenticate"));
                    }
                    catch (JSONException jsonExcepion2){
                        jsonExcepion2.printStackTrace ();
                    }
                    return params;
                }
            };
            mRequestQueue.add(jsonObjectRequest);

        }
        catch (JSONException jsonException){
            jsonException.printStackTrace ();
            clearView ();
        }
    }

    public void clearView(){
        SharedPreferences sharedPreferences2 =  getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        sharedPreferences2.edit ().remove ("sessionInfo").apply ();
        LinearLayout loginSection = findViewById (R.id.nosession);
        LinearLayout userDetails = findViewById (R.id.userDetails);
        CardView loggedMenu = findViewById (R.id.loggedMenu);
        loginSection.setVisibility (View.VISIBLE);
        userDetails.setVisibility (View.GONE);
        loggedMenu.setVisibility (View.GONE);
    }

    public void disableNotif(){
        try{
            SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
            if(!sharedPreferences.getString ("sessionInfo","").equals ("")){
                final JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
                JSONObject requestObject = new JSONObject ();
                String token = sharedPreferences.getString ("userMessageToken","");
                if(!token.equals ("")){
                    requestObject.put ("userType",sessionInfo.getString ("userType"));
                    requestObject.put ("typeId",sessionInfo.getInt ("typeId"));
                    requestObject.put ("userId",sessionInfo.getInt ("userId"));
                    requestObject.put ("token",token);
                    Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                    Network network = new BasicNetwork (new HurlStack ());
                    RequestQueue mRequestQueue = new RequestQueue (cache, network);
                    mRequestQueue.start();
                    String finalUrl = newClass.baseUrl+"remusertoken";
                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest (Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            logoutFinalSystem();
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            logoutFinalSystem();
                        }
                    }){
                        @Override
                        public Map getHeaders() throws AuthFailureError {
                            Map<String,String> params = new HashMap<> ();
                            params.put("Content-Type", "application/json");
                            try{
                                params.put("basicAuthenticate", sessionInfo.getString ("basicAuthenticate"));
                            }
                            catch (JSONException jsonExcepion2){
                                logoutFinalSystem();
                            }
                            return params;
                        }
                    };
                    mRequestQueue.add(jsonObjectRequest);
                }
            }
            else{
                logoutFinalSystem();
            }
        }
        catch (JSONException jsonException){
            logoutFinalSystem();
            jsonException.printStackTrace ();
        }
    }

    public void callUs(View view){
        Log.i("WOW","WOW");
        String telUse = "tel:9811706885";
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse(telUse));
        startActivity(intent);
        finish ();
    }
}
