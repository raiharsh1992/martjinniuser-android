package com.martjinni.userApp;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.List;

public class addOnListAdapter extends RecyclerView.Adapter<addOnListAdapter.addOnListViewHolder>{

    private Context mCtx;
    private List<addOnList> useList;
    private View mView;

    public addOnListAdapter(Context mCtx, List<addOnList> useList, View mView) {
        this.mCtx = mCtx;
        this.useList = useList;
        this.mView = mView;
    }

    @NonNull
    @Override
    public addOnListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from (mCtx);
        View view = inflater.inflate (R.layout.listaddon,null);
        return new addOnListViewHolder (view);
    }

    @Override
    public void onBindViewHolder(@NonNull addOnListViewHolder holder, int position) {
        addOnList container = useList.get (position);
        String displayName = container.getItemNameAddOn ()+"@ Rs"+container.getPrice ();
        holder.addOnName.setText (displayName);
        String singleTag = "addOn"+container.getItemId ();
        String multiTag = "multiAdd"+container.getItemId ();
        if(container.getIsMulti ()==0){
           holder.addOnSingle.setVisibility (View.VISIBLE);
           holder.addOnMulti.setVisibility (View.GONE);
        }
        holder.addOnSingle.setTag (singleTag);
        holder.addOnMulti.setTag (multiTag);
    }

    @Override
    public int getItemCount() {
        return useList.size ();
    }

    class addOnListViewHolder extends RecyclerView.ViewHolder{
        TextView addOnName;
        CheckBox addOnMulti;
        RadioButton addOnSingle;
        public addOnListViewHolder(final View itemView){
            super(itemView);
            addOnName = itemView.findViewById (R.id.addOnName);
            addOnMulti = itemView.findViewById (R.id.addOnMulti);
            addOnSingle = itemView.findViewById (R.id.addOnSingle);
            addOnSingle.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition ();
                    singleAddOn(position);
                }
            });
            addOnMulti.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition ();
                    multiAddOn(position);
                }
            });
        }
    }

    public void singleAddOn(int position){
        addOnList container = useList.get (position);
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        String checkName = "groupAdd"+String.valueOf (container.getGroupId ());
        if(sharedPreferences.getString (checkName,"").equals ("")){
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString (checkName,String.valueOf (container.getItemId ())).apply ();
        }
        else{
            int prevSelected = Integer.parseInt (sharedPreferences.getString (checkName,""));
            String singleTag = "addOn"+container.getItemId ();
            if(prevSelected==container.getItemId ()){
                RadioButton changeView = mView.findViewWithTag (singleTag);
                changeView.setChecked (false);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString (checkName,"").apply ();
            }
            else{
                singleTag = "addOn"+prevSelected;
                RadioButton changeView = mView.findViewWithTag (singleTag);
                changeView.setChecked (false);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString (checkName,String.valueOf (container.getItemId ())).apply ();
            }
        }
    }

    public void multiAddOn(int position){
        addOnList container = useList.get (position);
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        String checkName = "groupAdd"+String.valueOf (container.getGroupId ());
        if(sharedPreferences.getString (checkName,"").equals ("")){
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString (checkName,String.valueOf (container.getItemId ())).apply ();
        }
        else{
            String[] selectedList = sharedPreferences.getString (checkName,"").split (",");
            String insertValue = "";
            int alreadyAdded = 0;
            int first = 0;
            for(int i =0;i<selectedList.length;i++){
                if(Integer.parseInt (selectedList[i])==container.getItemId ()){
                    alreadyAdded = 1;
                }
                else if(first==0){
                    insertValue = selectedList[i];
                    first = 1;
                }
                else{
                    insertValue = insertValue+","+selectedList[i];
                }
            }
            if(alreadyAdded==0){
                if(first==0){
                    insertValue = String.valueOf (container.getItemId ());
                }
                else{
                    insertValue = insertValue+","+ String.valueOf (container.getItemId ());
                }
            }
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString (checkName,insertValue).apply ();
        }
    }
}
