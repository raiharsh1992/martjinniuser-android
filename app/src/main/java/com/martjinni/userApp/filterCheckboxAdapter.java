package com.martjinni.userApp;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;


import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class filterCheckboxAdapter extends RecyclerView.Adapter<filterCheckboxAdapter.filterCheckboxViewHolder>{

    private Context mCtx;
    private List<filterCheckBox> checkList;

    public filterCheckboxAdapter(Context mCtx, List<filterCheckBox> checkList) {
        this.mCtx = mCtx;
        this.checkList = checkList;
    }

    @NonNull
    @Override
    public filterCheckboxViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from (mCtx);
        View view = inflater.inflate (R.layout.button_checkbox,null);
        return new filterCheckboxViewHolder (view);
    }

    @Override
    public void onBindViewHolder(@NonNull filterCheckboxViewHolder holder, int position) {
        filterCheckBox checkBoxVal = checkList.get(position);
        holder.subCatNameUse.setText (checkBoxVal.getSubCat ());
        holder.catId.setText (checkBoxVal.getCatId ());
    }

    @Override
    public int getItemCount() {
        return checkList.size ();
    }

    class filterCheckboxViewHolder extends RecyclerView.ViewHolder{
        CheckBox subCatNameUse;
        TextView catId;

        public filterCheckboxViewHolder(View itemView){
            super(itemView);
            subCatNameUse = itemView.findViewById (R.id.checkbox_cheese2);
            catId = itemView.findViewById (R.id.catId);
            subCatNameUse.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    TextView catUse = v.findViewById (R.id.checkbox_cheese2);
                    SharedPreferences sharedPreferences = mCtx.getSharedPreferences("userInfo", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit ();
                    String selFilter = sharedPreferences.getString ("selctedFilter","");
                    String value = (String) catUse.getText ();
                    SQLiteDatabase myDatabase = mCtx.openOrCreateDatabase("martjinni",MODE_PRIVATE,null);
                    Cursor resultSet = myDatabase.rawQuery ("SELECT * from catList where catName='"+value+"'",null);
                    resultSet.moveToFirst ();
                    int catIdUse=resultSet.getInt (0);
                    if(selFilter.equals ("")){
                        selFilter = String.valueOf (catIdUse);
                    }
                    else{
                        selFilter = selFilter+","+String.valueOf (catIdUse);
                    }
                    editor.putString ("selctedFilter",selFilter).apply ();
                }
            });
        }
    }
}
