package com.martjinni.userApp;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class defaultAddress extends AppCompatActivity{

    ProgressBar progressBar4;
    private RequestQueue mRequestQueue;
    String returnPage = "";
    volleyCall newClass = new volleyCall();
    LocationManager lm;
    boolean gps_enabled = false;
    boolean network_enabled = false;
    public volatile boolean isWorking = true;
    public volatile boolean isRevGeoCoded = true;
    private static final String TAG = "MainActivity";// Will show the string "data" that holds the results

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.defaultaddress_layout);
        final EditText mEditInit = findViewById(R.id.search_box);
        progressBar4 = findViewById (R.id.progressBar4);
        mEditInit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSearch();
            }

        });
        ImageButton getLocationButton = findViewById(R.id.search_text);
        getLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                workWithDetect();
            }
        });
        TextView goToHome = findViewById(R.id.goToHome);
        goToHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                success();
            }
        });
        returnPage = (getIntent().getStringExtra("nextPage"));

    }

    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;

    public final LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(final Location location) {
            if (location != null) {
                double longitude = location.getLongitude ();
                double latitude = location.getLatitude ();
                if(isWorking){
                    isWorking = false;
                    revGeoCoding (latitude, longitude);
                }
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            try {
                if (ActivityCompat.checkSelfPermission(defaultAddress.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(defaultAddress.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(defaultAddress.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                    ActivityCompat.requestPermissions(defaultAddress.this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
                }
                else{
                    progressBar4.setVisibility (View.VISIBLE);
                    lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0,
                            0, mLocationListener);
                    lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0,
                            0, mLocationListener);
                }
            }
            catch (Exception e) {
                Log.e("My e", "Exceptiomn", e);
            }
        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }

    };

    public void onSearch() {
        try {
            AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                    .setCountry("IN")
                    .build();
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .setFilter(typeFilter)
                            .build(this);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException e) {
            // TODO: Handle the error.
        } catch (GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                Double lat = place.getLatLng().latitude;
                Double lng = place.getLatLng().longitude;
                revGeoCoding(lat, lng);
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.i(TAG, status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
                Log.i(TAG, "HELLO");
            }
        }
    }

    public void workWithDetect() {
        lm = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        try{
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        }
        catch (NullPointerException nullException){

        }
        if (!(gps_enabled||network_enabled)) {
            Toast.makeText(getApplicationContext(), "Enable GPS to access this service", Toast.LENGTH_LONG).show();
            Intent callGPSSettingIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(callGPSSettingIntent);
        }
        else {
            try {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
                }
                else{
                    progressBar4.setVisibility (View.VISIBLE);
                    lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0,
                            0, mLocationListener);
                    lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0,
                            0, mLocationListener);
                }
            }
            catch (Exception e) {
                Log.e("My e", "Exceptiomn", e);
            }
        }
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
                    }
                    else {
                        progressBar4.setVisibility (View.VISIBLE);
                        lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1,
                                0, mLocationListener);
                        lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0,
                                0, mLocationListener);
                    }

                } else{
                    Toast.makeText(this,"Permission Denied",Toast.LENGTH_LONG).show();
                }
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    public void revGeoCoding(Double lat, Double lng){
        progressBar4.setVisibility (View.VISIBLE);
        Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
        Network network = new BasicNetwork (new HurlStack ());
        mRequestQueue = new RequestQueue (cache, network);
        mRequestQueue.start();
        final String userPassed = ""+lat+","+""+lng;
        String url = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+userPassed+"&key=AIzaSyAQF1IHXvQpG4nEP1wQA1n-9eHcbnk3w3w";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        final JSONObject storeInformation = new JSONObject();
                        try{
                            JSONObject reader = response.getJSONArray ("results").getJSONObject (1);
                            JSONArray addComp = reader.getJSONArray ("address_components");
                            String addressName = "";
                            String pincode = "";
                            for(int i=0;i<addComp.length ();i++){
                                JSONObject interMediate = addComp.getJSONObject (i);
                                String dummy = interMediate.getString("types");
                                Log.i ("WOW",interMediate.getString ("types"));
                                if(dummy.contains("locality")){
                                    storeInformation.put("city",interMediate.getString ("long_name"));
                                }
                                else if(dummy.contains("country")){
                                    storeInformation.put("country",interMediate.getString ("long_name"));
                                }
                                else if(dummy.contains ("administrative_area_level_1")){
                                    storeInformation.put("state",interMediate.getString ("long_name"));
                                }
                                else if(dummy.contains ("postal_code")){
                                    pincode = interMediate.getString ("long_name");
                                    storeInformation.put("pincode",interMediate.getString ("long_name"));
                                }
                                else if(dummy.contains ("sublocality_level_1")){
                                    addressName = interMediate.getString ("long_name");
                                }
                            }
                            if(pincode.equals ("")){
                                storeInformation.put("pincode","000000");
                            }
                            if(addressName.equals ("")){
                                addressName = reader.getString ("formatted_address");
                            }
                            finalHomeVisit (userPassed,storeInformation.toString (),addressName);
                            final EditText mEditInit = findViewById(R.id.search_box);
                            mEditInit.setHint (addressName);
                            progressBar4.setVisibility (View.GONE);
                            isWorking = true;
                            if(lm!=null){
                                lm.removeUpdates(mLocationListener);
                                lm = null;
                            }
                        }
                        catch (JSONException e){
                            e.printStackTrace ();
                            progressBar4.setVisibility (View.GONE);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        progressBar4.setVisibility (View.GONE);

                    }
                });
        mRequestQueue.add(jsonObjectRequest);
        progressBar4.setVisibility (View.GONE);
    }

    public void finalHomeVisit(final String delVal,final String addInfo,final String addName){
        final SharedPreferences sharedPreferences =  getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        if(!delVal.equals ("")){
            Cursor shopList = getShopList();
            JSONArray shopListUse = new JSONArray ();
            String [] userInfo = delVal.split (",");
            try{
                if(shopList.getCount ()>0){
                    shopList.moveToFirst ();
                    while (!shopList.isAfterLast ()){
                        shopListUse.put (shopList.getInt (0));
                        shopList.moveToNext ();
                    }
                    JSONObject requestObject = new JSONObject ();
                    requestObject.put ("shopList",shopListUse);
                    requestObject.put ("lat",Float.valueOf (userInfo[0]));
                    requestObject.put ("lng",Float.valueOf (userInfo[1]));
                    Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                    Network network = new BasicNetwork (new HurlStack ());
                    RequestQueue mRequestQueue = new RequestQueue (cache, network);
                    mRequestQueue.start();
                    String finalUrl = newClass.baseUrl+"validatedelivery";
                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            sharedPreferences.edit ().putString ("delVal",delVal).apply ();
                            sharedPreferences.edit ().putString ("addressName",addName).apply ();
                            sharedPreferences.edit ().putString ("addressInform",addInfo).apply ();
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            alertshow("Address not deliverable, proceeding may clear cart",delVal,addName,addInfo);
                        }
                    });
                    mRequestQueue.add(jsonObjectRequest);
                }
                else{
                    Log.i(TAG,addName);
                    sharedPreferences.edit ().putString ("delVal",delVal).apply ();
                    sharedPreferences.edit ().putString ("addressName",addName).apply ();
                    sharedPreferences.edit ().putString ("addressInform",addInfo).apply ();
                    sharedPreferences.edit ().remove ("addressInformation").apply ();
                    sharedPreferences.edit ().remove ("addId").apply ();
                }
            }
            catch (JSONException jsonExecption){
                jsonExecption.printStackTrace ();
            }
        }
        else{
            Context context = getApplicationContext();
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText (context,"Please pass an address",duration);
            toast.show ();
        }
    }

    @Override
    public void onBackPressed()
    {
        if(returnPage.equals ("home")){
            android.app.AlertDialog.Builder mBuilder = new android.app.AlertDialog.Builder(this);
            View mView = getLayoutInflater().inflate(R.layout.alert_dialog2, null);
            mBuilder.setView(mView);
            final android.app.AlertDialog dialog = mBuilder.create();
            TextView errorMessage = mView.findViewById (R.id.errormessage);
            String displayMessage = "Do you wish to exit?";
            errorMessage.setText (displayMessage);
            TextView continueView = mView.findViewById (R.id.modalclose);
            continueView.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    dialog.dismiss ();
                    exit ();
                }
            });
            TextView cancelView = mView.findViewById (R.id.modalContinue);
            cancelView.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    dialog.dismiss ();
                }
            });
            dialog.show ();
        }
        else{
            Intent intent = new Intent(this, manageAddress.class);
            intent.putExtra("nextPage",returnPage);
            if(lm!=null){
                lm.removeUpdates(mLocationListener);
                lm = null;
            }
            startActivity(intent);
            finish ();
        }
    }

    public void exit(){
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(1);
        super.onBackPressed ();
    }

    public void success(){
        Log.i(TAG,returnPage);
        SharedPreferences sharedPreferences =  getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        String status = sharedPreferences.getString("delVal", "");
        if(!status.equals ("")){
            if(returnPage.equals ("home")){
                Intent intent = new Intent(this, homePage.class);
                if(lm!=null){
                    lm.removeUpdates(mLocationListener);
                    lm = null;
                }
                startActivity(intent);
                finish();
            }
            else{
                Intent intent = new Intent(this, createNewAddress.class);
                if(lm!=null){
                    lm.removeUpdates(mLocationListener);
                    lm = null;
                }
                intent.putExtra("nextPage",returnPage);
                startActivity(intent);
                finish ();
            }
        }
        else{

        }
    }

    public void alertshow(String message,final String delVal,final String addName,final String addInfo ){
        final SharedPreferences sharedPreferences =  getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        final Context mCtx = this;
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);
        View mView = getLayoutInflater().inflate(R.layout.alertdialog, null);
        TextView messageshow = mView.findViewById(R.id.errormessage);
        messageshow.setText(message);
        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        dialog.show();
        TextView continueUse = mView.findViewById (R.id.modalContinue);
        continueUse.setVisibility (View.VISIBLE);
        continueUse.setText ("Continue with address");
        continueUse.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                dialog.dismiss ();
                sharedPreferences.edit ().putString ("delVal",delVal).apply ();
                sharedPreferences.edit ().putString ("addressName",addName).apply ();
                sharedPreferences.edit ().putString ("addressInform",addInfo).apply ();
                mCtx.deleteDatabase ("martjinniItems");
                sharedPreferences.edit ().remove ("totalBill").apply ();
                sharedPreferences.edit ().remove ("addressInformation").apply ();
                sharedPreferences.edit ().remove ("addId").apply ();
            }
        });
        TextView close = mView.findViewById(R.id.modalclose);
        close.setText ("Select new address");
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss ();
            }
        });
    }

    public Cursor getShopList(){
        SQLiteDatabase myDatabase = openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        myDatabase.execSQL ("CREATE TABLE IF NOT EXISTS itemListMaster(itemId INTEGER PRIMARY KEY, itemName VARCHAR, spclCat VARCHAR, gstRate INTEGER,addonInfo VARCHAR, subsInfo VARCHAR, shopId INTEGER, isActive INTEGER, hasImage INTEGER, isCustomize INTEGER, itemCount INTEGER, mainItemId INTEGER, displayPrice INTEGER )");
        Cursor resultSet = myDatabase.rawQuery ("SELECT shopId from itemListMaster where itemCount>'0' group by shopId",null);
        return resultSet;
    }

}

