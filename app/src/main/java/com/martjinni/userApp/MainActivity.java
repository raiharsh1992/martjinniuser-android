package com.martjinni.userApp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        setContentView(R.layout.activity_main);

        super.onCreate(savedInstanceState);



        SharedPreferences sharedPreferences =  getSharedPreferences("userInfo", Context.MODE_PRIVATE);

        //int stat = Integer.parseInt(status);
        if(sharedPreferences.contains ("delVal")){
            Intent intent = new Intent(this, homePage.class);
            startActivity(intent);
            finish();
        }
        else {
            Intent intent = new Intent(this, defaultAddress.class);
            intent.putExtra("nextPage","home");
            startActivity(intent);
            finish();
        }

    }
}

