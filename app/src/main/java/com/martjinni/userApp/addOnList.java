package com.martjinni.userApp;

public class addOnList {
    private  String itemName;
    private int isMulti, price, itemId, groupId;

    public addOnList(String itemName, int isMulti, int price, int itemId, int groupId) {
        this.itemName = itemName;
        this.isMulti = isMulti;
        this.price = price;
        this.itemId = itemId;
        this.groupId = groupId;
    }

    public int getItemId() {
        return itemId;
    }

    public int getGroupId() {
        return groupId;
    }

    public String getItemNameAddOn() {
        return itemName;
    }

    public int getIsMulti() {
        return isMulti;
    }

    public int getPrice() {
        return price;
    }
}
