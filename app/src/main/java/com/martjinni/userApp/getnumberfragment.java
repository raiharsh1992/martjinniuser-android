package com.martjinni.userApp;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * A simple {@link Fragment} subclass.
 */
public class getnumberfragment extends Fragment {

    TextView fragmentaction;
    TextView forgotpass;
    public RequestQueue mRequestQueue;
    volleyCall newClass = new volleyCall ();
    public String baseUrl = newClass.baseUrl;

    public getnumberfragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        final View view = inflater.inflate(R.layout.fragment_getnumberfragment, container, false);
        ((loginSignup)getActivity()).changeHead("My Account");
        forgotpass = (TextView) view.findViewById(R.id.forgotpass);
        forgotpass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((loginSignup)getActivity()).restorepass();
            }
        });
        fragmentaction = (TextView) view.findViewById(R.id.sessionactions);
        fragmentaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               final EditText number = view.findViewById(R.id.mobnumber);
               final String mobnumber = number.getText().toString();
                if(mobnumber.length()==10){
                    Log.d("STRING lENGTH",Integer.toString(mobnumber.length()));
                  try{
                        Cache cache = new DiskBasedCache(getContext().getCacheDir(), 1024 * 1024);
                        Network network = new BasicNetwork(new HurlStack());
                        mRequestQueue = new RequestQueue(cache, network);
                        mRequestQueue.start();
                        JSONObject jsonVal = new JSONObject ();
                        jsonVal.put ("phoneNumber", Double.parseDouble(mobnumber));
                        jsonVal.put("userType", "CUST");
                        String userUrl = baseUrl+"validphone";
                        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, userUrl, jsonVal,  new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                Log.d("responseofvalidphone", response.toString());
                                number.clearFocus();
                                ((loginSignup)getActivity()).checknumber(mobnumber, "new");

                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                number.clearFocus();
                                ((loginSignup)getActivity()).checknumber(mobnumber, "exist");

                                error.printStackTrace ();
                            }
                        });
                        mRequestQueue.add(jsonObjectRequest);
                    }
                    catch (JSONException jsonException){
                        jsonException.printStackTrace ();
                    }
                }
                else{
                    Log.d("STRING lENGTH",Integer.toString(mobnumber.length()));
                    Toast.makeText(getContext(),"Enter a valid Number of 10 Digit ", Toast.LENGTH_LONG).show();
                }
            }
        });
        return view;
    }



    public TextView getFragmentaction() {
        return fragmentaction;
    }
}
