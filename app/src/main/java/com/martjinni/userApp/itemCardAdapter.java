package com.martjinni.userApp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import java.util.List;

public class itemCardAdapter extends RecyclerView.Adapter<itemCardAdapter.itemCardViewHolder>{

    private Context mCtx;
    private List<itemCardClass> itemList;

    public itemCardAdapter(Context mCtx, List<itemCardClass> itemList) {
        this.mCtx = mCtx;
        this.itemList = itemList;
    }

    @NonNull
    @Override
    public itemCardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from (mCtx);
        View view = inflater.inflate (R.layout.item_card,null);
        return new itemCardViewHolder (view);
    }

    @Override
    public void onBindViewHolder(@NonNull itemCardViewHolder holder, int position) {
        itemCardClass itemInfo = itemList.get(position);
        holder.itemName.setText (itemInfo.getItemName ());
        holder.itemPrice.setText (itemInfo.getDisplayPrice ());
        if(itemInfo.getCustomIf ()>0){
            holder.customIf.setVisibility (View.VISIBLE);
        }
        else{
            holder.customIf.setVisibility (View.INVISIBLE);
        }
        if(itemInfo.getVegNon ().equals ("veg")){
            holder.vegNon.setVisibility (View.VISIBLE);
        }
        else if(itemInfo.getVegNon ().equals ("nonveg")){
            holder.vegNon.setImageResource (R.mipmap.ic_nonveg);
            holder.vegNon.setVisibility (View.VISIBLE);
        }
        String plusButTag = "plusBut"+itemInfo.getSubItemId ();
        String visTag = "vis"+itemInfo.getSubItemId ();
        String itemCount = "itemCOunt"+itemInfo.getSubItemId ();
        holder.plusMinusLayout.setTag (plusButTag);
        holder.visibleAdd.setTag (visTag);
        holder.itemCount.setText (String.valueOf (itemInfo.getItemCount ()));
        holder.itemCount.setTag (itemCount);
        if(itemInfo.getItemCount ()>0){
            holder.visibleAdd.setVisibility (View.GONE);
            holder.plusMinusLayout.setVisibility (View.VISIBLE);
        }
        else{
            holder.visibleAdd.setVisibility (View.VISIBLE);
            holder.plusMinusLayout.setVisibility (View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return itemList.size ();
    }

    class itemCardViewHolder extends RecyclerView.ViewHolder{
        ImageView vegNon,minusClick,plusClick;
        TextView itemName,itemPrice,visibleAdd,itemCount,customIf;
        LinearLayout plusMinusLayout;
        public itemCardViewHolder(final View itemView){
            super(itemView);
            vegNon = itemView.findViewById (R.id.vegNon);
            minusClick = itemView.findViewById (R.id.minusClick);
            plusClick = itemView.findViewById (R.id.plusClick);
            itemName = itemView.findViewById (R.id.itemName);
            itemPrice = itemView.findViewById (R.id.itemPrice);
            visibleAdd = itemView.findViewById (R.id.visibleAdd);
            itemCount = itemView.findViewById (R.id.itemCount);
            customIf = itemView.findViewById (R.id.customIf);
            plusMinusLayout = itemView.findViewById (R.id.plusMinusLayout);
            plusClick.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition ();
                    itemCardClass container = itemList.get (position);
                    ((itemsPage)mCtx).addExistingOrder (container.getSubItemId ());
                }
            });
            minusClick.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition ();
                    itemCardClass container = itemList.get (position);
                    ((itemsPage)mCtx).deductExistingOrder (container.getSubItemId ());
                }
            });
            visibleAdd.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition ();
                    itemCardClass container = itemList.get (position);
                    ((itemsPage)mCtx).workWithItemPlus (container.getSubItemId ());
                }
            });
        }
    }
}
