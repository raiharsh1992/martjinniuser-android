package com.martjinni.userApp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

public class orderSuccess extends AppCompatActivity {
    int orderId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_order_success);
        orderId = Integer.parseInt (getIntent ().getStringExtra ("orderId"));
    }

    public void goToHome(View view){
        Intent intent = new Intent(this, homePage.class);
        startActivity(intent);
        finish ();
    }

    public void goToOrders(View view){
        Intent intent = new Intent(this, orderDetails.class);
        intent.putExtra("orderId",String.valueOf (orderId));
        intent.putExtra("nextPage","home");
        startActivity(intent);
        finish ();
    }

    @Override
    public void onBackPressed()
    {
        ViewGroup view = findViewById (R.id.content);
        goToHome (view);
        super.onBackPressed();  // optional depending on your needs
    }

}
