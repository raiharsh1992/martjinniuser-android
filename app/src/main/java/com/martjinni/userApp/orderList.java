package com.martjinni.userApp;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import pl.droidsonroids.gif.GifImageView;
public class orderList extends AppCompatActivity {
    public RequestQueue mRequestQueue;
    volleyCall newClass = new volleyCall ();
    public String baseUrl = newClass.baseUrl;
    LinearLayout noSubs;
    NestedScrollView nestedView;
    List <orederlistconstructor> orderlistval = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_order_list);
        noSubs = findViewById (R.id.noSubs);
        nestedView = findViewById (R.id.subListNew);
        try {
            SharedPreferences sharedPreferences2 = getSharedPreferences("userInfo", Context.MODE_PRIVATE);
            if (sharedPreferences2.getString("sessionInfo", "").equals("")) {
                Intent intent = new Intent(this, accountPage.class);
                startActivity(intent);
                finish ();
            } else {

                Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork(new HurlStack());
                RequestQueue mRequestQueue = new RequestQueue(cache, network);
                mRequestQueue.start();
                final JSONObject sessionInfo = new JSONObject(sharedPreferences2.getString("sessionInfo", ""));
                JSONObject requestObject = new JSONObject();
                requestObject.put("userId", sessionInfo.getInt("userId"));
                requestObject.put("typeId", sessionInfo.getInt("typeId"));
                requestObject.put("userType", sessionInfo.getString("userType"));
                String finalUrl = newClass.baseUrl + "orderlist";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, finalUrl, requestObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        GifImageView gifImageView = (GifImageView) findViewById(R.id.preloader);
                        gifImageView.setVisibility(View.GONE);
                        try {
                            inflateview(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            noSubs.setVisibility (View.VISIBLE);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        GifImageView gifImageView = (GifImageView) findViewById(R.id.preloader);
                        gifImageView.setVisibility(View.GONE);
                        noSubs.setVisibility (View.VISIBLE);
                    }
                }) {
                    @Override
                    public Map getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<>();
                        params.put("Content-Type", "application/json");
                        try {
                            params.put("basicAuthenticate", sessionInfo.getString("basicAuthenticate"));
                        } catch (JSONException jsonExcepion2) {
                            jsonExcepion2.printStackTrace();
                        }
                        return params;
                    }
                };
                mRequestQueue.add(jsonObjectRequest);
            }
        }
        catch (JSONException jsonException){
            jsonException.printStackTrace ();
        }


    }

    private void inflateview(JSONObject response) throws JSONException {
        if(response.getInt("totalCount")>0){
            JSONArray addValues = response.getJSONArray ("Data");
            for(int i=0;i<addValues.length ();i++){
                JSONObject useAddValue = addValues.getJSONObject (i);
                orderlistval.add (new orederlistconstructor (useAddValue.getString ("orderStatus"), useAddValue.getString ("creationDate"),useAddValue.getInt ("orderId"),useAddValue.getInt ("shopCount"),useAddValue.getInt("shopsLeft"),useAddValue.getString ("amount")));
            }
            nestedView.setVisibility (View.VISIBLE);
            orderlistAdapter adapter = new orderlistAdapter (orderlistval,this);
            RecyclerView addListView = findViewById (R.id.orderlistrecycler);
            addListView.setHasFixedSize (false);
            addListView.setLayoutManager (new LinearLayoutManager(this));
            addListView.setAdapter (adapter);
        }
        else{
            noSubs.setVisibility (View.VISIBLE);
        }
    }

    public void goToAccounts(View view){
        Intent intent = new Intent(this, accountPage.class);
        startActivity(intent);
        finish ();
    }

    @Override
    public void onBackPressed(){
        Intent intent = new Intent(this, accountPage.class);
        startActivity(intent);
        finish ();
    }

    public void goToShops(View view){
        Intent intent = new Intent(this, homePage.class);
        startActivity(intent);
        finish();
    }

}
