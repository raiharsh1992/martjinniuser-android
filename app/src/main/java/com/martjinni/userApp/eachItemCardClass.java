package com.martjinni.userApp;

import java.util.List;

public class eachItemCardClass {
    private List<displayHorizontalClass> information;
    private String itemName, unitInfo, susbStartDate;
    private int subsDayCount, subsPrice, perUnitPrice, baseUnits, cartId;

    public eachItemCardClass(List<displayHorizontalClass> information, String itemName, String unitInfo, String susbStartDate, int subsDayCount, int subsPrice, int perUnitPrice, int baseUnits, int cartId) {
        this.information = information;
        this.itemName = itemName;
        this.unitInfo = unitInfo;
        this.susbStartDate = susbStartDate;
        this.subsDayCount = subsDayCount;
        this.subsPrice = subsPrice;
        this.perUnitPrice = perUnitPrice;
        this.baseUnits = baseUnits;
        this.cartId = cartId;
    }

    public List<displayHorizontalClass> getInformation() {
        return information;
    }

    public String getItemName() {
        return itemName;
    }

    public String getUnitInfo() {
        return unitInfo;
    }

    public String getSusbStartDate() {
        return susbStartDate;
    }

    public int getSubsDayCount() {
        return subsDayCount;
    }

    public int getSubsPrice() {
        return subsPrice;
    }

    public int getPerUnitPrice() {
        return perUnitPrice;
    }

    public int getBaseUnits() {
        return baseUnits;
    }

    public int getCartId() {
        return cartId;
    }

    public void setBaseUnits(int baseUnits) {
        this.baseUnits = baseUnits;
    }
}
