package com.martjinni.userApp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class trend_card_view_adapter extends RecyclerView.Adapter<trend_card_view_adapter.trendViewHolder>{

    private Context mCtx;
    private List<trend_card_view> shopList;

    public trend_card_view_adapter(Context mCtx, List<trend_card_view> shopList) {
        this.mCtx = mCtx;
        this.shopList = shopList;
    }

    @NonNull
    @Override
    public trendViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from (mCtx);
        View view = inflater.inflate (R.layout.trend_shop_card,null);
        return new trendViewHolder (view);
    }

    @Override
    public void onBindViewHolder(@NonNull trendViewHolder holder, int position) {
        trend_card_view trend_card = shopList.get(position);
        holder.shop_name.setText (trend_card.getShopNameTrend ());
        holder.sub_text.setText (trend_card.getSubText ());
        holder.rate_bar.setRating (trend_card.getRating ());
        Picasso.get().load(trend_card.getImgUrl ()).into(holder.shopImage);
        holder.shopId.setText (String.valueOf (trend_card.getShopId_use ()));
    }

    @Override
    public int getItemCount() {
        return shopList.size ();
    }

    class trendViewHolder extends RecyclerView.ViewHolder{
        ImageView shopImage;
        TextView shop_name,sub_text,shopId;
        RatingBar rate_bar;

        public trendViewHolder(View itemView){
            super(itemView);
            shopImage = itemView.findViewById (R.id.shop_img);
            shop_name = itemView.findViewById (R.id.shop_name);
            sub_text = itemView.findViewById (R.id.sub_text);
            rate_bar = itemView.findViewById (R.id.ratingBar);
            shopId = itemView.findViewById (R.id.shop_id_use);
            itemView.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition ();
                    trend_card_view container = shopList.get (position);
                    ((homePage)mCtx).gotoItems (container.getShopId_use ());
                }
            });
        }

    }

}
