package com.martjinni.userApp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import java.util.List;

public class diplayOrderDetailsItemAdapter extends RecyclerView.Adapter<diplayOrderDetailsItemAdapter.displayOrderDetailsItemsViewHolder>{

    Context mCtx;
    private List<diplayOrderDetailsItemClass> useItems;

    public diplayOrderDetailsItemAdapter(Context mCtx, List<diplayOrderDetailsItemClass> useItems) {
        this.mCtx = mCtx;
        this.useItems = useItems;
    }

    @NonNull
    @Override
    public displayOrderDetailsItemsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from (mCtx);
        View view = inflater.inflate (R.layout.order_details_items,null);
        return new displayOrderDetailsItemsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull displayOrderDetailsItemsViewHolder holder, int position) {
        diplayOrderDetailsItemClass container = useItems.get (position);
        holder.itemName.setText (container.getItemName ());
        holder.addOnDisplay.setText (container.getAddOn ());
        holder.displayPrice.setText (("\u20b9" + container.getPrice ()));
        if(container.getWithSubs ()==1){
            holder.trackOrder.setVisibility (View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return useItems.size ();
    }

    class displayOrderDetailsItemsViewHolder extends RecyclerView.ViewHolder{
        TextView itemName, addOnDisplay, trackOrder, displayPrice;
        public displayOrderDetailsItemsViewHolder(View itemView){
            super(itemView);
            itemName = itemView.findViewById (R.id.itemName);
            addOnDisplay = itemView.findViewById (R.id.displayAddOn);
            trackOrder = itemView.findViewById (R.id.trackOrderItem);
            displayPrice = itemView.findViewById (R.id.itemPrice);
            trackOrder.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition ();
                    workWithSubs(position);
                }
            });
        }
    }

    public void workWithSubs(int position){
        diplayOrderDetailsItemClass container = useItems.get (position);
        Intent intent = new Intent(mCtx, subsDetails.class);
        intent.putExtra("itemId",container.getItemId ());
        intent.putExtra ("orderId",container.getOrderId ());
        mCtx.startActivity(intent);
        ((Activity)mCtx).finish();
    }
}
