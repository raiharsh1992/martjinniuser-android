package com.martjinni.userApp;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;


import java.util.List;

public class adapter_subsList extends RecyclerView.Adapter<adapter_subsList.MyViewHolder> {
    private List<constructor_subslist> subslistdata;
    public Context mct;



    public adapter_subsList(List<constructor_subslist> subsdetails, Context context) {
        this.subslistdata = subsdetails;
        this.mct = context;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.subslistrecycler, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        constructor_subslist cons = subslistdata.get(position);
        holder.itemname.setText(cons.getItemname());
        holder.shopname.setText(cons.getShopname());
        holder.subsstart.setText(("Starts from: " + cons.getStartDate()));
        holder.noofdays.setText(("No of Days : " + cons.getDayscount()));
        holder.priceofsubs.setText(("Price : " + cons.getPrice()));
        holder.subsstate = cons.getStatus();
        holder.addon.setText(("Addons : " + cons.getAddoname()));
        holder.workingtext.setText((cons.getDelleft() + " Working"));
        holder.delivertext.setText((cons.getDelDone() + " Delivered"));
        holder.progresswork.setProgress(cons.getDelpercent());
        holder.subsid.setTag(cons.getSubsId());
        switch (holder.subsstate) {
            case "NEW":
                holder.progresswork.setIndeterminate(true);
                holder.progressdeliver.setIndeterminate(false);
                holder.progressdeliver.setProgress(0);
                holder.intransit.setImageResource(R.drawable.intransit);
                holder.deliver.setImageResource(R.drawable.delivered);
                break;
            case "WORKING":
                holder.progresswork.setIndeterminate(false);
                holder.progresswork.setProgress(100);
                holder.intransit.setImageResource(R.drawable.intransitcolor);
                if (cons.getDelDone() == 0) {
                    holder.progressdeliver.setIndeterminate(false);
                    holder.progressdeliver.setProgress(0);
                    holder.deliver.setImageResource(R.drawable.delivered);
                } else {
                    holder.progressdeliver.setIndeterminate(false);
                    holder.progressdeliver.setProgress(cons.getDelpercent());
                    if (cons.getDelpercent() == 100) {

                        holder.deliver.setImageResource(R.drawable.deliveredcolor);
                    }
                }
                break;
            case "COMPLETED":
                holder.progresswork.setIndeterminate(false);
                holder.progressdeliver.setIndeterminate(false);
                holder.progresswork.setProgress(100);
                holder.progressdeliver.setProgress(100);
                holder.intransit.setImageResource(R.drawable.intransitcolor);
                holder.deliver.setImageResource(R.drawable.deliveredcolor);
                break;

            case "CANCELED":
                holder.statusbar.setVisibility(View.GONE);
                holder.canceled.setVisibility(View.VISIBLE);


        }
    }

    @Override
    public int getItemCount() {
        return subslistdata.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView shopname;
        TextView itemname;
        TextView subsstart;
        TextView noofdays;
        TextView priceofsubs;
        ProgressBar progresswork;
        ImageView intransit;
        ProgressBar progressdeliver;
        ImageView deliver;
        TextView workingtext;
        TextView delivertext;
        String subsstate;
        TextView subsid;
        TextView addon;
        LinearLayout statusbar;
        TextView canceled;

        public MyViewHolder(View itemView) {
            super(itemView);

            shopname = itemView.findViewById(R.id.shopname);
            itemname = itemView.findViewById(R.id.itemname);
            subsstart = itemView.findViewById(R.id.subsstart);
            noofdays = itemView.findViewById(R.id.noofdays);
            priceofsubs = itemView.findViewById(R.id.priceofsubs);
            progresswork = itemView.findViewById(R.id.progresswork);
            progressdeliver = itemView.findViewById(R.id.progressdeliver);
            intransit = itemView.findViewById(R.id.intransit);
            deliver = itemView.findViewById(R.id.deliver);
            workingtext = itemView.findViewById(R.id.workingtext);
            delivertext = itemView.findViewById(R.id.delivertext);
            subsid = itemView.findViewById(R.id.subsid);
            addon = itemView.findViewById(R.id.addonname);
            statusbar = itemView.findViewById(R.id.statusbar);
            canceled = itemView.findViewById(R.id.canceltext);
            subsid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition ();
                    subsintent(position);
                }
            });
        }
    }
    public void subsintent(int position){
        constructor_subslist container = subslistdata.get (position);
        Intent intent = new Intent(mct, subsDetails.class);
        intent.putExtra("subsId",String.valueOf (container.getSubsId ()));
        intent.putExtra("nextPage","subslist");
        mct.startActivity(intent);
    }
}

