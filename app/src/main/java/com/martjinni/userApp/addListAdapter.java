package com.martjinni.userApp;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class addListAdapter extends RecyclerView.Adapter<addListAdapter.addListViewHolder>{
    volleyCall newClass = new volleyCall ();
    private Context mCtx;
    private List<addListClass> useAdd;
    private int selectedAdd;
    private RelativeLayout scrollView;

    public addListAdapter(Context mCtx, List<addListClass> useAdd, int selectedAdd, RelativeLayout scrollView) {
        this.mCtx = mCtx;
        this.useAdd = useAdd;
        this.selectedAdd = selectedAdd;
        this.scrollView = scrollView;
    }

    @NonNull
    @Override
    public addListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from (mCtx);
        View view = inflater.inflate (R.layout.address_list,null);
        return new addListViewHolder (view);
    }

    @Override
    public void onBindViewHolder(@NonNull addListViewHolder holder, int position) {
        addListClass container = useAdd.get (position);
        String displayAdd1 = container.getAddLine1 ()+" , "+container.getAddLine2 ();
        holder.addLine1.setText (displayAdd1);
        displayAdd1 = container.getCity ()+" , "+container.getState ()+" , "+container.getCountry ();
        holder.addLine2.setText (displayAdd1);
        holder.addName.setText (container.getAddName ());
        String addTag = "checkSelect"+container.getAddId ();
        holder.addSelected.setTag (addTag);
        if(selectedAdd==container.getAddId ()){
            holder.addSelected.setChecked (true);
        }
        else{
            holder.addSelected.setChecked (false);
        }
    }

    @Override
    public int getItemCount() {
        return useAdd.size ();
    }

    class addListViewHolder extends RecyclerView.ViewHolder{
        TextView addName, addLine1, addLine2;
        CheckBox addSelected;
        public addListViewHolder(final View itemView){
            super(itemView);
            addName = itemView.findViewById (R.id.addName);
            addLine1 = itemView.findViewById (R.id.addLine1);
            addLine2 = itemView.findViewById (R.id.addLine2);
            addSelected = itemView.findViewById (R.id.selectAddress);
            itemView.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition ();
                    isDeliverable (position);
                }
            });
            addSelected.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition ();
                    isDeliverable (position);
                }
            });
        }
    }

    public void isDeliverable(final int position){
        try{
            Log.i("WOW",String.valueOf (position));
            addListClass container = useAdd.get (position);
            Cursor shopList = getShopList();
            JSONArray shopListUse = new JSONArray ();
            if(shopList.getCount ()>0){
                shopList.moveToFirst ();
                while (!shopList.isAfterLast ()){
                    shopListUse.put (shopList.getInt (0));
                    shopList.moveToNext ();
                }
                JSONObject requestObject = new JSONObject ();
                requestObject.put ("shopList",shopListUse);
                requestObject.put ("lat",container.getLat ());
                requestObject.put ("lng",container.getLng ());
                Cache cache = new DiskBasedCache (mCtx.getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork (new HurlStack ());
                RequestQueue mRequestQueue = new RequestQueue (cache, network);
                mRequestQueue.start();
                String finalUrl = newClass.baseUrl+"validatedelivery";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        success(position);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        alertshow("Address not deliverable, proceeding will clear cart",position);
                    }
                });
                mRequestQueue.add(jsonObjectRequest);
            }
            else{
                String addTag = "checkSelect"+container.getAddId ();
                CheckBox selectedView = scrollView.findViewWithTag (addTag);
                if(selectedView.isChecked ()){
                    clearSelection(position);
                }
                else{
                    clearAddress(position);
                }
            }
        }
        catch (JSONException jsonException){
            jsonException.printStackTrace ();
        }
    }

    public void success(int position){
        try{
            addListClass container = useAdd.get (position);
            SharedPreferences sharedPreferences = mCtx.getSharedPreferences("userInfo", Context.MODE_PRIVATE);
            JSONObject addressValue = new JSONObject ();
            addressValue.put("city",container.getCity ());
            addressValue.put("state",container.getState ());
            addressValue.put ("country",container.getCountry ());
            addressValue.put ("pincode",container.getPincode ());
            sharedPreferences.edit ().putString ("addressInform",String.valueOf (addressValue)).apply ();
            sharedPreferences.edit ().putInt ("addId",container.getAddId ()).apply ();
            addressValue.put ("addLine1",container.getAddLine1 ());
            addressValue.put("addLine2",container.getAddLine2 ());
            sharedPreferences.edit ().putString ("addressInformation",String.valueOf (addressValue)).apply ();
            String delVal = container.getLat ()+","+container.getLng ();
            sharedPreferences.edit ().putString ("delVal",delVal).apply ();
            sharedPreferences.edit ().putString ("addressName",container.getAddName ()).apply ();
            String addTag = "checkSelect"+container.getAddId ();
            CheckBox selectedView = scrollView.findViewWithTag (addTag);
            selectedView.setChecked (true);
            updateReclyeView (container.getAddId ());
        }
        catch (JSONException jsonException){
            jsonException.printStackTrace ();
        }
    }

    public void alertshow(String message, final int position){
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(mCtx);
        View mView = ((Activity) mCtx).getLayoutInflater().inflate(R.layout.alert_dialog2, null);
        TextView messageshow = mView.findViewById(R.id.errormessage);
        messageshow.setText(message);
        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        dialog.show();
        TextView continueUse = mView.findViewById (R.id.modalContinue);
        continueUse.setVisibility (View.VISIBLE);
        continueUse.setText ("Continue");
        continueUse.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                dialog.dismiss ();
                clearAddress(position);
            }
        });
        TextView close = mView.findViewById(R.id.modalclose);
        close.setText ("Select new");
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss ();
                clearSelection(position);
            }
        });
    }

    public void clearSelection(int position){
        addListClass container = useAdd.get (position);
        String addTag = "checkSelect"+container.getAddId ();
        CheckBox selectedView = scrollView.findViewWithTag (addTag);
        selectedView.setChecked (false);
        updateReclyeView (0);
    }

    public void clearAddress(int position){
        try{
            addListClass container = useAdd.get (position);
            SharedPreferences sharedPreferences = mCtx.getSharedPreferences("userInfo", Context.MODE_PRIVATE);
            JSONObject addressValue = new JSONObject ();
            addressValue.put("city",container.getCity ());
            addressValue.put("state",container.getState ());
            addressValue.put ("country",container.getCountry ());
            addressValue.put ("pincode",container.getPincode ());
            sharedPreferences.edit ().putString ("addressInform",String.valueOf (addressValue)).apply ();
            sharedPreferences.edit ().putInt ("addId",container.getAddId ()).apply ();
            addressValue.put ("addLine1",container.getAddLine1 ());
            addressValue.put("addLine2",container.getAddLine2 ());
            sharedPreferences.edit ().putString ("addressInformation",String.valueOf (addressValue)).apply ();
            String delVal = container.getLat ()+","+container.getLng ();
            sharedPreferences.edit ().putString ("delVal",delVal).apply ();
            sharedPreferences.edit ().putString ("addressName",container.getAddName ()).apply ();
            mCtx.deleteDatabase ("martjinniItems");
            sharedPreferences.edit ().remove ("totalBill").apply ();
            String addTag = "checkSelect"+container.getAddId ();
            CheckBox selectedView = scrollView.findViewWithTag (addTag);
            selectedView.setChecked (true);
            updateReclyeView (container.getAddId ());
        }
        catch (JSONException jsonException){
            jsonException.printStackTrace ();
        }
    }

    public Cursor getShopList(){
        SQLiteDatabase myDatabase = mCtx.openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        myDatabase.execSQL ("CREATE TABLE IF NOT EXISTS itemListMaster(itemId INTEGER PRIMARY KEY, itemName VARCHAR, spclCat VARCHAR, gstRate INTEGER,addonInfo VARCHAR, subsInfo VARCHAR, shopId INTEGER, isActive INTEGER, hasImage INTEGER, isCustomize INTEGER, itemCount INTEGER, mainItemId INTEGER, displayPrice INTEGER )");
        Cursor resultSet = myDatabase.rawQuery ("SELECT shopId from itemListMaster where itemCount>'0' group by shopId",null);
        return resultSet;
    }

    public  void updateReclyeView(int selectedAdd){
        addListAdapter adapter = new addListAdapter (mCtx,useAdd,selectedAdd,scrollView);
        RecyclerView addListView = scrollView.findViewById (R.id.displayAddList);
        addListView.setHasFixedSize (false);
        addListView.setLayoutManager (new LinearLayoutManager (mCtx));
        addListView.setAdapter (adapter);
    }
}
