package com.martjinni.userApp;

import java.util.List;

public class filterRecyler {
    String catNameFilter;
    List<filterCheckBox> subCatList;

    public filterRecyler(String catNameFilter, List<filterCheckBox> subCatList) {
        this.catNameFilter = catNameFilter;
        this.subCatList = subCatList;
    }

    public String getCatNameFilter() {
        return catNameFilter;
    }

    public List<filterCheckBox> getSubCatList() {
        return subCatList;
    }
}
