package com.martjinni.userApp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class manageAddress extends AppCompatActivity {
    private static final String TAG = "MY_ACT";
    volleyCall newClass = new volleyCall ();
    String returnPage = "";
    List<addListClass> userInfoUse = new ArrayList<> ();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_manage_address);
        returnPage = (getIntent ().getStringExtra ("nextPage"));
        SharedPreferences sharedPreferences2 =  getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        if(sharedPreferences2.getString ("sessionInfo","").equals ("")){
            Intent intent = new Intent(this, accountPage.class);
            startActivity(intent);
            finish ();
        }
        else{
            try{
                Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork (new HurlStack ());
                RequestQueue mRequestQueue = new RequestQueue (cache, network);
                mRequestQueue.start();
                final JSONObject sessionInfo = new JSONObject (sharedPreferences2.getString ("sessionInfo",""));
                JSONObject requestObject = new JSONObject ();
                requestObject.put("userId",sessionInfo.getInt ("userId"));
                String finalUrl = newClass.baseUrl+"custaddresslist";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        inflateView(response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace ();
                    }
                }){
                    @Override
                    public Map getHeaders() throws AuthFailureError {
                        Map<String,String> params = new HashMap<> ();
                        params.put("Content-Type", "application/json");
                        try{
                            params.put("basicAuthenticate", sessionInfo.getString ("basicAuthenticate"));
                        }
                        catch (JSONException jsonExcepion2){
                            jsonExcepion2.printStackTrace ();
                        }
                        return params;
                    }
                };
                mRequestQueue.add(jsonObjectRequest);
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }
    }

    public void inflateView(JSONObject response){
        RelativeLayout scrollView = findViewById (R.id.scrollView);
        try{
            if(response.getInt ("addressCount")>0){
                JSONArray addValues = response.getJSONArray ("address");
                for(int i=0;i<addValues.length ();i++){
                    JSONObject useAddValue = addValues.getJSONObject (i);
                    userInfoUse.add (new addListClass (useAddValue.getString ("addName"),useAddValue.getString ("addressLine1"),useAddValue.getString ("addressLine2"),useAddValue.getString ("city"),useAddValue.getString ("state"),useAddValue.getString ("country"),String.valueOf (useAddValue.getInt("pincode")),useAddValue.getInt ("addressId"),useAddValue.getDouble ("addLat"),useAddValue.getDouble ("addLng")));
                }
                SharedPreferences sharedPreferences = getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                addListAdapter adapter = new addListAdapter (this, userInfoUse,sharedPreferences.getInt ("addId",0),scrollView);
                RecyclerView addListView = findViewById (R.id.displayAddList);
                addListView.setHasFixedSize (false);
                addListView.setLayoutManager (new LinearLayoutManager (this));
                addListView.setAdapter (adapter);
            }
            else{
                Intent intent = new Intent(this, createNewAddress.class);
                intent.putExtra("nextPage",returnPage);
                startActivity(intent);
                finish ();
            }
        }
        catch (JSONException jsonException){
            jsonException.printStackTrace ();
        }
    }

    public void goToNext(View v){
        SharedPreferences sharedPreferences = getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        if(returnPage.equals ("account")){
            if(!sharedPreferences.getString ("delVal","").equals ("")){
                Intent intent = new Intent(this, accountPage.class);
                startActivity(intent);
                finish ();
            }
            else{
                alertshow ("Kindly pass a location");
            }
        }
        else{
            if(sharedPreferences.getInt ("addId",0)==0){
                alertshow ("Kindly select an address");
            }
            else{
                Intent intent = new Intent(this, cartPage1.class);
                startActivity(intent);
                finish ();
            }
        }
    }

    public void addNewAdd(View v){
        Intent intent = new Intent(this, defaultAddress.class);
        Log.i(TAG,returnPage);
        intent.putExtra("nextPage",returnPage);
        startActivity(intent);
        finish ();
    }

    public void alertshow(String message){
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);
        View mView = getLayoutInflater().inflate(R.layout.alertdialog, null);
        TextView messageshow = mView.findViewById(R.id.errormessage);
        messageshow.setText(message);
        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        dialog.show();
        TextView close = mView.findViewById(R.id.modalclose);
        close.setText ("Select new address");
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss ();
            }
        });
    }

    @Override
    public void onBackPressed(){
        if(returnPage.equals ("account")){
            Intent intent = new Intent(manageAddress.this, accountPage.class);
            startActivity(intent);
            finish();
        }
        else{
            Intent intent = new Intent(manageAddress.this, homePage.class);
            intent.putExtra ("nextPage",returnPage);
            startActivity(intent);
            finish();
        }
    }
}
