package com.martjinni.userApp;

import java.util.List;

public class displayHorizontalClass {

    private List<displaySubAddOnListClass> information;
    private String addOnName;

    public displayHorizontalClass(List<displaySubAddOnListClass> information, String addOnName) {
        this.information = information;
        this.addOnName = addOnName;
    }

    public List<displaySubAddOnListClass> getInformation() {
        return information;
    }

    public String getAddOnName() {
        return addOnName;
    }
}
