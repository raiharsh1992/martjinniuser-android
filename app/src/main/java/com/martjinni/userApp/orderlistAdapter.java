package com.martjinni.userApp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

public class orderlistAdapter extends RecyclerView.Adapter<orderlistAdapter.MyViewHolder>{
    private List<orederlistconstructor> orderlistconstructorl;
    Context mCtx;

    public orderlistAdapter(List<orederlistconstructor> orderlistconstructorl, Context mCtx) {
        this.orderlistconstructorl = orderlistconstructorl;
        this.mCtx = mCtx;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.orderlistcard, null);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
            orederlistconstructor list = orderlistconstructorl.get(position);
            holder.orderid.setText(("Order Id : " + list.getOrderId()));
            holder.orderstate = list.getOrderStatus();
            holder.datecreated.setText(("Dated : "+list.getDate()));
            holder.delivernumber.setText((list.getDonedelivery() + " Delivered"));
            holder.workingnumber.setText((list.getWorking() + " Working"));
            holder.totalsum.setText(("Rs. "+list.getTotalAmount()));
            holder.progressfinal.setProgress(list.getDonepercent());
        switch (holder.orderstate) {
            case "NEW":
                holder.progresswork.setIndeterminate(false);
                holder.progressfinal.setIndeterminate(false);
                holder.imageorderaccepted.setImageResource(R.drawable.intransit);
                holder.imagedeliveredall.setImageResource(R.drawable.delivered);
                break;
            case "ACCEPTED":
                holder.progresswork.setIndeterminate(false);
                holder.imageorderaccepted.setImageResource(R.drawable.intransitcolor);
                if(list.getDonedelivery()==0)
                {
                holder.progressfinal.setIndeterminate(false);
                holder.imagedeliveredall.setImageResource(R.drawable.delivered);
                }
                else{
                    holder.progressfinal.setIndeterminate(false);
                    holder.progressfinal.setProgress(list.getDonepercent());
                    if(list.getDonepercent()==100)
                    {

                        holder.imagedeliveredall.setImageResource(R.drawable.deliveredcolor);
                    }
                }
                break;
            case "COMPLETED":
                holder.progresswork.setIndeterminate(false);
                holder.progressfinal.setIndeterminate(false);
                holder.imageorderaccepted.setImageResource(R.drawable.intransitcolor);
                holder.imagedeliveredall.setImageResource(R.drawable.deliveredcolor);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return orderlistconstructorl.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView orderid ;
        TextView datecreated;
        TextView workingnumber;
        TextView delivernumber;
        TextView totalsum;
        TextView viewdetails;
        ImageView imagedeliveredall;
        ImageView imageorderaccepted;
        ProgressBar progressfinal;
        ProgressBar progresswork;
        String orderstate;
        public MyViewHolder(View itemView) {
            super(itemView);
            imagedeliveredall = itemView.findViewById(R.id.deliveredtruck);
            imageorderaccepted = itemView.findViewById(R.id.intransit);
             orderid = (TextView) itemView.findViewById(R.id.orderid);
             datecreated = (TextView) itemView.findViewById(R.id.datecreated);
             workingnumber = (TextView) itemView.findViewById(R.id.workingnumber);
             delivernumber = (TextView) itemView.findViewById(R.id.deliverynumber);
             totalsum = (TextView) itemView.findViewById(R.id.totalnumber);
             viewdetails = (TextView) itemView.findViewById(R.id.viewdetails);
             progressfinal = (ProgressBar) itemView.findViewById(R.id.progressfinal);
             progresswork = (ProgressBar) itemView.findViewById(R.id.progresswork);
             viewdetails.setTag(orderid.getText().toString());
             viewdetails.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     int position = getAdapterPosition ();
                     callOrderDetails(position);
                 }
             });

        }
    }

    public void callOrderDetails(int position){
        orederlistconstructor container = orderlistconstructorl.get (position);
        Intent intent = new Intent(mCtx, orderDetails.class);
        intent.putExtra("orderId",String.valueOf (container.getOrderId ()));
        intent.putExtra("nextPage","orders");
        mCtx.startActivity(intent);
        ((Activity)mCtx).finish ();
    }
}
