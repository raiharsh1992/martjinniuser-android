package com.martjinni.userApp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

public class filterSetAdapter extends RecyclerView.Adapter<filterSetAdapter.filterViewHolder>{

    private Context mCtx;
    public View exview;
    private List<filterRecyler> filterList;
    public filterSetAdapter(Context mCtx, List<filterRecyler> filterList) {
        this.mCtx = mCtx;
        this.filterList = filterList;
    }

    @NonNull
    @Override
    public filterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from (mCtx);
        View view = inflater.inflate (R.layout.button_filter,null);
        return new filterViewHolder (view);
    }

    @Override
    public void onBindViewHolder(@NonNull filterViewHolder holder, int position) {
        filterRecyler filterData = filterList.get(position);
        holder.mjCatName.setText (filterData.getCatNameFilter ());
        filterCheckboxAdapter adapter = new filterCheckboxAdapter (mCtx,filterData.getSubCatList ());
        holder.checkBoxes.setHasFixedSize (false);
        holder.checkBoxes.setLayoutManager (new LinearLayoutManager (mCtx));
        holder.checkBoxes.setAdapter (adapter);
    }

    @Override
    public int getItemCount() {
        return filterList.size ();
    }

    class filterViewHolder extends RecyclerView.ViewHolder{
        TextView mjCatName;
        RecyclerView checkBoxes;
        public filterViewHolder(View itemView){
            super(itemView);
            mjCatName = itemView.findViewById (R.id.textHead);
            checkBoxes = itemView.findViewById (R.id.checkboxList);
            itemView.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    LinearLayout linearLayout = v.findViewById (R.id.subcat);
                    LinearLayout filterlistborder = v.findViewById(R.id.filterlistborder);
                    if (linearLayout.getVisibility () == View.GONE) {
                        TranslateAnimation animate = new TranslateAnimation (-v.getWidth(), 0, 0, 0);
                        animate.setDuration (500);
                        animate.setFillAfter (true);
                        expand(linearLayout);
                    } else if (linearLayout.getVisibility () == View.VISIBLE) {
                        TranslateAnimation animate = new TranslateAnimation (0, -v.getWidth (), 0, 0);
                        animate.setDuration (500);
                        animate.setFillAfter (true);
                        collapse(linearLayout);
                    }
                }
            });
        }
    }


    public  void expand(final View v) {
        try{
            exview.setVisibility(View.GONE);
        }
        catch (Exception e){
            Log.i("Hello", e.toString());
        }
        exview = v;
        v.measure(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation()
        {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? RecyclerView.LayoutParams.WRAP_CONTENT
                        : (int)(targetHeight * interpolatedTime*2);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int)(targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation()
        {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if(interpolatedTime == 1){
                    v.setVisibility(View.GONE);
                }else{
                    v.getLayoutParams().height = initialHeight - (int)(initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int)(initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }


}
