package com.martjinni.userApp;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class cart1ItemAdapter extends RecyclerView.Adapter<cart1ItemAdapter.cartItemViewHolder>{

    private Context mCtx;
    private List<eachItemCardClass> itemInformation;

    public cart1ItemAdapter(Context mCtx, List<eachItemCardClass> itemInformation) {
        this.mCtx = mCtx;
        this.itemInformation = itemInformation;
    }

    @NonNull
    @Override
    public cartItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from (mCtx);
        View view = inflater.inflate (R.layout.eachitemdetail,null);
        return new cartItemViewHolder (view);
    }

    @Override
    public void onBindViewHolder(@NonNull cartItemViewHolder holder, int position) {
        eachItemCardClass container = itemInformation.get (position);
        holder.itemNameDisplay.setText (container.getItemName ());
        holder.unitName.setText (container.getUnitInfo ());
        if(container.getSubsDayCount ()>0){
            String displayText = "Subscription start date : "+container.getSusbStartDate ();
            holder.subsStartDate.setText (displayText);
            displayText = "No. of days : "+container.getSubsDayCount ();
            holder.subsNoOfDays.setText (displayText);
            displayText = "Subscription fees : "+container.getSubsPrice ();
            holder.subsPrice.setText (displayText);
            holder.subscriptionInformation.setVisibility (View.VISIBLE);
        }
        if(container.getInformation ().size ()>0){
            diplayHorizontalAdapter adapter = new diplayHorizontalAdapter (mCtx,container.getInformation ());
            holder.displayAddon.setVisibility (View.VISIBLE);
            holder.displayAddon.setHasFixedSize (false);
            holder.displayAddon.setLayoutManager (new LinearLayoutManager (mCtx));
            holder.displayAddon.setAdapter (adapter);
        }
        String quantTag = "quantity"+container.getCartId ();
        holder.quantInfor.setText (String.valueOf (container.getBaseUnits ()));
        holder.quantInfor.setTag (quantTag);
        float price = container.getBaseUnits ()*container.getPerUnitPrice ()+container.getSubsPrice ();
        String displayName = "Rs : "+price;
        holder.itemTotal.setText (displayName);
        quantTag = "priceValue"+container.getCartId ();
        holder.itemTotal.setTag (quantTag);
    }

    @Override
    public int getItemCount() {
        return itemInformation.size ();
    }

    class cartItemViewHolder extends RecyclerView.ViewHolder{
        TextView itemNameDisplay, unitName, subsStartDate, subsNoOfDays, subsPrice, quantInfor, itemTotal;
        LinearLayout subscriptionInformation;
        ImageView plusButton, minusButton;
        RecyclerView displayAddon;
        public cartItemViewHolder(final View itemView){
            super(itemView);
            itemNameDisplay = itemView.findViewById (R.id.itemNameSub);
            unitName = itemView.findViewById (R.id.unitDetails);
            displayAddon = itemView.findViewById (R.id.displayAddOnInfo);
            subsStartDate = itemView.findViewById (R.id.subsStartDate);
            subsNoOfDays = itemView.findViewById (R.id.subsNoOfDays);
            quantInfor = itemView.findViewById (R.id.itemCount);
            subsPrice = itemView.findViewById (R.id.subsPrice);
            subscriptionInformation = itemView.findViewById (R.id.subsDetailsView);
            minusButton = itemView.findViewById (R.id.minusClick);
            plusButton = itemView.findViewById (R.id.plusClick);
            itemTotal = itemView.findViewById (R.id.itemTotal);
            minusButton.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition ();
                    decrementFromItemShop(position,itemView);
                }
            });
            plusButton.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition ();
                    incrementItemShop (position,itemView);
                }
            });
        }
    }

    public void incrementItemShop(int position, View itemView){
        eachItemCardClass container = itemInformation.get (position);
        Cursor cartInfo = getCartInfo (container.getCartId ());
        cartInfo.moveToFirst ();
        int count = cartInfo.getInt (1)+1;
        incrementCountCart(container.getCartId (),count);
        updateTotalValue (cartInfo.getFloat (9));
        int itemCount = getItemCountItem (cartInfo.getInt (2));
        itemCount = itemCount+1;
        updateItemCount(cartInfo.getInt (2),itemCount);
        ((cartPage1)mCtx).displayList ();
    }

    public void decrementFromItemShop(int position, View itemView){
        eachItemCardClass container = itemInformation.get (position);
        Cursor cartInfo = getCartInfo (container.getCartId ());
        cartInfo.moveToFirst ();
        int count = cartInfo.getInt (1)-1;
        incrementCountCart(container.getCartId (),count);
        int itemCount = getItemCountItem (cartInfo.getInt (2));
        itemCount = itemCount-1;
        updateItemCount(cartInfo.getInt (2),itemCount);
        if(count==0){
            deleteCartInfo(container.getCartId ());
            updateTotalValueNegative (cartInfo.getFloat (9)+container.getSubsPrice ());
        }
        else{
            updateTotalValueNegative (cartInfo.getFloat (9));
        }
        ((cartPage1)mCtx).displayList ();
    }


    public void updateTotalValue(float addAmount){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        float totalAmount = sharedPreferences.getFloat ("totalBill",0.0f);
        totalAmount = totalAmount + addAmount;
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putFloat ("totalBill",totalAmount).apply ();
    }

    public void updateTotalValueNegative(float addAmount){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        float totalAmount = sharedPreferences.getFloat ("totalBill",0.0f);
        totalAmount = totalAmount - addAmount;
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putFloat ("totalBill",totalAmount).apply ();
    }

    public void incrementCountCart(int cartId, int quant){
        SQLiteDatabase myDatabase = mCtx.openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        myDatabase.execSQL ("UPDATE cartInfo set count='"+quant+"' where cartId='"+cartId+"'");
    }

    public void deleteCartInfo(int cartId){
        SQLiteDatabase myDatabase = mCtx.openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        myDatabase.execSQL ("delete from cartInfo where cartId='"+cartId+"'");
    }

    public Cursor getCartInfo(int cartId){
        SQLiteDatabase myDatabase = mCtx.openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        Cursor resultSet = myDatabase.rawQuery ("SELECT * from cartInfo where cartId='"+cartId+"'",null);
        return resultSet;
    }

    public int getItemCountItem(int itemId){
        SQLiteDatabase myDatabase = mCtx.openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        Cursor resultSet = myDatabase.rawQuery ("SELECT itemCount from itemListMaster where itemId='"+itemId+"'",null);
        resultSet.moveToFirst ();
        return resultSet.getInt (0);
    }

    public void updateItemCount(int itemId, int quant){
        SQLiteDatabase myDatabase = mCtx.openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        myDatabase.execSQL ("UPDATE itemListMaster set itemCount='"+quant+"' where itemId='"+itemId+"'");
    }

}