package com.martjinni.userApp;

import java.util.List;

public class addOnMaster {
    private String addOnMasterName;
    private int price, isActive, isMulti, groupIdMaster;
    private List<addOnList> addOnListUse;

    public List<addOnList> getAddOnListUse() {
        return addOnListUse;
    }

    public String getAddOnMasterName() {
        return addOnMasterName;
    }

    public int getPrice() {
        return price;
    }

    public int getIsActive() {
        return isActive;
    }

    public int getIsMulti() {
        return isMulti;
    }

    public int getGroupIdMaster() {
        return groupIdMaster;
    }

    public addOnMaster(String addOnMasterName, int price, int isActive, int isMulti, int groupId, List<addOnList> addOnListUse) {
        this.addOnMasterName = addOnMasterName;
        this.price = price;
        this.isActive = isActive;
        this.isMulti = isMulti;
        this.groupIdMaster = groupId;
        this.addOnListUse = addOnListUse;
    }
}
