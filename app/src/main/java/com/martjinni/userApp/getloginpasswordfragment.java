package com.martjinni.userApp;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * A simple {@link Fragment} subclass.
 */
public class getloginpasswordfragment extends Fragment {

    TextView loginBtn, forgotpass;
    EditText password;
    public RequestQueue mRequestQueue;
    volleyCall newClass = new volleyCall ();
    public String baseUrl = newClass.baseUrl;

    public getloginpasswordfragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
            View view =  inflater.inflate(R.layout.fragment_getpassword, container, false);
            loginBtn = view.findViewById(R.id.loginaction);
            password = view.findViewById(R.id.getpassword);

        forgotpass = view.findViewById(R.id.forgotpass);
        forgotpass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((loginSignup)getActivity()).restorepass();
            }
        });
            loginBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String pass = password.getText().toString();
                    String number = ((loginSignup)getActivity()).getNumber();
                    String mode = "client";
                    if(!pass.equals("")) {
                        try {
                            Cache cache = new DiskBasedCache(getContext().getCacheDir(), 1024 * 1024);
                            Network network = new BasicNetwork(new HurlStack());
                            mRequestQueue = new RequestQueue(cache, network);
                            mRequestQueue.start();
                            JSONObject jsonVal = new JSONObject();
                            jsonVal.put("userName", number);
                            jsonVal.put("password", pass);
                            jsonVal.put("mode", mode);
                            String userUrl = baseUrl + "login";
                            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, userUrl, jsonVal, new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    password.clearFocus();
                                    SharedPreferences sharedPreferences = (getContext ()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = sharedPreferences.edit();
                                    editor.putString ("sessionInfo",response.toString ()).apply ();
                                    ((loginSignup)getActivity()).registerToken ();
                                    ((loginSignup)getActivity()).redirectToNext("login");
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    password.clearFocus();
                                    Log.d("error", error.toString());
                                    Toast.makeText(getContext(), "Wrong Password entered", Toast.LENGTH_LONG).show();
                                    error.printStackTrace();
                                }
                            });
                            mRequestQueue.add(jsonObjectRequest);
                        } catch (JSONException jsonException) {
                            jsonException.printStackTrace();
                        }
                    }
                    else{
                        Toast.makeText(getContext(), "Please Enter a Valid password", Toast.LENGTH_LONG).show();
                    }
                }

                });
            return view;
            }

    }

