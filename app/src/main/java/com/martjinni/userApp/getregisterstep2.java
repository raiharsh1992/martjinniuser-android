package com.martjinni.userApp;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;



/**
 * A simple {@link Fragment} subclass.
 */
public class getregisterstep2 extends Fragment {


    EditText otpval;
    TextView signup;
    TextView resend;
    CheckBox rb;
    public int totalOtp = 1;
    public getregisterstep2() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_getregisterstep2, container, false);
        signup = view.findViewById(R.id.finalregister);
        resend = view.findViewById(R.id.resendotp);
        otpval = view.findViewById(R.id.getotp);
        rb = view.findViewById(R.id.radio);

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String otpvalnew = otpval.getText().toString();
                if(otpvalnew.length() <6 || otpvalnew.length() >6)
                {
                    otpval.setError ("Otp must be of 6 Digits");
                }
                else if(!rb.isChecked()){
                    rb.setError ("Please accept our terms and conditions");
                    ((loginSignup)getActivity()).alertshow("PLease expect our terms and conditions");
                }
                else{
                    ((loginSignup)getActivity()).createuser(otpvalnew);
                }

            }
        });

        resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if(totalOtp>3){
                   ((loginSignup)getActivity()).alertshow("Please be patient, OTP is on its way");
               }
               else{
                   ((loginSignup)getActivity()).generateOtp("retry");
                   totalOtp = totalOtp+1;
               }
            }
        });
        return view;
    }

}
