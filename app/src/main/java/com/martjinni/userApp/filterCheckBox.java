package com.martjinni.userApp;

public class filterCheckBox {
    String subCat;
    String catId;

    public filterCheckBox(String subCat, String catId) {
        this.subCat = subCat;
        this.catId = catId;
    }

    public String getCatId() {
        return catId;
    }

    public String getSubCat() {
        return subCat;
    }
}
