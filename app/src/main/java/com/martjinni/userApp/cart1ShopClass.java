package com.martjinni.userApp;

import java.util.List;

public class cart1ShopClass {
    private List<eachItemCardClass> itemInformation;
    private String shopName;
    private float shopCost, shopGst, shopDel;
    private int shopId;

    public cart1ShopClass(List<eachItemCardClass> itemInformation, String shopName, float shopCost, float shopGst, float shopDel, int shopId) {
        this.itemInformation = itemInformation;
        this.shopName = shopName;
        this.shopCost = shopCost;
        this.shopGst = shopGst;
        this.shopDel = shopDel;
        this.shopId = shopId;
    }

    public int getShopId() {
        return shopId;
    }

    public List<eachItemCardClass> getItemInformation() {
        return itemInformation;
    }

    public String getShopName() {
        return shopName;
    }

    public float getShopCost() {
        return shopCost;
    }

    public float getShopGst() {
        return shopGst;
    }

    public float getShopDel() {
        return shopDel;
    }
}
