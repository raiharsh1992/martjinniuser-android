package com.martjinni.userApp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class slideActivity extends AppCompatActivity {

    private ViewPager mSlideViewPager;
    private LinearLayout mDotLayout;
    private Button mNextBtn;
    private Button mPrevBtn;



    private int mCurrentPage;
    private TextView[] mDots;

    private SliderAdapter sliderAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SharedPreferences sharedPreferences =  getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        String status = sharedPreferences.getString("introStatus", "");
        //int stat = Integer.parseInt(status);
        if (status == "1")
        {
            Toast.makeText(getApplicationContext(), "Value is set to "+status,
                    Toast.LENGTH_LONG).show();
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slide);

        mSlideViewPager =findViewById(R.id.slideViewPager);
        mDotLayout = findViewById(R.id.dots_layout);

        mNextBtn =  findViewById(R.id.nextBtn);
        mPrevBtn =  findViewById(R.id.prevBtn);


        sliderAdapter = new SliderAdapter (this);

        mSlideViewPager.setAdapter(sliderAdapter);

        addDotsIndicators(0);

        mSlideViewPager.addOnPageChangeListener(viewListerner);

        mNextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mNextBtn.getText().toString() == "Finish") {
                    introComplete();
                } else {
                    mSlideViewPager.setCurrentItem(mCurrentPage + 1);
                }
            }
        });

        mPrevBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSlideViewPager.setCurrentItem(mCurrentPage - 1);
            }
        });
    }
    public void addDotsIndicators(int position)
    {
        mDots = new TextView[3];
        mDotLayout.removeAllViews();
        for (int i=0; i < mDots.length; i++)
        {
            mDots[i] = new TextView(this);
            mDots[i].setText(Html.fromHtml("&#8226;"));
            mDots[i].setTextColor(getResources().getColor(R.color.colorTransparentWhite));
            mDots[i].setTextSize(54);
            mDotLayout.addView(mDots[i]);
        }
        if(mDots.length >= 0)
        {
            mDots[position].setTextColor(getResources().getColor(R.color.colorWhite));
            mDots[position].setTextSize(10);
            mDots[position].setTextScaleX(50);


        }
    }

    public void introComplete(){
        SharedPreferences sharedPreferences = getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("introStatus", "1");
        editor.apply();
        Toast.makeText(getApplicationContext(), "Welcome To MartJinni, Intro Set",Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, defaultAddress.class);
        intent.putExtra("nextPage","home");
        startActivity(intent);
        finish();
    }
    ViewPager.OnPageChangeListener viewListerner = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int i, float v, int i1) {

        }

        @Override
        public void onPageSelected(int i) {
            addDotsIndicators(i);

            mCurrentPage =i;

            if(i==0)
            {
                mNextBtn.setEnabled(true);
                mPrevBtn.setEnabled(false);
                mPrevBtn.setVisibility(View.INVISIBLE);

                mNextBtn.setText("Next");
                mPrevBtn.setText("");

            }
            else if(i== mDots.length - 1)
            {
                mNextBtn.setEnabled(true);
                mPrevBtn.setEnabled(true);

                mPrevBtn.setVisibility(View.VISIBLE);

                mPrevBtn.setText("Prev");
                mNextBtn.setText("Finish");

            }
            else{
                mNextBtn.setEnabled(true);
                mPrevBtn.setEnabled(true);

                mPrevBtn.setVisibility(View.VISIBLE);

                mPrevBtn.setText("Prev");
                mNextBtn.setText("Next");

            }
        }

        @Override
        public void onPageScrollStateChanged(int i) {

        }
    };

}
