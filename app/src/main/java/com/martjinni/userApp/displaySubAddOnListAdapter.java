package com.martjinni.userApp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import java.util.List;

public class displaySubAddOnListAdapter extends  RecyclerView.Adapter<displaySubAddOnListAdapter.displaySubAddOnViewHolde>{

    private Context mCtx;
    private List<displaySubAddOnListClass> useInfo;

    public displaySubAddOnListAdapter(Context mCtx, List<displaySubAddOnListClass> useInfo) {
        this.mCtx = mCtx;
        this.useInfo = useInfo;
    }

    @NonNull
    @Override
    public displaySubAddOnViewHolde onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from (mCtx);
        View view = inflater.inflate (R.layout.subaddonlist,null);
        return new displaySubAddOnViewHolde (view);
    }

    @Override
    public void onBindViewHolder(@NonNull displaySubAddOnViewHolde holder, int position) {
        displaySubAddOnListClass information = useInfo.get (position);
        String displayText = "+"+information.getAddOnName ();
        holder.subAddOnName.setText (displayText);
    }

    @Override
    public int getItemCount() {
        return useInfo.size ();
    }

    class displaySubAddOnViewHolde extends RecyclerView.ViewHolder{
        TextView subAddOnName;
        public displaySubAddOnViewHolde(final View itemView){
            super(itemView);
            subAddOnName = itemView.findViewById (R.id.subAddOnName);
        }
    }
}
