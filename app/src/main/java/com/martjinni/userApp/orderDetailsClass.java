package com.martjinni.userApp;

import java.util.List;

public class orderDetailsClass {
    private  String shopName, orderStatus, amount, rating;
    private List<diplayOrderDetailsItemClass> useInfo;
    private int orderStoreId, orderMasterId;

    public orderDetailsClass(String shopName, String orderStatus, String amount, List<diplayOrderDetailsItemClass> useInfo, int orderStoreId, int orderMasterId, String rating) {
        this.shopName = shopName;
        this.orderStatus = orderStatus;
        this.amount = amount;
        this.useInfo = useInfo;
        this.orderStoreId = orderStoreId;
        this.orderMasterId = orderMasterId;
        this.rating = rating;
    }

    public String getRating() {
        return rating;
    }

    public String getShopName() {
        return shopName;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public String getAmount() {
        return amount;
    }

    public List<diplayOrderDetailsItemClass> getUseInfo() {
        return useInfo;
    }

    public int getOrderStoreId() {
        return orderStoreId;
    }

    public int getOrderMasterId() {
        return orderMasterId;
    }
}
