package com.martjinni.userApp;

public class constructor_subslist{
    private String shopname, itemname, startDate, status, addoname;
    private Double price;
    private int delcount, delleft, subsId, dayscount;
    private int donepercent;
    int delDone;

    public constructor_subslist(String shopname, String itemname, String startDate, int dayscount, String status, String addoname, Double price, int delcount, int delleft, int subsId) {
        this.shopname = shopname;
        this.itemname = itemname;
        this.startDate = startDate;
        this.dayscount = dayscount;
        this.status = status;
        this.addoname = addoname;
        this.price = price;
        this.delcount = delcount;
        this.delleft = delleft;
        this.delDone = getDelcount() - getDelleft();
        this.subsId = subsId;
        this.donepercent =((this.delDone*100/this.delcount));
    }

    public String getShopname() {
        return shopname;
    }

    public String getItemname() {
        return itemname;
    }

    public String getStartDate() {
        return startDate;
    }

    public int getDayscount() {
        return dayscount;
    }

    public String getStatus() {
        return status;
    }

    public String getAddoname() {
        return addoname;
    }

    public Double getPrice() {
        return price;
    }

    public int getDelcount() {
        return delcount;
    }

    public int getDelleft() {
        return delleft;
    }

    public int getSubsId() {
        return subsId;
    }
    public int getDelDone(){
        return delDone;
    }
    public int getDelpercent(){
        return donepercent;
    }
}
