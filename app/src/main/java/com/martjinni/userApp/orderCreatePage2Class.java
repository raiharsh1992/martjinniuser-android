package com.martjinni.userApp;

public class orderCreatePage2Class {

    private  String shopName, promoApplied;
    private int totalItem,  orderStoreId, orderMasterId;
    private float totalBill;

    public orderCreatePage2Class(String shopName, int totalItem, Double totalBill, int orderStoreId, int orderMasterId) {
        this.shopName = shopName;
        this.totalItem = totalItem;
        this.totalBill = Float.valueOf (String.valueOf (totalBill));
        this.orderStoreId = orderStoreId;
        this.orderMasterId = orderMasterId;
        this.promoApplied = "no";
    }

    public String getShopName() {
        return shopName;
    }

    public int getTotalItem() {
        return totalItem;
    }

    public float getTotalBill() {
        return totalBill;
    }

    public int getOrderStoreId() {
        return orderStoreId;
    }

    public int getOrderMasterId() {
        return orderMasterId;
    }

    public String getPromoApplied() {
        return promoApplied;
    }

    public void setPromoApplied(String promoApplied) {
        this.promoApplied = promoApplied;
    }

    public void setTotalBill(float totalBill) {
        this.totalBill = totalBill;
    }
}
