package com.martjinni.userApp;

public class recomendClass {
    String imgUrl, headText, subText, priceText, spclCat;
    int itemId,count,customIf;

    public recomendClass(String imgUrl, String headText, String subText, String priceText, String spclCat, int itemId, int count, int CustomIf) {
        this.imgUrl = imgUrl;
        this.headText = headText;
        this.subText = subText;
        this.priceText = priceText;
        this.spclCat = spclCat;
        this.itemId = itemId;
        this.count = count;
        this.customIf = CustomIf;
    }

    public int getCustomIf() {
        return customIf;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public String getHeadText() {
        return headText;
    }

    public String getSubText() {
        return subText;
    }

    public String getPriceText() {
        return priceText;
    }

    public String getSpclCat() {
        return spclCat;
    }

    public int getItemId() {
        return itemId;
    }

    public int getCount() {
        return count;
    }
}
