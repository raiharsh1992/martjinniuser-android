package com.martjinni.userApp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import java.util.List;

public class shopCardAdapter extends RecyclerView.Adapter<shopCardAdapter.shopCardViewHolder>{

    private Context mCtx;
    private List<shopCard> shopList;

    public shopCardAdapter(Context mCtx, List<shopCard> shopList) {
        this.mCtx = mCtx;
        this.shopList = shopList;
    }

    @NonNull
    @Override
    public shopCardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from (mCtx);
        View view = inflater.inflate (R.layout.shops_card,null);
        return new shopCardViewHolder (view);
    }

    @Override
    public void onBindViewHolder(@NonNull shopCardViewHolder holder, int position) {
        shopCard shopcard = shopList.get(position);

        holder.shopName.setText (shopcard.getShopName());
        holder.shopCat.setText (shopcard.getShopCat ());
        holder.shopLoctaion.setText (shopcard.getShopLocation ());
        holder.rateText.setText (String .valueOf (shopcard.getRating ()));
        holder.minDel.setText (shopcard.getMinCharge ());
        holder.delCharge.setText (shopcard.getDelCharge ());
        holder.rateBar.setRating (shopcard.getRating ());
        holder.shopId.setText (String.valueOf(shopcard.getShopId ()));
        Picasso
                 .get ()
                .load(shopcard.getImgUrl ())
                .resize(320, 120)
                .into(holder.carImage);
        if(shopcard.getStatus ()==1){
            holder.status.setText (R.string.open);
            holder.status.setBackgroundResource(R.color.open);
        }
        else{
            holder.status.setText (R.string.close);
            holder.status.setBackgroundResource(R.color.close);
        }

    }

    @Override
    public int getItemCount() {
        return shopList.size ();
    }

    class shopCardViewHolder extends RecyclerView.ViewHolder{

         ImageView carImage;
         TextView shopName, shopCat, shopLoctaion, rateText,  minDel, delCharge, shopId, status;
         RatingBar rateBar;

        public shopCardViewHolder(View itemView) {
            super (itemView);
            carImage = itemView.findViewById (R.id.shopImage);
            shopName = itemView.findViewById (R.id.shopName);
            shopCat = itemView.findViewById (R.id.shopCat);
            shopLoctaion = itemView.findViewById (R.id.shopLocation);
            rateBar = itemView.findViewById (R.id.ratingBar);
            rateText = itemView.findViewById (R.id.rateText);
            minDel = itemView.findViewById (R.id.minDel);
            delCharge = itemView.findViewById (R.id.delCharge);
            shopId = itemView.findViewById (R.id.shopId);
            status = itemView.findViewById (R.id.statusOpenClose);
            itemView.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition ();
                    shopCard shopcard = shopList.get(position);
                    if(shopcard.getStatus ()==1){
                        ((homePage)mCtx).gotoItems (shopcard.getShopId ());
                    }
                }
            });
        }
    }

    public void setShopList(List<shopCard> shopList1) {
        shopList = shopList1;
    }
}
