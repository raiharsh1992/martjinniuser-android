package com.martjinni.userApp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class cartPage1 extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_cart_page1);
        SharedPreferences sharedPreferences2 =  getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        if(sharedPreferences2.getString ("sessionInfo","").equals ("")){
            Intent intent = new Intent(this, loginSignup.class);
            intent.putExtra("nextPage","cart");
            startActivity(intent);
            finish ();
        }
        else if(sharedPreferences2.getInt ("addId",0)==0){
            Intent intent = new Intent(this, manageAddress.class);
            intent.putExtra("nextPage","cart");
            startActivity(intent);
            finish ();
        }
        else{
            try{
                JSONObject sessionInfo = new JSONObject (sharedPreferences2.getString ("sessionInfo",""));
                TextView custName = findViewById (R.id.custName);
                custName.setText (sessionInfo.getString ("userName"));
                custName = findViewById (R.id.custNumber);
                custName.setText (sessionInfo.getString ("phoneNumber"));
                custName = findViewById (R.id.totalPrice);
                custName.setText (String.valueOf (sharedPreferences2.getFloat ("totalBill",0.0f)));
                JSONObject addInfo = new JSONObject (sharedPreferences2.getString ("addressInformation",""));
                custName = findViewById (R.id.addLine1);
                custName.setText (addInfo.getString ("addLine1"));
                custName = findViewById (R.id.addLine2);
                custName.setText (addInfo.getString ("addLine2"));
                String restAddress = addInfo.getString ("city")+" , "+addInfo.getString ("state")+" , "+addInfo.getString ("country");
                custName = findViewById (R.id.restDetails);
                custName.setText (restAddress);
                displayList();
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }
    }

    public void manageAddress(View v){
        Intent intent = new Intent(this, manageAddress.class);
        intent.putExtra("nextPage","cart");
        startActivity(intent);
        finish ();
    }

    public void displayList(){
        try{
            float orderValue = 0.0f,orderGst = 0.0f, orderDelivery = 0.0f;
            Cursor resultSet = getShopList ();
            if(resultSet.getCount ()>0){
                resultSet.moveToFirst ();
                List<cart1ShopClass> cartInfo= new ArrayList<> ();
                while (!resultSet.isAfterLast ()){
                    float shopTotal=0.0f, shopGst=0.0f, shopDel=0.0f;
                    Cursor itemList = getItemList (resultSet.getInt (0));
                    itemList.moveToFirst ();
                    List<eachItemCardClass> itemInfo = new ArrayList<> ();
                    int shopAdded = 0;
                    while(!itemList.isAfterLast ()){
                        Cursor cartInfoUse = getCartInfo (itemList.getInt (0));
                        cartInfoUse.moveToFirst ();
                        while(!cartInfoUse.isAfterLast ()){
                            List<displayHorizontalClass> subAddList = new ArrayList<> ();
                            if(!cartInfoUse.getString (5).equals ("")){
                                JSONArray addOnList = new JSONArray (cartInfoUse.getString (5));
                                for(int i=0;i<addOnList.length ();i++){
                                    List<displaySubAddOnListClass> subAddonList = new ArrayList<> ();
                                    JSONArray itemListUse = addOnList.getJSONObject (i).getJSONArray ("itemId");
                                    for(int j=0;j<itemListUse.length ();j++){
                                        int subAddId = itemListUse.getJSONObject (j).getInt ("subAddId");
                                        String addName = getAddName(subAddId);
                                        subAddonList.add (new displaySubAddOnListClass (addName));
                                    }
                                    if(subAddonList.size ()>0){
                                        String groupName = getGroupName(addOnList.getJSONObject (i).getInt ("addOnId"));
                                        subAddList.add (new displayHorizontalClass (subAddonList,groupName));
                                    }
                                }
                            }
                            String unitName = getUnitName(cartInfoUse.getInt(4));
                            int noOfDays = 0;
                            int subsAmount = 0;
                            if(cartInfoUse.getInt(6)>0){
                                noOfDays = getDaysCount(cartInfoUse.getInt(6));
                                subsAmount = getSubsPrice (cartInfoUse.getInt(6));
                            }
                            shopTotal = shopTotal+((cartInfoUse.getFloat (9)-cartInfoUse.getFloat (10)))*cartInfoUse.getInt (1)+subsAmount;
                            shopGst = shopGst+cartInfoUse.getFloat (10);
                            itemInfo.add (new eachItemCardClass (subAddList,cartInfoUse.getString (3),unitName,cartInfoUse.getString (7),noOfDays,subsAmount,cartInfoUse.getInt(9),cartInfoUse.getInt(1),cartInfoUse.getInt(0)));

                            cartInfoUse.moveToNext ();
                            shopAdded = 1;
                        }
                        itemList.moveToNext ();
                    }
                    if(shopAdded==1){
                        orderValue = shopTotal+orderValue;
                        orderGst = shopGst+orderGst;
                        Cursor shopInformation = getShopInfo (resultSet.getInt (0));
                        shopInformation.moveToFirst ();
                        if(shopInformation.getInt(3)>shopTotal){
                            shopDel = shopInformation.getInt (2);
                            orderDelivery = orderDelivery+shopInformation.getInt (2);
                        }
                        cartInfo.add (new cart1ShopClass (itemInfo,shopInformation.getString (1),shopTotal,shopGst,shopDel,resultSet.getInt (0)));
                        inserUpdateShopDetails(shopTotal,shopGst,resultSet.getInt (0),shopDel);
                    }
                    resultSet.moveToNext ();
                }
                TextView useInfor = findViewById (R.id.orderTotalNoTax);
                String interim = "Rs. "+orderValue;
                useInfor.setText (interim);
                useInfor = findViewById (R.id.orderGst);
                interim = "Rs. "+orderGst;
                useInfor.setText (interim);
                useInfor = findViewById (R.id.orderDelivery);
                interim = "Rs. "+orderDelivery;
                useInfor.setText (interim);
                float total = orderDelivery+orderGst+orderValue;
                interim = "Rs. "+total;
                useInfor = findViewById (R.id.orderFinalTotal);
                useInfor.setText (interim);
                useInfor = findViewById (R.id.totalPrice);
                useInfor.setText (String.valueOf (total));
                cart1ShopAdapter adapter = new cart1ShopAdapter (this,cartInfo);
                RecyclerView shopInfoHere = findViewById (R.id.displayShopInfo);
                shopInfoHere.setHasFixedSize (false);
                shopInfoHere.setNestedScrollingEnabled (false);
                shopInfoHere.setLayoutManager (new GridLayoutManager (this,1));
                shopInfoHere.setAdapter (adapter);
            }
            else{
                Intent intent = new Intent(this, homePage.class);
                startActivity(intent);
                finish ();
            }
        }
        catch (JSONException jsonException){
            jsonException.printStackTrace ();
        }
    }

    public void inserUpdateShopDetails(float shopTotal, float shopGst, int shopId, float shopDel){
        SQLiteDatabase myDatabase = openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        myDatabase.execSQL ("CREATE TABLE IF NOT EXISTS shopCartInformation(shopId INTEGER PRIMARY KEY, shopTotal real, shopGst real, shopDel real)");
        myDatabase.execSQL ("INSERT or replace into shopCartInformation (shopId, shopTotal, shopGst, shopDel) values ('"+shopId+"','"+shopTotal+"','"+shopGst+"','"+shopDel+"')");
    }

    public Cursor getShopInfo(int shopId){
        SQLiteDatabase myDatabase = openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        Cursor resultSet = myDatabase.rawQuery ("SELECT * from shopList where shopId='"+shopId+"'",null);
        return resultSet;
    }

    public int getDaysCount(int subsId){
        SQLiteDatabase myDatabase = openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        Cursor resultSet = myDatabase.rawQuery ("SELECT noOfDays from subsMaster where subsId='"+subsId +"'",null);
        resultSet.moveToFirst ();
        return resultSet.getInt (0);
    }

    public int getSubsPrice(int subsId){
        SQLiteDatabase myDatabase = openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        Cursor resultSet = myDatabase.rawQuery ("SELECT price from subsMaster where subsId='"+subsId +"'",null);
        resultSet.moveToFirst ();
        return resultSet.getInt (0);
    }

    public String getUnitName(int quantId){
        SQLiteDatabase myDatabase = openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        Cursor resultSet = myDatabase.rawQuery ("SELECT quantName from quantList where quantId='"+quantId+"'",null);
        resultSet.moveToFirst ();
        return resultSet.getString (0);
    }

    public String getGroupName(int subAddId){
        SQLiteDatabase myDatabase = openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        Cursor resultSet = myDatabase.rawQuery ("SELECT groupName from addonMaster where groupId='"+subAddId+"'",null);
        resultSet.moveToFirst ();
        return resultSet.getString (0);
    }

    public String getAddName(int subAddId){
        SQLiteDatabase myDatabase = openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        Cursor resultSet = myDatabase.rawQuery ("SELECT addonName from addOnList where addOnItemId='"+subAddId+"'",null);
        resultSet.moveToFirst ();
        return resultSet.getString (0);
    }

    public Cursor getShopList(){
        SQLiteDatabase myDatabase = openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        myDatabase.execSQL ("CREATE TABLE IF NOT EXISTS itemListMaster(itemId INTEGER PRIMARY KEY, itemName VARCHAR, spclCat VARCHAR, gstRate INTEGER,addonInfo VARCHAR, subsInfo VARCHAR, shopId INTEGER, isActive INTEGER, hasImage INTEGER, isCustomize INTEGER, itemCount INTEGER, mainItemId INTEGER, displayPrice INTEGER )");
        Cursor resultSet = myDatabase.rawQuery ("SELECT shopId from itemListMaster where itemCount>'0' group by shopId",null);
        return resultSet;
    }

    public Cursor getItemList(int shopId){
        SQLiteDatabase myDatabase = openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        Cursor resultSet = myDatabase.rawQuery ("SELECT itemId from itemListMaster where itemCount>'0' and shopId='"+shopId+"'",null);
        return resultSet;
    }

    public Cursor getCartInfo(int itemId){
        SQLiteDatabase myDatabase = openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        Cursor resultSet = myDatabase.rawQuery ("SELECT * from cartInfo where itemId='"+itemId+"'",null);
        return resultSet;
    }

    public void proceedOrderCreation(View v){
        SharedPreferences sharedPreferences2 =  getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        if(sharedPreferences2.getFloat ("totalBill",0.0f)>0.0f){
            Intent intent = new Intent(this, cartPage2.class);
            startActivity(intent);
            finish ();
        }
        else{
            Toast.makeText(this,"Cart is empty", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, homePage.class);
            startActivity(intent);
            finish ();
        }
    }

    public void goBackHome(View v){
        Intent intent = new Intent(this, homePage.class);
        startActivity(intent);
        finish ();
    }

    @Override
    public void onBackPressed()
    {
        ViewGroup view = findViewById (R.id.content);
        goBackHome (view); // optional depending on your needs
    }

}
