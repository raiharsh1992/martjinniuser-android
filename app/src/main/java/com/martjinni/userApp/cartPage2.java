package com.martjinni.userApp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class cartPage2 extends AppCompatActivity {
    volleyCall newClass = new volleyCall ();
    float orderValue, discountAmount = 0.0f;
    int orderId;
    public volatile boolean workinOn = false;
    LinearLayout progBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_cart_page2);
        ImageView goBackStep = findViewById (R.id.goBackStep);
        progBar = findViewById (R.id.progressBar);
        goBackStep.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                goToPrevious ();
            }
        });
        SharedPreferences sharedPreferences2 =  getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        if(sharedPreferences2.getString ("sessionInfo","").equals ("")){
            Intent intent = new Intent(this, loginSignup.class);
            intent.putExtra("nextPage","cart");
            startActivity(intent);
            finish ();
        }
        else if(sharedPreferences2.getInt ("addId",0)==0){
            Intent intent = new Intent(this, manageAddress.class);
            intent.putExtra("nextPage","cart");
            startActivity(intent);
            finish ();
        }
        else{
            createOrder ();
        }
    }

    public void createOrder(){
        try{
            Cursor shopList = getShopList ();
            if(shopList.getCount ()>0){
                shopList.moveToFirst ();
                JSONArray shopItemList = new JSONArray ();
                while(!shopList.isAfterLast ()){
                    Cursor itemList = getItemList (shopList.getInt (0));
                    itemList.moveToFirst ();
                    JSONArray itemStoreList = new JSONArray ();
                    while (!itemList.isAfterLast ()){
                        Cursor cartInfoUse = getCartInfo (itemList.getInt (0));
                        cartInfoUse.moveToFirst ();
                        while(!cartInfoUse.isAfterLast ()){
                            JSONObject insertItem = new JSONObject ();
                            if(!cartInfoUse.getString (7).equals ("")){
                                Cursor shopTimeInfo = getShopSubs(shopList.getInt (0));
                                String startDate = "";
                                if(shopTimeInfo.getCount ()>0){
                                    shopTimeInfo.moveToFirst ();
                                    while (!shopTimeInfo.isAfterLast ()){
                                        if(shopTimeInfo.getInt (0)==Integer.parseInt (cartInfoUse.getString (8))){
                                            startDate = cartInfoUse.getString (7)+" "+shopTimeInfo.getString (3);
                                        }
                                        shopTimeInfo.moveToNext ();
                                    }
                                }
                                else{
                                    startDate = cartInfoUse.getString (7)+" "+cartInfoUse.getString (8);
                                }
                                JSONObject subsInfo = new JSONObject ();
                                subsInfo.put ("startDate",startDate);
                                subsInfo.put ("subId",cartInfoUse.getInt (6));
                                insertItem.put ("subsInfo",subsInfo);
                            }
                            else{
                                insertItem.put ("subsInfo","");
                            }
                            if(!cartInfoUse.getString (5).equals ("")){
                                JSONArray addOnInfo = new JSONArray (cartInfoUse.getString (5));
                                insertItem.put("addOnInfo",addOnInfo);
                            }
                            else{
                                insertItem.put("addOnInfo","");
                            }
                            insertItem.put("units",cartInfoUse.getInt (1));
                            insertItem.put("itemId",cartInfoUse.getInt (2));
                            insertItem.put("quantId",cartInfoUse.getInt (4));
                            itemStoreList.put (insertItem);
                            cartInfoUse.moveToNext ();
                        }
                        itemList.moveToNext ();
                    }
                    if(itemStoreList.length ()>0){
                        JSONObject shopInformation = new JSONObject ();
                        shopInformation.put ("itemList",itemStoreList);
                        shopInformation.put ("shopId",shopList.getInt (0));
                        shopItemList.put(shopInformation);
                    }
                    shopList.moveToNext ();
                }
                if(shopItemList.length ()>0){
                    JSONObject requestObject = new JSONObject ();
                    SharedPreferences sharedPreferences =  getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                    final JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
                    requestObject.put ("shopItemList",shopItemList);
                    requestObject.put("typeId",sessionInfo.getInt ("typeId"));
                    requestObject.put("userId",sessionInfo.getInt ("userId"));
                    requestObject.put("custAddId",sharedPreferences.getInt ("addId",0));
                    Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                    Network network = new BasicNetwork (new HurlStack ());
                    RequestQueue mRequestQueue = new RequestQueue (cache, network);
                    mRequestQueue.start();
                    requestObject.put("userId",sessionInfo.getInt ("userId"));
                    String finalUrl = newClass.baseUrl+"createorder";
                    Log.i("Kya request h",String.valueOf (requestObject));
                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            inflateView(response);
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace ();
                        }
                    }){
                        @Override
                        public Map getHeaders() throws AuthFailureError {
                            Map<String,String> params = new HashMap<> ();
                            params.put("Content-Type", "application/json");
                            try{
                                params.put("basicAuthenticate", sessionInfo.getString ("basicAuthenticate"));
                            }
                            catch (JSONException jsonExcepion2){
                                jsonExcepion2.printStackTrace ();
                            }
                            return params;
                        }
                    };
                    mRequestQueue.add(jsonObjectRequest);
                }
            }
            else{
                Intent intent = new Intent(this, homePage.class);
                startActivity(intent);
                finish ();
            }
        }
        catch(JSONException jsonException){
            jsonException.printStackTrace ();
        }
    }

    public void goToPrevious(){
        if(workinOn){
            Intent intent = new Intent(this, cartPage1.class);
            startActivity(intent);
            finish ();
        }
        else{
            Toast.makeText (getApplicationContext (), "Please be patient, payment is being processed.", Toast.LENGTH_SHORT).show ();
        }
    }

    public void updateDiscountAmount(float newAmount){
        discountAmount = discountAmount+newAmount;
        LinearLayout displayPromo = findViewById (R.id.displayPromoAmount);
        TextView promoValue = findViewById (R.id.promoValue);
        String interim = "- Rs "+discountAmount;
        promoValue.setText (interim);
        orderValue = orderValue-newAmount;
        interim = "Rs "+orderValue;
        promoValue = findViewById (R.id.totalAmount);
        promoValue.setText (interim);
        promoValue = findViewById (R.id.totalAmountFixed);
        promoValue.setText (interim);
        displayPromo.setVisibility (View.VISIBLE);
        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);

        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }

    public void inflateView(JSONObject response){
        try{
            Log.i("Response is ",String.valueOf (response));
            JSONArray successShop = response.getJSONArray ("successShop");
            List<orderCreatePage2Class> useInfo = new ArrayList<> ();
            if(successShop.length ()>0){
                for(int i =0;i<successShop.length ();i++){
                    if(successShop.getJSONObject (i).getJSONArray ("successItem").length ()>0){
                        JSONObject responseUse = successShop.getJSONObject (i);
                        useInfo.add (new orderCreatePage2Class (responseUse.getString ("shopName"),responseUse.getJSONArray("successItem").length(),responseUse.getDouble ("shopTotal"),responseUse.getInt ("orderStoreId"),response.getInt ("orderId")));
                    }
                }
                if(useInfo.size ()>0){
                    displayRecyler (useInfo);
                    TextView totalSum = findViewById (R.id.totalGrandTotal);
                    float interim = Float.valueOf (String.valueOf (response.getDouble ("totalBilledAmount")))-Float.valueOf (String.valueOf (response.getDouble ("totalGstPaid")));
                    String useInterim = "Rs "+interim;
                    totalSum.setText (useInterim);
                    useInterim = "Rs "+response.getDouble ("totalGstPaid");
                    totalSum = findViewById (R.id.orderGst);
                    totalSum.setText (useInterim);
                    useInterim = "Rs "+response.getDouble ("totalBilledAmount");
                    totalSum = findViewById (R.id.totalAmount);
                    totalSum.setText (useInterim);
                    totalSum = findViewById (R.id.totalAmountFixed);
                    totalSum.setText (useInterim);
                    orderValue = Float.valueOf (String.valueOf (response.getDouble ("totalBilledAmount")));
                    orderId = response.getInt ("orderId");
                }
            }
        }
        catch (JSONException jsonException){
            jsonException.printStackTrace ();
        }
    }

    public void createFinalOrder(View view){
        if(workinOn){
            payForOrder runner = new payForOrder ();
            new Thread (runner).start ();
        }
        else{
            Toast.makeText (getApplicationContext (), "Please be patient, payment is being processed.", Toast.LENGTH_SHORT).show ();
        }
    }

    class payForOrder implements Runnable{

        @Override
        public void run(){
            payForOrderFinal();
        }

        private void payForOrderFinal(){
            if(workinOn){
                try{
                    RequestFuture<String> future = RequestFuture.newFuture();
                    workinOn = false;
                    SharedPreferences sharedPreferences =  getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                    final JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
                    JSONObject requestObject = new JSONObject ();
                    requestObject.put ("orderId",orderId);
                    requestObject.put("amount",orderValue);
                    requestObject.put("userId",sessionInfo.getInt ("userId"));
                    requestObject.put("typeId",sessionInfo.getInt ("typeId"));
                    requestObject.put("userType",sessionInfo.getString ("userType"));
                    Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                    Network network = new BasicNetwork (new HurlStack ());
                    RequestQueue mRequestQueue = new RequestQueue (cache, network);
                    mRequestQueue.start();
                    requestObject.put("userId",sessionInfo.getInt ("userId"));
                    String finalUrl = newClass.baseUrl+"payfororder";
                    runOnUiThread (new Runnable () {
                        @Override
                        public void run() {
                            progBar.setVisibility (View.VISIBLE);
                            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        }
                    });
                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            goToSuccess(response);
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace ();
                            Log.i("WOW",error.getMessage ());
                        }
                    }){
                        @Override
                        public Map getHeaders() throws AuthFailureError {
                            Map<String,String> params = new HashMap<> ();
                            params.put("Content-Type", "application/json");
                            try{
                                params.put("basicAuthenticate", sessionInfo.getString ("basicAuthenticate"));
                            }
                            catch (JSONException jsonExcepion2){
                                jsonExcepion2.printStackTrace ();
                            }
                            return params;
                        }
                    };
                    jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(500000, -1,
                            0));
                    mRequestQueue.add(jsonObjectRequest);
                    String result = future.get();
                }
                catch (JSONException jsonException){
                    jsonException.printStackTrace ();
                }
                catch (InterruptedException intException){
                    intException.printStackTrace ();
                }
                catch (ExecutionException execException){
                    execException.printStackTrace ();
                }
            }
            else{
                Toast.makeText (getApplicationContext (), "Please be patient, payment is being processed.", Toast.LENGTH_SHORT).show ();
            }
        }
    }

    public void goToSuccess(JSONObject response){
        this.deleteDatabase ("martjinniItems");
        SharedPreferences sharedPreferences =  getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        sharedPreferences.edit ().remove ("totalBill").apply ();
        Intent intent = new Intent(this, orderSuccess.class);
        intent.putExtra("orderId",String.valueOf (orderId));
        startActivity(intent);
        finish ();
    }

    public void displayRecyler(List<orderCreatePage2Class> useInfo){
        RecyclerView displayInfo = findViewById (R.id.displayInfo);
        orderCretaePage2Adapter adapter = new orderCretaePage2Adapter (this, useInfo);
        displayInfo.setHasFixedSize (false);
        displayInfo.setLayoutManager (new LinearLayoutManager (this));
        displayInfo.setAdapter (adapter);
        workinOn = true;
    }

    public Cursor getShopSubs(int shopId){
        SQLiteDatabase myDatabase = openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        return(myDatabase.rawQuery ("SELECT * from subsData where shopId='"+shopId+"'",null));
    }

    public Cursor getShopList(){
        SQLiteDatabase myDatabase = openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        return(myDatabase.rawQuery ("SELECT shopId from itemListMaster where itemCount>'0' group by shopId",null));
    }

    public Cursor getItemList(int shopId){
        SQLiteDatabase myDatabase = openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        Cursor resultSet = myDatabase.rawQuery ("SELECT itemId from itemListMaster where itemCount>'0' and shopId='"+shopId+"'",null);
        return resultSet;
    }

    public Cursor getCartInfo(int itemId){
        SQLiteDatabase myDatabase = openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        Cursor resultSet = myDatabase.rawQuery ("SELECT * from cartInfo where itemId='"+itemId+"'",null);
        return resultSet;
    }

    @Override
    public void onBackPressed()
    {
        goToPrevious ();
        super.onBackPressed();  // optional depending on your needs
    }
}
