package com.martjinni.userApp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class subsList extends AppCompatActivity {
    public RequestQueue mRequestQueue;
    volleyCall newClass = new volleyCall ();
    public String baseUrl = newClass.baseUrl;
    List<constructor_subslist> subslistval = new ArrayList<>();
    LinearLayout progressBar, noSubs;
    NestedScrollView nestedView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_subs_list);
        progressBar = findViewById (R.id.progressBar4);
        noSubs = findViewById (R.id.noSubs);
        nestedView = findViewById (R.id.subListNew);
        try {
            SharedPreferences sharedPreferences2 = getSharedPreferences("userInfo", Context.MODE_PRIVATE);
            if (sharedPreferences2.getString("sessionInfo", "").equals("")) {
                Intent intent = new Intent(this, accountPage.class);
                startActivity(intent);
                finish ();
            }
            else {

                Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork(new HurlStack());
                RequestQueue mRequestQueue = new RequestQueue(cache, network);
                mRequestQueue.start();
                final JSONObject sessionInfo = new JSONObject(sharedPreferences2.getString("sessionInfo", ""));
                JSONObject requestObject = new JSONObject();
                requestObject.put("typeId", sessionInfo.getInt("typeId"));
                requestObject.put("userId", sessionInfo.getInt("userId"));
                requestObject.put("userType", sessionInfo.getString("userType"));
                String finalUrl = newClass.baseUrl + "viesublist";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, finalUrl, requestObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            inflateview(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Toast.makeText(getApplicationContext(), "Response is Not here", Toast.LENGTH_LONG).show();
                    }
                }) {
                    @Override
                    public Map getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<>();
                        params.put("Content-Type", "application/json");
                        try {
                            params.put("basicAuthenticate", sessionInfo.getString("basicAuthenticate"));
                        } catch (JSONException jsonExcepion2) {
                            jsonExcepion2.printStackTrace();
                        }
                        return params;
                    }
                };
                mRequestQueue.add(jsonObjectRequest);
            }
        }
        catch (JSONException jsonException){
            jsonException.printStackTrace ();
        }
        final TextView sid = findViewById(R.id.subsid);
    }

    private void inflateview(JSONObject response) throws JSONException {
        if(response.getInt("totalCount")>0){
            JSONArray addValues = response.getJSONArray ("Data");
            for(int i=0;i<addValues.length ();i++){
                JSONObject useAddValue = addValues.getJSONObject (i);
                subslistval.add (new constructor_subslist (useAddValue.getString("shopName"), useAddValue.getString("itemName"), useAddValue.getString("subsStartDate"), useAddValue.getInt("subsDays"), useAddValue.getString("itemStatus"), useAddValue.getString("addOnName"), useAddValue.getDouble("cost"), useAddValue.getInt("subsDays"), useAddValue.getInt("subsDaysLeft"), useAddValue.getInt("subscriptIonId")));
            }
            progressBar.setVisibility (View.GONE);
            nestedView.setVisibility (View.VISIBLE);
            adapter_subsList adapter = new adapter_subsList(subslistval, this);
            RecyclerView addListView = findViewById (R.id.subslist);
            addListView.setHasFixedSize (false);
            addListView.setLayoutManager (new LinearLayoutManager(this));
            addListView.setNestedScrollingEnabled(false);
            addListView.setAdapter (adapter);
        }
        else{
            progressBar.setVisibility (View.GONE);
            noSubs.setVisibility (View.VISIBLE);
        }
    }

    @Override
    public void onBackPressed(){
        Intent intent = new Intent(this, accountPage.class);
        startActivity(intent);
        finish();
    }

    public void goToBack(View view){
        Intent intent = new Intent(this, accountPage.class);
        startActivity(intent);
        finish();
    }

    public void goToShops(View view){
        Intent intent = new Intent(this, homePage.class);
        startActivity(intent);
        finish();
    }

}
