package com.martjinni.userApp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class createNewAddress extends AppCompatActivity {
    String returnPage;
    volleyCall newClass = new volleyCall ();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_create_new_address);
        returnPage = (getIntent ().getStringExtra ("nextPage"));
        SharedPreferences sharedPreferences2 =  getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        if(sharedPreferences2.getString ("delVal","").equals ("")||sharedPreferences2.getString ("addressInform","").equals ("")){
            Intent intent = new Intent(this, defaultAddress.class);
            intent.putExtra("nextPage",returnPage);
            startActivity(intent);
            finish ();
        }
        else if(sharedPreferences2.getString ("sessionInfo","").equals ("")){
            Intent intent = new Intent(this, loginSignup.class);
            intent.putExtra("nextPage",returnPage);
            startActivity(intent);
            finish ();
        }
    }

    public void goBack(View v){
        if(returnPage.equals ("cart")){
            Intent intent = new Intent(this, homePage.class);
            startActivity(intent);
            finish ();
        }
        else{
            Intent intent = new Intent(this, manageAddress.class);
            intent.putExtra("nextPage",returnPage);
            startActivity(intent);
            finish ();
        }
    }

    @Override
    public void onBackPressed(){
        if(returnPage.equals ("cart")){
            Intent intent = new Intent(this, homePage.class);
            startActivity(intent);
            finish ();
        }
        else{
            Intent intent = new Intent(this, manageAddress.class);
            intent.putExtra("nextPage",returnPage);
            startActivity(intent);
            finish ();
        }
    }

    public void addNew(View v){
        SharedPreferences sharedPreferences2 =  getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        EditText addLine1 = findViewById (R.id.housenumber);
        EditText addLine2 = findViewById (R.id.streetadd);
        EditText addName = findViewById (R.id.addname);
        if(!addLine1.getText ().toString ().equals ("")){
            if(!addLine2.getText ().toString ().equals ("")){
                if(!addName.getText ().toString ().equals ("")){
                    try{
                        final JSONObject sessionInfo = new JSONObject (sharedPreferences2.getString ("sessionInfo",""));
                        JSONObject requestObject = new JSONObject (sharedPreferences2.getString ("addressInform",""));
                        requestObject.put ("userId",sessionInfo.getInt ("userId"));
                        String [] userInfo = sharedPreferences2.getString ("delVal","").split (",");
                        requestObject.put ("lat",Float.valueOf (userInfo[0]));
                        requestObject.put("lng",Float.valueOf (userInfo[1]));
                        requestObject.put("addName",String.valueOf (addName.getText ().toString ()));
                        requestObject.put("addLine1",String.valueOf (addLine1.getText ().toString ()));
                        requestObject.put("addLine2",String.valueOf (addLine2.getText ().toString ()));
                        Log.i ("WOW",String.valueOf (requestObject));
                        Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                        Network network = new BasicNetwork (new HurlStack ());
                        RequestQueue mRequestQueue = new RequestQueue (cache, network);
                        mRequestQueue.start();
                        String finalUrl = newClass.baseUrl+"addaddress";
                        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                proceedWithAddress (response);
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                error.printStackTrace ();
                            }
                        }){
                            @Override
                            public Map getHeaders() throws AuthFailureError {
                                Map<String,String> params = new HashMap<> ();
                                params.put("Content-Type", "application/json");
                                try{
                                    params.put("basicAuthenticate", sessionInfo.getString ("basicAuthenticate"));
                                }
                                catch (JSONException jsonExcepion2){
                                    jsonExcepion2.printStackTrace ();
                                }
                                return params;
                            }
                        };
                        mRequestQueue.add(jsonObjectRequest);
                    }
                    catch (JSONException jsonException){
                        jsonException.printStackTrace ();
                    }
                }
                else{
                    Toast toast = Toast.makeText (this,"Please name your address",Toast.LENGTH_SHORT);
                    toast.show ();
                }
            }
            else{
                Toast toast = Toast.makeText (this,"Please pass street locality",Toast.LENGTH_SHORT);
                toast.show ();
            }
        }
        else{
            Toast toast = Toast.makeText (this,"Please pass house number",Toast.LENGTH_SHORT);
            toast.show ();
        }
    }

    public void proceedWithAddress(JSONObject response){
        try{
            SharedPreferences sharedPreferences2 =  getSharedPreferences("userInfo", Context.MODE_PRIVATE);
            sharedPreferences2.edit ().putInt ("addId",response.getInt ("addressId")).apply ();
            sharedPreferences2.edit ().putString ("addressName",response.getString ("addName")).apply ();
            JSONObject addressInformation = new JSONObject (sharedPreferences2.getString ("addressInform",""));
            addressInformation.put ("addLine1",response.getString ("addressLine1"));
            addressInformation.put("addLine2",response.getString ("addressLine2"));
            sharedPreferences2.edit ().putString ("addressInformation",String.valueOf (addressInformation)).apply ();
            if(returnPage.equals ("cart")){
                Intent intent = new Intent(this, cartPage1.class);
                intent.putExtra("nextPage",returnPage);
                startActivity(intent);
                finish ();
            }
            else{
                Intent intent = new Intent(this, manageAddress.class);
                intent.putExtra("nextPage",returnPage);
                startActivity(intent);
                finish ();
            }
        }
        catch (JSONException jsonException){
            jsonException.printStackTrace ();
        }
    }

}
