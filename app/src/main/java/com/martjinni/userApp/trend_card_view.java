package com.martjinni.userApp;

public class trend_card_view {
    private String imgUrl, shopName, subText;
    private float rating;
    private int shopId_use;

    public trend_card_view(String imgUrl, String shopName, String subText, float rating, int shopId_use) {
        this.imgUrl = imgUrl;
        this.shopName = shopName;
        this.subText = subText;
        this.rating = rating;
        this.shopId_use = shopId_use;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public String getShopNameTrend() {
        return shopName;
    }

    public String getSubText() {
        return subText;
    }

    public float getRating() {
        return rating;
    }

    public int getShopId_use() {
        return shopId_use;
    }
}
