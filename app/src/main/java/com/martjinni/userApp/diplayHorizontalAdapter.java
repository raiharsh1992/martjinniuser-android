package com.martjinni.userApp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import java.util.List;

public class diplayHorizontalAdapter extends  RecyclerView.Adapter<diplayHorizontalAdapter.diplayHorizontalViewHolder>{

    private Context mCtx;
    private List<displayHorizontalClass> useList;

    public diplayHorizontalAdapter(Context mCtx, List<displayHorizontalClass> useList) {
        this.mCtx = mCtx;
        this.useList = useList;
    }

    @NonNull
    @Override
    public diplayHorizontalViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from (mCtx);
        View view = inflater.inflate (R.layout.displayhorizontaladdon,null);
        return new diplayHorizontalViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull diplayHorizontalViewHolder holder, int position) {
        displayHorizontalClass container = useList.get (position);
        holder.addonName.setText (container.getAddOnName ());
        displaySubAddOnListAdapter adapter = new displaySubAddOnListAdapter (mCtx,container.getInformation ());
        holder.horizontalList.setHasFixedSize (false);
        holder.horizontalList.setLayoutManager (new LinearLayoutManager (mCtx, LinearLayoutManager.HORIZONTAL, false));
        holder.horizontalList.setAdapter (adapter);
    }

    @Override
    public int getItemCount() {
        return useList.size ();
    }

    class diplayHorizontalViewHolder extends RecyclerView.ViewHolder{
        TextView addonName;
        RecyclerView horizontalList;
        public diplayHorizontalViewHolder(View itemView){
            super(itemView);
            addonName = itemView.findViewById (R.id.addOnName);
            horizontalList = itemView.findViewById (R.id.subAddOnList);
        }
    }
}
