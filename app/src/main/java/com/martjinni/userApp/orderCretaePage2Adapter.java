package com.martjinni.userApp;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class orderCretaePage2Adapter extends RecyclerView.Adapter<orderCretaePage2Adapter.orderCreatePage2ViewHolder>{

    volleyCall newClass = new volleyCall ();
    Context mCtx;
    List<orderCreatePage2Class> useList;

    public orderCretaePage2Adapter(Context mCtx, List<orderCreatePage2Class> useList) {
        this.mCtx = mCtx;
        this.useList = useList;
    }

    @NonNull
    @Override
    public orderCreatePage2ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from (mCtx);
        View view = inflater.inflate (R.layout.ordershop,null);
        return new orderCreatePage2ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull orderCreatePage2ViewHolder holder, int position) {
        orderCreatePage2Class container = useList.get (position);
        holder.shopName.setText (container.getShopName ());
        holder.itemCount.setText (String.valueOf (container.getTotalItem ()));
        holder.totalBill.setText (String.valueOf (container.getTotalBill ()));
        if(container.getPromoApplied ().equals ("yes")){
            holder.applyPromo.setText ("Applied!");
            holder.promoCode.setFocusable(false);
        }
    }

    @Override
    public int getItemCount() {
        return useList.size ();
    }

    class orderCreatePage2ViewHolder extends RecyclerView.ViewHolder{
        TextView shopName, itemCount, totalBill, applyPromo;
        EditText promoCode;
        public orderCreatePage2ViewHolder(final View itemView){
            super(itemView);
            shopName = itemView.findViewById (R.id.shopName);
            itemCount = itemView.findViewById (R.id.totalItemCount);
            totalBill = itemView.findViewById (R.id.totalBillAmount);
            applyPromo = itemView.findViewById (R.id.applyPromo);
            promoCode = itemView.findViewById (R.id.promocode);
            applyPromo.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition ();
                    String promoValue = promoCode.getText ().toString ();
                    applyPromoCode(position, promoValue, itemView);
                }
            });
        }
    }

    public void applyPromoCode(final int position, String promoCode, View itemView){
        try{
            SharedPreferences sharedPreferences = mCtx.getSharedPreferences("userInfo", Context.MODE_PRIVATE);
            orderCreatePage2Class container = useList.get (position);
            final JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
            JSONObject requestObject = new JSONObject ();
            requestObject.put("orderMasterId",container.getOrderMasterId ());
            requestObject.put("orderStoreId",container.getOrderStoreId ());
            requestObject.put("promoCode",promoCode);
            requestObject.put ("typeId",sessionInfo.getInt ("typeId"));
            requestObject.put("userId",sessionInfo.getInt ("userId"));
            requestObject.put ("userType",sessionInfo.getString ("userType"));
            Cache cache = new DiskBasedCache (mCtx.getCacheDir(), 1024 * 1024);
            Network network = new BasicNetwork (new HurlStack ());
            RequestQueue mRequestQueue = new RequestQueue (cache, network);
            mRequestQueue.start();
            requestObject.put("userId",sessionInfo.getInt ("userId"));
            String finalUrl = newClass.baseUrl+"applypromo";
            Log.i("Kya request h",String.valueOf (requestObject));
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    goToSuccess(response, position);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace ();
                }
            }){
                @Override
                public Map getHeaders() throws AuthFailureError {
                    Map<String,String> params = new HashMap<> ();
                    params.put("Content-Type", "application/json");
                    try{
                        params.put("basicAuthenticate", sessionInfo.getString ("basicAuthenticate"));
                    }
                    catch (JSONException jsonExcepion2){
                        jsonExcepion2.printStackTrace ();
                    }
                    return params;
                }
            };
            mRequestQueue.add(jsonObjectRequest);
        }
        catch (JSONException jsonException){
            jsonException.printStackTrace ();
        }
    }

    public void goToSuccess(JSONObject response, int position){
        try{
            if(response.getString ("Success").equals ("TRUE")){
                orderCreatePage2Class container = useList.get (position);
                float discountAmount = container.getTotalBill ()-Float.valueOf (String.valueOf (response.getDouble ("newStoreAmount")));
                container.setPromoApplied ("yes");
                container.setTotalBill (Float.valueOf (String.valueOf (response.getDouble ("newStoreAmount"))));
                useList.set (position,container);
                ((cartPage2)mCtx).updateDiscountAmount (discountAmount);
                ((cartPage2)mCtx).displayRecyler (useList);
            }
            else{
                Toast.makeText(mCtx,response.getString ("Reason"), Toast.LENGTH_LONG).show();
            }
        }
        catch (JSONException jsonException){
            jsonException.printStackTrace ();
        }
    }
}
