package com.martjinni.userApp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import static android.os.SystemClock.sleep;

public class contactus extends AppCompatActivity {

    public RequestQueue mRequestQueue;
    volleyCall newClass = new volleyCall();
    public String baseUrl = newClass.baseUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contactus);
        final EditText getname = findViewById(R.id.getname);
        final EditText getnumber = findViewById(R.id.getnumber);
        final EditText getemail = findViewById(R.id.getemail);
        final EditText getaddress = findViewById(R.id.getaddress);
        final EditText getmessage = findViewById(R.id.getmessage);
        final TextView action = findViewById(R.id.contctaction);
        action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = getname.getText().toString();
                String number = getnumber.getText().toString();
                String email = getemail.getText().toString();
                String address = getaddress.getText().toString();
                String message = getmessage.getText().toString();
                if(name.equals("") || number.equals("") || email.equals("") || address.equals("") || message.equals("")){
                    Toast.makeText(contactus.this, "Please fill all the fields", Toast.LENGTH_SHORT).show();
                }
                else {
                    try {

                        Cache cache = new DiskBasedCache(getCacheDir(),1024*1024);
                        Network network =  new BasicNetwork(new HurlStack());
                        mRequestQueue = new RequestQueue(cache, network);
                        mRequestQueue.start();
                        JSONObject myObj = new JSONObject();
                        myObj.put("userName", name);
                        myObj.put("userType", "CUST");
                        myObj.put("message", message);
                        myObj.put("phoneNumber", number);
                        myObj.put("address", address);
                        myObj.put("email", email);
                        String userUrl = baseUrl + "makequery";
                        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, userUrl, myObj, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                Toast.makeText(contactus.this,"Thanks for reaching to us. We will get back to you soon. ", Toast.LENGTH_LONG).show();
                                redirect();
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                                Toast.makeText(contactus.this, "This is error" + error.toString(), Toast.LENGTH_LONG).show();
                                error.printStackTrace();
                            }
                        });
                        mRequestQueue.add(jsonObjectRequest);
                    }
                    catch (JSONException jsonException){

                        jsonException.printStackTrace();
                    }


                }

            }
        });
    }
    public void redirect(){
        sleep(1000);
        Intent intent = new Intent(getApplicationContext(), accountPage.class);
        startActivity(intent);
        finish();

    }

    @Override
    public void onBackPressed(){
        redirect ();
    }
}
