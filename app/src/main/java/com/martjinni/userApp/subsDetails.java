package com.martjinni.userApp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class subsDetails extends AppCompatActivity {

    public RequestQueue mRequestQueue;
    public String baseUrl = new volleyCall ().baseUrl;
    List<constructor_subs_details> subsdetails = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_subs_details);
        Intent intent = getIntent();
        String sid = intent.getStringExtra("subsId");
        ImageView backButton = findViewById (R.id.backButton);
        backButton.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {

            }
        });
        try {
            SharedPreferences sharedPreferences2 = getSharedPreferences("userInfo", Context.MODE_PRIVATE);
            if (sharedPreferences2.getString("sessionInfo", "").equals("")) {
                Intent intent2 = new Intent(this, accountPage.class);
                startActivity(intent2);
                finish ();
            } else {

                Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork(new HurlStack());
                RequestQueue mRequestQueue = new RequestQueue(cache, network);
                mRequestQueue.start();
                final JSONObject sessionInfo = new JSONObject(sharedPreferences2.getString("sessionInfo", ""));
                JSONObject requestObject = new JSONObject();
                requestObject.put("typeId", sessionInfo.getInt("typeId"));
                requestObject.put("userId", sessionInfo.getInt("userId"));
                requestObject.put("subsId", sid);
                requestObject.put("userType", sessionInfo.getString("userType"));
                String finalUrl = baseUrl + "viesubdetails";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, finalUrl, requestObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            inflateview(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Toast.makeText(getApplicationContext(), "Response is Not here", Toast.LENGTH_LONG).show();
                    }
                }) {
                    @Override
                    public Map getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<>();
                        params.put("Content-Type", "application/json");
                        try {
                            params.put("basicAuthenticate", sessionInfo.getString("basicAuthenticate"));
                        } catch (JSONException jsonExcepion2) {
                            jsonExcepion2.printStackTrace();
                        }
                        return params;
                    }
                };
                mRequestQueue.add(jsonObjectRequest);
            }
        }
        catch (JSONException jsonException){
            jsonException.printStackTrace ();
        }


    }
    private void inflateview(JSONObject response) throws JSONException {
        if(response.getInt("totalCount")>0){
            JSONArray addValues = response.getJSONArray ("itemTranHistory");
            for(int i=0;i<addValues.length ();i++){
                JSONObject useAddValue = addValues.getJSONObject (i);
                subsdetails.add (new constructor_subs_details (useAddValue.getString("dayCount"), useAddValue.getString("dayEta"), useAddValue.getString("dayStatus")));
            }
            adapter_subs_details adapter = new adapter_subs_details(subsdetails, this);
            RecyclerView addListView = findViewById (R.id.subsdetailsrecycler);
            addListView.setHasFixedSize (false);
            addListView.setLayoutManager (new LinearLayoutManager(this));
            addListView.setNestedScrollingEnabled(false);
            addListView.setAdapter (adapter);
        }
        TextView shopName = findViewById (R.id.shopName);
        shopName.setText (response.getString ("shopName"));
        String [] useItemName = response.getString ("itemName").split ("@");
        String itemName = "Item name : "+useItemName[0];
        TextView itemNameView = findViewById (R.id.itemName);
        itemNameView.setText (itemName);
        String [] startDate = response.getString ("startDate").split ("T");
        String startDateuse = startDate[0];
        TextView startDateView = findViewById (R.id.startDate);
        startDateView.setText (startDateuse);
        TextView quantView = findViewById (R.id.quantView);
        String quantDisp = "Quantity : "+useItemName[1];
        quantView.setText (quantDisp);
        String totalAmount = "Total amount : Rs "+response.getDouble ("value");
        TextView totalAmountView = findViewById (R.id.totalAmount);
        totalAmountView.setText (totalAmount);
        TextView daysLeft = findViewById (R.id.daysLeft);
        daysLeft.setText (String.valueOf (response.getInt ("daysLeft")));
        TextView daysLeft1 = findViewById (R.id.daysWorked);
        daysLeft1.setText (String.valueOf (response.getInt ("totalCount")));
    }

    @Override
    public void onBackPressed(){
        Intent intent = new Intent(this, subsList.class);
        startActivity(intent);
        finish();
    }

    public void goToBack(View view){
        Intent intent = new Intent(this, subsList.class);
        startActivity(intent);
        finish();
    }

}
