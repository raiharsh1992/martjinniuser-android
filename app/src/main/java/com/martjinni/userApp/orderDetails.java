package com.martjinni.userApp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class orderDetails extends AppCompatActivity {
    int orderId;
    String returnPage;
    volleyCall newClass = new volleyCall ();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_order_details);
        returnPage = (getIntent ().getStringExtra ("nextPage"));
        orderId = Integer.valueOf ((getIntent ().getStringExtra ("orderId")));
        fetchOrderDetails();
    }

    public void submitratingreview(int masterId, int orderId, float rate, String reviewMark, String review) throws JSONException {
        SharedPreferences sharedPreferences =  getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        final JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
        JSONObject myObj = new JSONObject();
        myObj.put ("typeId",sessionInfo.getInt ("typeId"));
        myObj.put("userId",sessionInfo.getInt ("userId"));
        myObj.put ("userType",sessionInfo.getString ("userType"));
        myObj.put("orderMasterId",masterId);
        myObj.put("orderStoreId",orderId);
        myObj.put("rating",rate);
        myObj.put("reviewMark",reviewMark);
        myObj.put("review",review);
        Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024);
        Network network = new BasicNetwork(new HurlStack());
        RequestQueue mRequestQueue = new RequestQueue (cache, network);
        mRequestQueue.start();
        String finalUrl = newClass.baseUrl+"resigsterreview";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, finalUrl, myObj,  new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                fetchOrderDetails();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace ();
                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            public Map getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                try{
                    params.put("basicAuthenticate", sessionInfo.getString ("basicAuthenticate"));
                }
                catch (JSONException jsonExcepion2){
                    jsonExcepion2.printStackTrace ();
                }
                return params;
            }
        };
        mRequestQueue.add(jsonObjectRequest);
    }

    public void cancelorder(int masterId, int orderId) throws JSONException {
        Toast.makeText(this, "Button clicked again", Toast.LENGTH_LONG).show();
        JSONArray oid = new JSONArray();
        oid.put(orderId);
        int mid = masterId;
        SharedPreferences sharedPreferences =  getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        final JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
        JSONObject myObj = new JSONObject();
        myObj.put("orderId",mid);
        myObj.put("orderList",oid);
        myObj.put ("typeId",sessionInfo.getInt ("typeId"));
        myObj.put("userId",sessionInfo.getInt ("userId"));
        myObj.put ("userType",sessionInfo.getString ("userType"));
        Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
        Network network = new BasicNetwork (new HurlStack ());
        RequestQueue mRequestQueue = new RequestQueue (cache, network);
        mRequestQueue.start();
        String finalUrl = newClass.baseUrl+"cancelorder";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, finalUrl, myObj,  new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                fetchOrderDetails();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace ();
                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            public Map getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<> ();
                params.put("Content-Type", "application/json");
                try{
                    params.put("basicAuthenticate", sessionInfo.getString ("basicAuthenticate"));
                }
                catch (JSONException jsonExcepion2){
                    jsonExcepion2.printStackTrace ();
                }
                return params;
            }
        };
        mRequestQueue.add(jsonObjectRequest);

    }

    public void fetchOrderDetails(){
        try{
            SharedPreferences sharedPreferences =  getSharedPreferences("userInfo", Context.MODE_PRIVATE);
            final JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
            JSONObject requestObject = new JSONObject ();
            requestObject.put ("typeId",sessionInfo.getInt ("typeId"));
            requestObject.put("userId",sessionInfo.getInt ("userId"));
            requestObject.put ("userType",sessionInfo.getString ("userType"));
            requestObject.put("orderId",orderId);
            Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
            Network network = new BasicNetwork (new HurlStack ());
            RequestQueue mRequestQueue = new RequestQueue (cache, network);
            mRequestQueue.start();
            String finalUrl = newClass.baseUrl+"orderdetails";
            Log.i("Kya request h",String.valueOf (requestObject));
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    displayObjects(response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace ();
                }
            }){
                @Override
                public Map getHeaders() throws AuthFailureError {
                    Map<String,String> params = new HashMap<> ();
                    params.put("Content-Type", "application/json");
                    try{
                        params.put("basicAuthenticate", sessionInfo.getString ("basicAuthenticate"));
                    }
                    catch (JSONException jsonExcepion2){
                        jsonExcepion2.printStackTrace ();
                    }
                    return params;
                }
            };
            mRequestQueue.add(jsonObjectRequest);
        }
        catch (JSONException jsonException){
            jsonException.printStackTrace ();
        }
    }

    public void displayObjects(JSONObject response){
        try{
            String orderId = "Order Id : "+response.getInt ("orderId");
            String startDate = "Order Date : "+response.getString ("creationDate");
            TextView displayContent = findViewById (R.id.displayOrderId);
            displayContent.setText (orderId);
            displayContent.findViewById (R.id.addressName);
            displayContent.setText (response.getString ("addName"));
            displayContent = findViewById (R.id.displayOrderDate);
            displayContent.setText (startDate);
            displayContent = findViewById (R.id.addressValue);
            startDate = response.getString ("addLine1")+" , "+response.getString ("addLine2")+" , "+response.getString ("city")+" , "+response.getString ("state")+" , "+response.getString ("country");
            displayContent.setText (startDate);
            startDate = "Total sub orders : "+response.getInt ("totalShopCount");
            displayContent = findViewById (R.id.displayTotalSubOrderCount);
            displayContent.setText (startDate);
            JSONArray shopInfo = response.getJSONArray ("shopInfo");
            List<orderDetailsClass> insertItem = new ArrayList<> ();
            for(int i=0;i<shopInfo.length ();i++){
                List<diplayOrderDetailsItemClass> itemShopList = new ArrayList<> ();
                JSONObject intermediate = shopInfo.getJSONObject (i);
                JSONArray shopItem = intermediate.getJSONArray ("shopItems");
                for(int j=0;j<shopItem.length ();j++){
                    JSONObject useItNow = shopItem.getJSONObject (j);
                    int withSubs = 0;
                    int count = useItNow.optInt ("subsDays",0);
                    if(count>0){
                        withSubs = 1;
                    }
                    itemShopList.add (new diplayOrderDetailsItemClass (useItNow.getInt ("orderItemId"),withSubs,useItNow.getString ("itemName"),useItNow.getString ("addOnName"),String.valueOf (useItNow.getDouble ("amount")),response.getInt ("orderId")));
                }
                String shopAmount =""+intermediate.getDouble ("shopAmount");
                insertItem.add (new orderDetailsClass (intermediate.getString ("shopName"),intermediate.getString ("orderStatus"),shopAmount,itemShopList,intermediate.getInt ("shopOrderId"),response.getInt ("orderId"),intermediate.getString ("rating")));
            }
            RecyclerView displayShopInfo = findViewById (R.id.orderDetailsInfo);
            orderDetailsAdapter adapter = new orderDetailsAdapter (this, insertItem);
            displayShopInfo.setHasFixedSize (false);
            displayShopInfo.setNestedScrollingEnabled (false);
            displayShopInfo.setLayoutManager (new LinearLayoutManager (this));
            displayShopInfo.setAdapter (adapter);
        }
        catch (JSONException jsonException){
            jsonException.printStackTrace ();
        }
    }

    public void goBackHome(View view){
        if(returnPage.equals ("home")){
            Intent intent = new Intent(this, homePage.class);
            startActivity(intent);
            finish ();
        }
        else{
            Intent intent = new Intent(this, orderList.class);
            startActivity(intent);
            finish ();
        }
    }

    @Override
    public void onBackPressed()
    {
        ViewGroup view = findViewById (R.id.content);
        goBackHome (view);
        super.onBackPressed();  // optional depending on your needs
    }
}
