package com.martjinni.userApp;

public class addListClass {
    private String addName, addLine1, addLine2, city, state, country, pincode;
    private int addId;
    private float lat, lng;

    public addListClass(String addName, String addLine1, String addLine2, String city, String state, String country, String pincode, int addId, Double lat, Double lng) {
        this.addName = addName;
        this.addLine1 = addLine1;
        this.addLine2 = addLine2;
        this.city = city;
        this.state = state;
        this.country = country;
        this.pincode = pincode;
        this.addId = addId;
        this.lat = Float.valueOf(String.valueOf(lat));
        this.lng = Float.valueOf(String.valueOf(lng));
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public String getCountry() {
        return country;
    }

    public String getPincode() {
        return pincode;
    }

    public float getLat() {
        return lat;
    }

    public float getLng() {
        return lng;
    }

    public String getAddName() {
        return addName;
    }

    public String getAddLine1() {
        return addLine1;
    }

    public String getAddLine2() {
        return addLine2;
    }

    public int getAddId() {
        return addId;
    }
}
