package com.martjinni.userApp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class loginSignup extends AppCompatActivity {
    String returnPage;
    final getnumberfragment fnum = new getnumberfragment();
    final getloginpasswordfragment fpass = new getloginpasswordfragment();
    final getregisterstep1 freg1 = new getregisterstep1();
    final getregisterstep2 freg2 = new getregisterstep2();
    public RequestQueue mRequestQueue;
    volleyCall newClass = new volleyCall ();
    public String baseUrl = newClass.baseUrl;
    String number = "";
    String otp = "";
    String username ="";
    String password ="";
    String fullname="";
    String email = "";
    String userType = "CUST";
    int already = 0;
    public volatile boolean isWorking = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_signup);
        openFragment(fnum);
        returnPage = (getIntent ().getStringExtra ("nextPage"));
    }

    private void openFragment(final Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(android.R.anim.slide_out_right, android.R.anim.slide_in_left);
        transaction.replace(R.id.sessioncontainer, fragment, "FRAG");
        if(fragment == fnum){
        }
        else{
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }

    public void checknumber(String num, String status){
        number = num;
        already = 0;
        if(status.equals ("exist")){
            openFragment(fpass);
        }
        else{
            openFragment(freg1);
        }
    }

    public void redirectToNext(String method){
        if(returnPage.equals ("account")){
            Intent intent = new Intent(this, accountPage.class);
            startActivity(intent);
            finish ();
        }
        else if(returnPage.equals ("cart")){
            if(method.equals ("login")){
                Intent intent = new Intent(this, manageAddress.class);
                intent.putExtra("nextPage","cart");
                startActivity(intent);
                finish ();
            }
            else{
                Intent intent = new Intent(this, createNewAddress.class);
                intent.putExtra("nextPage","cart");
                startActivity(intent);
                finish ();
            }
        }
        else{
            Intent intent = new Intent(this, accountPage.class);
            startActivity(intent);
            finish ();
        }
    }

    public void validatereg1(String name, String uname, String pass1, String email1){
        fullname = name;
        username = uname;
        password = pass1;
        email = email1;
        generateOtp("hi");
        openFragment(freg2);
    }

    public void alertshow(String message){
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(loginSignup.this);
        View mView = getLayoutInflater().inflate(R.layout.alertdialog, null);
        TextView messageshow = mView.findViewById(R.id.errormessage);
        messageshow.setText(message);
        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        dialog.show();
        TextView close = mView.findViewById(R.id.modalclose);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
    }

    public String getNumber() {
        return number;
    }

    public void changeHead(String head){

        TextView headxml = (TextView) findViewById(R.id.head);
        headxml.setText(head);

    }

    public void redirect(){
        if(returnPage.equals ("account")){
            Intent intent = new Intent(loginSignup.this, accountPage.class);
            startActivity(intent);
            finish();
        }
        else{
            Intent intent = new Intent(loginSignup.this, homePage.class);
            intent.putExtra ("nextPage",returnPage);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onBackPressed(){
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag ("FRAG");
        if(fragment==fnum){
            redirect ();
        }
        else if (fragment==fpass){
            openFragment(fnum);
        }
        else if(fragment==freg1){
            openFragment (fnum);
        }
        else if(fragment==freg2){
            openFragment (freg1);
        }
    }

    public void onBackClick(View view){
        redirect ();
    }

    public void createuser(String otpval){
        if(isWorking){
            isWorking = false;
            otp=otpval;
            try {
                Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork(new HurlStack());
                mRequestQueue = new RequestQueue(cache, network);
                mRequestQueue.start();
                JSONObject jsonVal = new JSONObject();
                jsonVal.put("otp", otp);
                jsonVal.put("userNameLogin", username);
                jsonVal.put("password", password);
                jsonVal.put("phoneNumber", Double.parseDouble(number));
                jsonVal.put("custName", fullname);
                jsonVal.put("emailId", email);
                jsonVal.put("userType", userType);
                String userUrl = baseUrl + "createuser";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, userUrl, jsonVal, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        SharedPreferences sharedPreferences = getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString ("sessionInfo",response.toString ()).apply ();
                        applyNewToken runner = new applyNewToken();
                        new Thread (runner).start ();
                        redirectToNext ("signUp");
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        isWorking = true;
                        Toast.makeText(loginSignup.this,"Incorrect OTP passed", Toast.LENGTH_LONG).show();
                        error.printStackTrace();
                    }
                });
                mRequestQueue.add(jsonObjectRequest);
            } catch (JSONException jsonException) {
                jsonException.printStackTrace();
            }
        }
        else{
            Toast.makeText(loginSignup.this,"Please be patient, system already working", Toast.LENGTH_LONG).show();
        }
    }

    public void generateOtp(String from){
        if(from.equals ("retry")){
            workForCreateOtp ();
        }
        else{
            if(already==0){
                workForCreateOtp ();
            }
        }
    }

    public void workForCreateOtp(){
        isWorking = true;
        try {
            Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024);
            Network network = new BasicNetwork(new HurlStack());
            mRequestQueue = new RequestQueue(cache, network);
            mRequestQueue.start();
            JSONObject jsonVal = new JSONObject();
            jsonVal.put("phoneNumber", Double.parseDouble(number));
            jsonVal.put("userType", "CUST");
            jsonVal.put("userNeed", "SIGNUP");
            String userUrl = baseUrl + "generateotp";
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, userUrl, jsonVal, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Toast.makeText(loginSignup.this,"OTP sent to +91"+ String.valueOf (number), Toast.LENGTH_LONG).show();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });
            mRequestQueue.add(jsonObjectRequest);
        } catch (JSONException jsonException) {
            jsonException.printStackTrace();
        }
    }
    public void restorepass(){
        Intent intent = new Intent(getApplicationContext(), changepassword.class);
        intent.putExtra ("nextPage",returnPage);
        startActivity(intent);
        finish();
    }

    public void registerToken(){
        applyNewToken runner = new applyNewToken();
        new Thread (runner).start ();
    }

    class applyNewToken implements Runnable{
        @Override
        public void run(){
            try{
                SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                if(!sharedPreferences.getString ("sessionInfo","").equals ("")){
                    final JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
                    JSONObject requestObject = new JSONObject ();
                    String token = sharedPreferences.getString ("userMessageToken","");
                    if(!token.equals ("")){
                        requestObject.put ("userType",sessionInfo.getString ("userType"));
                        requestObject.put ("typeId",sessionInfo.getInt ("typeId"));
                        requestObject.put ("userId",sessionInfo.getInt ("userId"));
                        requestObject.put ("token",token);
                        Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                        Network network = new BasicNetwork (new HurlStack ());
                        RequestQueue mRequestQueue = new RequestQueue (cache, network);
                        mRequestQueue.start();
                        String finalUrl = newClass.baseUrl+"setusertoken";
                        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest (Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {

                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                error.printStackTrace ();
                            }
                        }){
                            @Override
                            public Map getHeaders() throws AuthFailureError {
                                Map<String,String> params = new HashMap<> ();
                                params.put("Content-Type", "application/json");
                                try{
                                    params.put("basicAuthenticate", sessionInfo.getString ("basicAuthenticate"));
                                }
                                catch (JSONException jsonExcepion2){
                                    jsonExcepion2.printStackTrace ();
                                }
                                return params;
                            }
                        };
                        mRequestQueue.add(jsonObjectRequest);
                    }
                }
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }
    }

}
