package com.martjinni.userApp;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class itemListAdapter extends RecyclerView.Adapter<itemListAdapter.itemListViewHolder>{

    private Context mCtx;
    private List<itemListClass> itemListItem;
    private int workingMaster=0;
    public itemListAdapter(Context mCtx, List<itemListClass> itemListItem) {
        this.mCtx = mCtx;
        this.itemListItem = itemListItem;
    }

    @NonNull
    @Override
    public itemListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from (mCtx);
        View view = inflater.inflate (R.layout.shopitemlist,null);
        return new itemListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull itemListViewHolder holder, int position) {
        itemListClass object = itemListItem.get (position);
        holder.menuName.setText (object.getItemName ());
        holder.subDetailCard.setHasFixedSize (false);
        holder.subDetailCard.setLayoutManager (new LinearLayoutManager (mCtx));
        String tagValueSub = "subItem"+object.getMasterId ();
        holder.subDetailCard.setTag (tagValueSub);
        String tagValue = "mainItem"+object.getMasterId ();
        holder.displaySubItems.setTag (tagValue);
    }

    @Override
    public int getItemCount() {
        return itemListItem.size ();
    }

    class itemListViewHolder extends RecyclerView.ViewHolder{
        TextView menuName;
        RecyclerView subDetailCard;
        LinearLayout displaySubItems;
        public itemListViewHolder(View itemView){
            super(itemView);
            menuName=itemView.findViewById (R.id.menuName);
            subDetailCard=itemView.findViewById (R.id.subDetailCard);
            displaySubItems=itemView.findViewById (R.id.displayItem);
            itemView.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition ();
                    handleClickItems(position);
                }
            });
        }
    }

    public void handleClickItems(int position){
        itemListClass object = itemListItem.get (position);
        String tagValue = "mainItem"+object.getMasterId ();
        View rootView = ((Activity)mCtx).findViewById(R.id.content);
        if(workingMaster==0){
            LinearLayout displaySubItem = rootView.findViewWithTag (tagValue);
            displaySubItem.setVisibility (View.VISIBLE);
            workingMaster = object.getMasterId ();
            displaySubItem(object.getMasterId ());
        }
        else if(workingMaster==object.getMasterId ()){
            LinearLayout displaySubItem = rootView.findViewWithTag (tagValue);
            displaySubItem.setVisibility (View.GONE);
            workingMaster = 0;
            displaySubItem(object.getMasterId ());
        }
        else{
            LinearLayout displaySubItem = rootView.findViewWithTag (tagValue);
            displaySubItem.setVisibility (View.VISIBLE);
            String newTag = "mainItem"+workingMaster;
            LinearLayout hideSubItem = rootView.findViewWithTag (newTag);
            hideSubItem.setVisibility (View.GONE);
            workingMaster = object.getMasterId ();
        }
        ((itemsPage)mCtx).setCurrentOpenTab (workingMaster);
    }

    public void displaySubItem(int mainItemId){
        Cursor itemInfo = getItemMasterDetails (mainItemId);
        itemInfo.moveToFirst ();
        List<itemCardClass> itemCardList = new ArrayList<> ();
        while(!itemInfo.isAfterLast ()){
            if((itemInfo.getInt (7)>0)&&(itemInfo.getInt (12)>0)){
                String displayPrice = "";
                if(itemInfo.getInt (9)>0){
                    displayPrice = "Starting from Rs. "+itemInfo.getInt (12);
                }
                else{
                    displayPrice = "Selling at Rs. "+itemInfo.getInt (12);
                }
                itemCardList.add (new itemCardClass (itemInfo.getString (1),displayPrice,itemInfo.getInt(0),itemInfo.getString (2),itemInfo.getInt (10),itemInfo.getInt (9)));
            }
            itemInfo.moveToNext ();
        }
        String tagValueSub = "subItem"+mainItemId;
        View rootView = ((Activity)mCtx).findViewById(R.id.content);
        RecyclerView displayView = rootView.findViewWithTag (tagValueSub);
        itemCardAdapter adapter = new itemCardAdapter (mCtx,itemCardList);
        displayView.setAdapter (adapter);
    }

    public Cursor getItemMasterDetails(int masterId){
        SQLiteDatabase myDatabase = mCtx.openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        return myDatabase.rawQuery ("SELECT * from itemListMaster where mainItemId='"+masterId+"'",null);
    }
}
