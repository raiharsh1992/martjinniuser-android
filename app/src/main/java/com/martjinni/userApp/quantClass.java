package com.martjinni.userApp;

public class quantClass {
    private int itemId, quantId, valueQuant, gst, mrp;
    private String quantName;

    public quantClass(int itemId, int quantId, int valueQuant, int gst, int mrp, String quantName) {
        this.itemId = itemId;
        this.quantId = quantId;
        this.valueQuant = valueQuant;
        this.gst = gst;
        this.mrp = mrp;
        this.quantName = quantName;
    }

    public int getItemId() {
        return itemId;
    }

    public int getQuantId() {
        return quantId;
    }

    public int getValueQuant() {
        return valueQuant;
    }

    public int getGst() {
        return gst;
    }

    public int getMrp() {
        return mrp;
    }

    public String getQuantName() {
        return quantName;
    }
}
