package com.martjinni.userApp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class eachItemAdapter extends RecyclerView.Adapter<eachItemAdapter.eachItemViewHolder>{

    private Context mCtx;
    private List<eachItemCardClass> itemInformation;
    private View mView;
    private AlertDialog dialog;

    public eachItemAdapter(Context mCtx, List<eachItemCardClass> itemInformation, View mView, AlertDialog dialog) {
        this.mCtx = mCtx;
        this.itemInformation = itemInformation;
        this.mView = mView;
        this.dialog = dialog;
    }

    @NonNull
    @Override
    public eachItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from (mCtx);
        View view = inflater.inflate (R.layout.eachitemdetail,null);
        return new eachItemViewHolder (view);
    }

    @Override
    public void onBindViewHolder(@NonNull eachItemViewHolder holder, int position) {
        eachItemCardClass container = itemInformation.get (position);
        holder.itemNameDisplay.setText (container.getItemName ());
        holder.unitName.setText (container.getUnitInfo ());
        if(container.getSubsDayCount ()>0){
            String displayText = "Subscription start date : "+container.getSusbStartDate ();
            holder.subsStartDate.setText (displayText);
            displayText = "No. of days : "+container.getSubsDayCount ();
            holder.subsNoOfDays.setText (displayText);
            displayText = "Subscription fees : "+container.getSubsPrice ();
            holder.subsPrice.setText (displayText);
            holder.subscriptionInformation.setVisibility (View.VISIBLE);
        }
        if(container.getInformation ().size ()>0){
            diplayHorizontalAdapter adapter = new diplayHorizontalAdapter (mCtx,container.getInformation ());
            holder.displayAddon.setVisibility (View.VISIBLE);
            holder.displayAddon.setHasFixedSize (false);
            holder.displayAddon.setLayoutManager (new LinearLayoutManager (mCtx));
            holder.displayAddon.setAdapter (adapter);
        }
        String quantTag = "quantity"+container.getCartId ();
        holder.quantInfor.setText (String.valueOf (container.getBaseUnits ()));
        holder.quantInfor.setTag (quantTag);
        float price = container.getBaseUnits ()*container.getPerUnitPrice ()+container.getSubsPrice ();
        String displayName = "Rs : "+price;
        holder.itemTotal.setText (displayName);
    }

    @Override
    public int getItemCount() {
        return itemInformation.size ();
    }

    class eachItemViewHolder extends RecyclerView.ViewHolder{
        TextView itemNameDisplay, unitName, subsStartDate, subsNoOfDays, subsPrice, quantInfor, itemTotal;
        LinearLayout subscriptionInformation;
        ImageView plusButton, minusButton;
        RecyclerView displayAddon;
        public eachItemViewHolder(final View itemView){
            super(itemView);
            itemNameDisplay = itemView.findViewById (R.id.itemNameSub);
            unitName = itemView.findViewById (R.id.unitDetails);
            displayAddon = itemView.findViewById (R.id.displayAddOnInfo);
            subsStartDate = itemView.findViewById (R.id.subsStartDate);
            subsNoOfDays = itemView.findViewById (R.id.subsNoOfDays);
            quantInfor = itemView.findViewById (R.id.itemCount);
            subsPrice = itemView.findViewById (R.id.subsPrice);
            subscriptionInformation = itemView.findViewById (R.id.subsDetailsView);
            minusButton = itemView.findViewById (R.id.minusClick);
            plusButton = itemView.findViewById (R.id.plusClick);
            itemTotal = itemView.findViewById (R.id.itemTotal);
            minusButton.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition ();
                    deductFromItem(position,itemView);
                }
            });
            plusButton.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition ();
                    incrementFromItem (position,itemView);
                }
            });
        }
    }

    public void deductFromItem(int position, View itemView){
        eachItemCardClass contaier = itemInformation.get (position);
        Cursor cartInfo = getCartInfo (contaier.getCartId ());
        cartInfo.moveToFirst ();
        float incrementPrice = cartInfo.getFloat (9);
        ((itemsPage)mCtx).decrementCountView (cartInfo.getInt (2));
        if(cartInfo.getInt (1)>1){
            int cartCount = cartInfo.getInt(1);
            cartCount = cartCount-1;
            contaier.setBaseUnits (cartCount);
            itemInformation.set (position,contaier);
            updateCartCount (cartInfo.getInt (0),cartCount);
            incrementPrice = incrementPrice*cartCount+contaier.getSubsPrice ();
            TextView itemTotal = itemView.findViewById (R.id.itemTotal);
            String DisplayPrice = "Rs : "+incrementPrice;
            itemTotal.setText (DisplayPrice);
            itemTotal = itemView.findViewById (R.id.itemCount);
            itemTotal.setText (String.valueOf (cartCount));
        }
        else{
            int cartCount = cartInfo.getInt(1);
            cartCount = cartCount-1;
            updateCartCount (cartInfo.getInt (0),cartCount);
            itemInformation.remove (position);
            if(itemInformation.size ()>0){
                eachItemAdapter adapter = new eachItemAdapter(mCtx,itemInformation,mView,dialog);
                RecyclerView quantPrint = mView.findViewById (R.id.displayQuantInfoFinal);
                quantPrint.setHasFixedSize (false);
                quantPrint.setLayoutManager (new LinearLayoutManager (mCtx));
                quantPrint.setAdapter (adapter);
            }
            else{
                dialog.dismiss ();
            }
        }
    }

    public void incrementFromItem(int position, View itemView){
        eachItemCardClass contaier = itemInformation.get (position);
        Cursor cartInfo = getCartInfo (contaier.getCartId ());
        cartInfo.moveToFirst ();
        int cartCount = cartInfo.getInt(1);
        cartCount = cartCount+1;
        float incrementPrice = cartInfo.getFloat (9);
        updateTotalValue (incrementPrice);
        updateCartCount (cartInfo.getInt (0),cartCount);
        incrementPrice = incrementPrice*cartCount+contaier.getSubsPrice ();
        TextView itemTotal = itemView.findViewById (R.id.itemTotal);
        String DisplayPrice = "Rs : "+incrementPrice;
        itemTotal.setText (DisplayPrice);
        itemTotal = itemView.findViewById (R.id.itemCount);
        itemTotal.setText (String.valueOf (cartCount));
        incrementMainView(cartInfo.getInt (2));
    }

    public void incrementMainView(int itemId){
        int itemQuant = getItemCount (itemId);
        updateItemCount (itemId,itemQuant+1);
        ((itemsPage)mCtx).updateDisplayView(itemQuant,itemId);
    }


    public Cursor getCartInfo(int cartId){
        SQLiteDatabase myDatabase = mCtx.openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        Cursor resultSet = myDatabase.rawQuery ("SELECT * from cartInfo where cartId='"+cartId+"'",null);
        return resultSet;
    }

    public void updateTotalValue(float addAmount){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        float totalAmount = sharedPreferences.getFloat ("totalBill",0.0f);
        totalAmount = totalAmount + addAmount;
        SharedPreferences.Editor editor = sharedPreferences.edit();
        View rootView = ((Activity)mCtx).findViewById(R.id.content);
        if(sharedPreferences.getFloat ("totalBill",0.0f)==0.0f){
            TextView showAmount = rootView.findViewById (R.id.totalSum);
            String amount = "Rs : "+totalAmount;
            showAmount.setText (amount);
            CardView showNavigation = rootView.findViewById (R.id.displayNavigation);
            showNavigation.setVisibility (View.VISIBLE);
        }
        else{
            TextView showAmount = rootView.findViewById (R.id.totalSum);
            String amount = "Rs : "+totalAmount;
            showAmount.setText (amount);
        }
        editor.putFloat ("totalBill",totalAmount).apply ();
    }

    public void updateTotalValueNegative(float addAmount){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        float totalAmount = sharedPreferences.getFloat ("totalBill",0.0f);
        totalAmount = totalAmount - addAmount;
        SharedPreferences.Editor editor = sharedPreferences.edit();
        View rootView = ((Activity)mCtx).findViewById(R.id.content);
        if(totalAmount==0.0f){
            TextView showAmount = rootView.findViewById (R.id.totalSum);
            String amount = "Rs : "+totalAmount;
            showAmount.setText (amount);
            CardView showNavigation = rootView.findViewById (R.id.displayNavigation);
            showNavigation.setVisibility (View.GONE);
        }
        else{
            TextView showAmount = rootView.findViewById (R.id.totalSum);
            String amount = "Rs : "+totalAmount;
            showAmount.setText (amount);
        }
        editor.putFloat ("totalBill",totalAmount).apply ();
    }

    public int getItemCount(int itemId){
        SQLiteDatabase myDatabase = mCtx.openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        Cursor resultSet = myDatabase.rawQuery ("SELECT itemCount from itemListMaster where itemId='"+itemId+"'",null);
        resultSet.moveToFirst ();
        int returnInt = resultSet.getInt (0);
        myDatabase.close ();
        return  returnInt;
    }

    public void updateItemCount(int itemId, int quant){
        SQLiteDatabase myDatabase = mCtx.openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        myDatabase.execSQL ("UPDATE itemListMaster set itemCount='"+quant+"' where itemId='"+itemId+"'");
        myDatabase.close ();
    }

    public void updateCartCount(int cartId, int quant){
        SQLiteDatabase myDatabase = mCtx.openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        if(quant>0){
            myDatabase.execSQL ("UPDATE cartInfo set count='"+quant+"' where cartId='"+cartId+"'");
        }
        else{
            myDatabase.execSQL ("delete from cartInfo where cartId='"+cartId+"'");
        }
        myDatabase.close ();
    }

    public int itemHasImage(int itemId){
        SQLiteDatabase myDatabase = mCtx.openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        Cursor resultSet = myDatabase.rawQuery ("SELECT hasImage from itemListMaster where itemId='"+itemId+"'",null);
        resultSet.moveToFirst ();
        int returnInt = resultSet.getInt (0);
        myDatabase.close ();
        return  returnInt;
    }
}
