package com.martjinni.userApp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import java.util.List;

public class cart1ShopAdapter extends RecyclerView.Adapter<cart1ShopAdapter.cart1ShopViewHolder>{

    Context mCtx;
    List<cart1ShopClass> useInfo;

    public cart1ShopAdapter(Context mCtx, List<cart1ShopClass> useInfo) {
        this.mCtx = mCtx;
        this.useInfo = useInfo;
    }

    @NonNull
    @Override
    public cart1ShopViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from (mCtx);
        View view = inflater.inflate (R.layout.cart1_itemlist,null);
        return new cart1ShopViewHolder (view);
    }

    @Override
    public void onBindViewHolder(@NonNull cart1ShopViewHolder holder, int position) {
        cart1ShopClass container = useInfo.get (position);
        holder.shopName.setText (container.getShopName ());
        String interim = "Rs. "+container.getShopCost ();
        holder.shopCost.setText (interim);
        interim ="Rs. "+container.getShopGst ();
        holder.shopGst.setText (interim);
        interim = "Rs. "+container.getShopDel ();
        holder.shopDel.setText (interim);
        float total = container.getShopCost ()+container.getShopGst ()+container.getShopDel ();
        interim = "Rs. "+total;
        holder.shopTotal.setText (interim);
        cart1ItemAdapter adapter = new cart1ItemAdapter (mCtx,container.getItemInformation ());
        holder.itemList.setHasFixedSize (false);
        holder.itemList.setLayoutManager (new LinearLayoutManager (mCtx));
        holder.itemList.setAdapter (adapter);
    }

    @Override
    public int getItemCount() {
        return useInfo.size ();
    }

    class cart1ShopViewHolder extends RecyclerView.ViewHolder{
        TextView shopName, shopCost, shopGst, shopDel, shopTotal;
        RecyclerView itemList;
        cart1ShopViewHolder(View itemView){
            super(itemView);
            shopName = itemView.findViewById (R.id.shopName);
            shopCost = itemView.findViewById (R.id.shopCost);
            shopGst = itemView.findViewById (R.id.shopGst);
            shopDel = itemView.findViewById (R.id.shopDel);
            shopTotal = itemView.findViewById (R.id.shopTotal);
            itemList = itemView.findViewById (R.id.displayItemInfo);
        }
    }
}
