package com.martjinni.userApp;


public class itemListClass {
    String itemName;
    int masterId;

    public itemListClass(String itemName, int masterId) {
        this.itemName = itemName;
        this.masterId = masterId;
    }

    public int getMasterId() {
        return masterId;
    }

    public String getItemName() {
        return itemName;
    }

}
