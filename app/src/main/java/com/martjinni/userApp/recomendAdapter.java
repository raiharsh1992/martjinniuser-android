package com.martjinni.userApp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import java.io.IOException;
import java.net.URL;
import java.util.List;

public class recomendAdapter extends RecyclerView.Adapter<recomendAdapter.recomendViewHolder> {

    private Context mCtx;
    private List<recomendClass> recomenList;

    public recomendAdapter(Context mCtx, List<recomendClass> recomenList) {
        this.mCtx = mCtx;
        this.recomenList = recomenList;
    }

    @NonNull
    @Override
    public recomendViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from (mCtx);
        View view = inflater.inflate (R.layout.recomendedcard,null);
        return new recomendViewHolder (view);
    }

    @Override
    public void onBindViewHolder(@NonNull recomendViewHolder holder, int position) {
        recomendClass objectUse = recomenList.get (position);
        holder.itemName.setText (objectUse.getHeadText ());
        holder.subText.setText (objectUse.getSubText ());
        holder.priceText.setText (objectUse.getPriceText ());
        BitmapDrawable background = changeItemImage(objectUse.getImgUrl ());
        holder.defaultImage.setBackground (background);
        if(objectUse.getSpclCat ().equals ("veg")){
            holder.sclCat.setVisibility (View.VISIBLE);
        }
        else if(objectUse.getSpclCat ().equals ("nonveg")){
            holder.sclCat.setImageResource (R.mipmap.ic_nonveg);
            holder.sclCat.setVisibility (View.VISIBLE);
        }
        String plusButTag = "plusButRecomend"+objectUse.getItemId ();
        String visTag = "visRecomend"+objectUse.getItemId ();
        holder.layoutUse.setTag (plusButTag);
        holder.addButton.setTag (visTag);
        holder.quantValue.setText (String.valueOf (objectUse.getCount ()));
        holder.quantValue.setTag ("recomenQuant"+objectUse.getItemId ());
        if(objectUse.getCount ()>0){
            holder.addButton.setVisibility (View.GONE);
            holder.layoutUse.setVisibility (View.VISIBLE);
        }
        else{
            holder.addButton.setVisibility (View.VISIBLE);
            holder.layoutUse.setVisibility (View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return recomenList.size ();
    }

    class recomendViewHolder extends RecyclerView.ViewHolder{
        ImageView sclCat,defaultImage,plusButton,minusButton;
        LinearLayout layoutUse;
        TextView itemName, subText, priceText, addButton, quantValue;
        public recomendViewHolder(final View itemView){
            super(itemView);
            sclCat=itemView.findViewById (R.id.spclCatRecomend);
            defaultImage = itemView.findViewById (R.id.itemImageRecomend);
            plusButton = itemView.findViewById (R.id.plusClickRecomend);
            minusButton = itemView.findViewById (R.id.minusClickRecomend);
            layoutUse = itemView.findViewById (R.id.plusMinusLayoutRecomend);
            itemName = itemView.findViewById (R.id.itemNameRecomend);
            subText = itemView.findViewById (R.id.subTextRecomend);
            priceText = itemView.findViewById (R.id.priceRecomend);
            addButton = itemView.findViewById (R.id.addButtonRecomend);
            quantValue = itemView.findViewById (R.id.itemCountRecomend);
            addButton.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition ();
                    recomendClass container = recomenList.get (position);
                    ((itemsPage)mCtx).workWithItemPlus(container.getItemId ());
                }
            });
            plusButton.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition ();
                    recomendClass container = recomenList.get (position);
                    ((itemsPage)mCtx).addExistingOrder (container.getItemId ());
                }
            });
            minusButton.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition ();
                    recomendClass container = recomenList.get (position);
                    ((itemsPage)mCtx).deductExistingOrder (container.getItemId ());
                }
            });
        }
    }

    public BitmapDrawable changeItemImage(String shopUrl){
        try{
            URL imageUrl = new URL(shopUrl);
            Bitmap bitmap = BitmapFactory.decodeStream(imageUrl.openConnection().getInputStream());
            int height = bitmap.getHeight();
            int width = bitmap.getWidth();
            final int IMAGE_MAX_SIZE = 1200000; // 1.2MP
            double y = Math.sqrt(IMAGE_MAX_SIZE
                    / (((double) width) / height));
            double x = (y / height) * width;
            Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, (int) x,
                    (int) y, true);
            return new BitmapDrawable(mCtx.getResources(), scaledBitmap);
        }
        catch (IOException e2){
            e2.printStackTrace ();
            return  null;
        }
    }

}
