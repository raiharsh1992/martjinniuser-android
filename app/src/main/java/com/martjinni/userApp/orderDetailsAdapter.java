package com.martjinni.userApp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;

import java.util.List;

public class orderDetailsAdapter extends RecyclerView.Adapter<orderDetailsAdapter.orderDetailsViewHolder>{

    Context mCtx;
    List<orderDetailsClass> userInfo;

    public orderDetailsAdapter(Context mCtx, List<orderDetailsClass> userInfo) {
        this.mCtx = mCtx;
        this.userInfo = userInfo;
    }

    @NonNull
    @Override
    public orderDetailsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from (mCtx);
        View view = inflater.inflate (R.layout.order_details_shop_info,null);
        return new orderDetailsViewHolder (view);
    }

    @Override
    public void onBindViewHolder(@NonNull final orderDetailsViewHolder holder, int position) {
        final orderDetailsClass container = userInfo.get (position);
        holder.shopName.setText (container.getShopName ());
        holder.shopName.setTag(container.getOrderMasterId());
        holder.orderAmount.setText (("\u20b9" + container.getAmount ()));
        diplayOrderDetailsItemAdapter adapter = new diplayOrderDetailsItemAdapter (mCtx, container.getUseInfo ());
        holder.displayItemList.setHasFixedSize (false);
        holder.displayItemList.setLayoutManager (new LinearLayoutManager (mCtx));
        holder.displayItemList.setAdapter (adapter);
        if(container.getOrderStatus ().equals ("NEW")){
            holder.cancelOrder.setVisibility (View.VISIBLE);
            holder.cancelOrder.setTag(container.getOrderStoreId());
        }
        else if(container.getOrderStatus ().equals ("ACCEPTED")){
            holder.progressBar1.setProgress (100);
            holder.orderAccept.setImageResource (R.drawable.handshakecolor);
        }
        else if(container.getOrderStatus ().equals ("WORKING")){
            holder.progressBar1.setProgress (100);
            holder.orderAccept.setImageResource (R.drawable.handshakecolor);
            holder.progressBar2.setProgress (100);
            holder.orderIntransit.setImageResource (R.drawable.intransitcolor);
        }
        else if(container.getOrderStatus().equals("CANCELED")){
                holder.ordercancelstate.setVisibility(View.VISIBLE);
                holder.statusbar.setVisibility(View.GONE);
        }
        else{
            holder.progressBar1.setProgress (100);
            holder.orderAccept.setImageResource (R.drawable.handshakecolor);
            holder.progressBar2.setProgress (100);
            holder.orderIntransit.setImageResource (R.drawable.intransitcolor);
            holder.progressBar3.setProgress (100);
            holder.orderDelivered.setImageResource (R.drawable.deliveredcolor);
            if(container.getRating ().equals ("0.00")){
                holder.rateOrder.setVisibility (View.VISIBLE);
            }
        }
        holder.cancelOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(mCtx);
                View mView = ((Activity) mCtx).getLayoutInflater().inflate(R.layout.dialog_boolean, null);
                mBuilder.setView(mView);
                final AlertDialog dialog = mBuilder.create();
                dialog.show ();
                Button yes = mView.findViewById(R.id.yes);
                Button no = mView.findViewById(R.id.no);
                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            ((orderDetails)mCtx).cancelorder(container.getOrderMasterId(), container.getOrderStoreId());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        dialog.dismiss ();
                    }
                });
                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss ();
                    }
                });

            }
        });
        holder.rateOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder mBuilder = new AlertDialog.Builder(mCtx);
                View mView = ((Activity) mCtx).getLayoutInflater().inflate(R.layout.ratingreview, null);
                mBuilder.setView(mView);
                final AlertDialog dialog = mBuilder.create();
                dialog.show ();
               final RatingBar mBar = mView.findViewById(R.id.myratingbar);
               final EditText review = mView.findViewById(R.id.reviewbox);
              final String rev = review.toString();
                TextView submitbtn = mView.findViewById(R.id.submitbtn);
                submitbtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(mBar.getRating()==0){
                            Toast.makeText(mCtx, "Please Enter a Valid review", Toast.LENGTH_LONG).show();
                        }

                        else {
                            String reviewMark = "YES";

                            if(rev.equals("")){
                                reviewMark = "NO";

                            }


                            try { dialog.dismiss();
                                ((orderDetails)mCtx).submitratingreview(container.getOrderMasterId(), container.getOrderStoreId(), mBar.getRating(), reviewMark, rev);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return userInfo.size ();
    }

    class orderDetailsViewHolder extends RecyclerView.ViewHolder{
        TextView shopName, orderAmount, rateOrder, cancelOrder;
              LinearLayout  ordercancelstate;
        ImageView orderAccept, orderIntransit, orderDelivered;
        ProgressBar progressBar1, progressBar2,progressBar3;
        LinearLayout statusbar;
        RecyclerView displayItemList;

        public orderDetailsViewHolder(View itemView){
            super(itemView);
            shopName = itemView.findViewById (R.id.shopName);
            displayItemList = itemView.findViewById (R.id.displayItemList);
            rateOrder = itemView.findViewById (R.id.rateOrder);
            cancelOrder = itemView.findViewById (R.id.orderCancel);
            orderAmount = itemView.findViewById (R.id.subOrderAmount);
            orderDelivered = itemView.findViewById (R.id.orderDelivered);
            progressBar3 = itemView.findViewById (R.id.progressBar3);
            progressBar2 = itemView.findViewById (R.id.progressBar2);
            progressBar1 = itemView.findViewById (R.id.progressBar1);
            orderAccept = itemView.findViewById (R.id.orderAccept);
            orderIntransit = itemView.findViewById (R.id.orderIntransit);
            statusbar = itemView.findViewById(R.id.statusbar);
            ordercancelstate = itemView.findViewById(R.id.orderCancelstate);
        }
    }
}
