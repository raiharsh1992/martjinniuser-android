package com.martjinni.userApp;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.StrictMode;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;



public class itemsPage extends AppCompatActivity {
    public RequestQueue mRequestQueue;
    public int shopId, dispRecomend = 0;
    volleyCall newClass = new volleyCall ();
    public String baseUrl = newClass.baseUrl;
    public SharedPreferences sharedPreferences;
    public String droppedFrom;
    List<itemListClass> itemListObject = new ArrayList<> ();
    List<recomendClass> recomendListObject = new ArrayList<> ();
    Context mCtx;
    int currentOpenTab = 0;
    public ProgressBar preloader;
    public void setCurrentOpenTab(int currentOpenTab) {
        this.currentOpenTab = currentOpenTab;
    }
    LinearLayout noShop1;
    private static final String TAG = "itemsPage";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mCtx = this;
        sharedPreferences = getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        droppedFrom = sharedPreferences.getString ("viewType","");
        super.onCreate (savedInstanceState);
        setContentView (R.layout.shop_item_page);
        noShop1 = findViewById (R.id.noShop1);
        preloader = findViewById (R.id.progressBar);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        shopId = Integer.parseInt (getIntent ().getStringExtra ("shopId"));
        ImageView backButton = findViewById (R.id.btndown);
        backButton.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                goToPrevious ();
            }
        });
        if(initializeDb ()){
            displayingShopBasic runForShop = new displayingShopBasic ();
            new Thread (runForShop).start ();
        }
        else{
            goToPrevious ();
        }
        float totalSum = sharedPreferences.getFloat ("totalBill",0.0f);
        if(totalSum!=0.0f){
            TextView showAmount = findViewById (R.id.totalSum);
            String amount = "\u20B9"+totalSum;
            showAmount.setText (amount);
            CardView showNavigation = findViewById (R.id.displayNavigation);
            showNavigation.setVisibility (View.VISIBLE);
        }
    }

    /*
    * Big functions section define all the mamoth task functions here
    * */

    class displayingShopBasic implements Runnable{
        @Override
        public void run(){
            try{
                Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork (new HurlStack ());
                sharedPreferences = getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                mRequestQueue = new RequestQueue (cache, network);
                mRequestQueue.start();
                JSONArray shops = new JSONArray ();
                shops.put (shopId);
                String userUrl = baseUrl+"getshopinfo";
                JSONObject jsonVal = new JSONObject ();
                jsonVal.put ("shops",shops);
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, userUrl, jsonVal,  new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        addingItemToList runForItem = new addingItemToList();
                        new Thread (runForItem).start ();
                        workIngForDisplay (response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        error.printStackTrace ();
                        onStop ();
                    }
                });
                mRequestQueue.add(jsonObjectRequest);
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();

            }
        }

        private void workIngForDisplay(JSONObject response){
            try{
                if(response.getInt ("count")>0){
                    JSONArray useInfo = response.getJSONArray ("Data");
                    for(int i =0;i<useInfo.length ();i++){
                        final JSONObject shopJson = useInfo.getJSONObject (i);
                        String shopUrl;
                        if(shopJson.getInt ("imageUploaded")==1){
                            shopUrl = newClass.shopUrl+shopJson.getInt ("shopId")+".jpeg";
                        }
                        else{
                            shopUrl = "https://www.martjinni.com/images/noImage.gif";
                        }
                        changeShopImage(shopUrl);
                        final float useRating = Float.parseFloat (shopJson.getString ("rating"));
                        final String ratingUse = shopJson.getString ("rating");
                        final RatingBar rateBar = findViewById (R.id.ratingBar);
                        final TextView ratingCount = findViewById (R.id.ratingText);
                        String delCharge = "";
                        String minCharge = "";
                        if(shopJson.getString ("viewType").equals ("shop")){
                            delCharge = "Del charge : \u20B9"+shopJson.getInt ("delCharges");
                            minCharge = "Del free after : \u20B9"+shopJson.getInt ("minOrderValue");
                            if(droppedFrom.equals ("trend")){
                                droppedFrom = "shop";
                            }
                        }
                        else{
                            if(droppedFrom.equals ("trend")){
                                droppedFrom = "service";
                            }
                        }
                        String [] catId = shopJson.getString ("catId").split (",");
                        String catName ="";
                        final SQLiteDatabase myDatabase = openOrCreateDatabase("martjinni",MODE_PRIVATE,null);
                        for(int j = 0;j<catId.length;j++){
                            int catUseId = Integer.valueOf (catId[j]);
                            Cursor resultSet = myDatabase.rawQuery ("SELECT * from catList where catId="+catUseId,null);
                            resultSet.moveToFirst ();
                            if(j==0){
                                catName = resultSet.getString (1);
                            }
                            else{
                                catName = catName +","+resultSet.getString (1);
                            }
                        }
                        myDatabase.close ();
                        final TextView minChargeUse = findViewById (R.id.minCharge);
                        final TextView minChargeUse2 = findViewById (R.id.delCharge);
                        final TextView catNameUse = findViewById (R.id.shopCat);
                        final String shopNameUse = shopJson.getString ("shopName");
                        final TextView shopName = findViewById (R.id.shopName);
                        final String address = shopJson.getString ("address");
                        final TextView addressValue = findViewById (R.id.address);
                        final TextView addressValue2 = findViewById (R.id.shopDesc);
                        final String delValuew = delCharge;
                        final String minChargew = minCharge;
                        final String catNamew = catName;
                        if(shopJson.getLong ("shopPhoneNumber")>0){
                            final ImageView callMake = findViewById (R.id.makeCall);
                            runOnUiThread (new Runnable () {
                                @Override
                                public void run() {
                                    callMake.setVisibility (View.VISIBLE);
                                }
                            });
                            callMake.setOnClickListener (new View.OnClickListener () {
                                @Override
                                public void onClick(View v) {
                                    try{
                                        onCall((shopJson.getLong ("shopPhoneNumber")));
                                    }
                                    catch (JSONException jsonException){
                                        jsonException.printStackTrace ();
                                    }
                                }
                            });
                        }
                        runOnUiThread (new Runnable () {
                            @Override
                            public void run() {
                                try{
                                    ratingCount.setText (ratingUse);
                                    rateBar.setRating (useRating);
                                    rateBar.setVisibility (View.VISIBLE);
                                    minChargeUse2.setText (delValuew);
                                    addressValue2.setText (shopJson.getString ("shopDesc"));
                                    minChargeUse.setText (minChargew);
                                    catNameUse.setText (catNamew);
                                    shopName.setText (shopNameUse);
                                    addressValue.setText (address);
                                }
                                catch (JSONException jsonException){
                                    jsonException.printStackTrace ();
                                }
                            }
                        });
                        if(!(insertShopInfo(shopNameUse,shopJson.getInt("delCharges"),shopJson.getInt ("minOrderValue")))){
                            goToPrevious ();
                        }
                    }
                }
                onStop ();
            }
            catch (JSONException e2){
                e2.printStackTrace ();
                onStop ();
            }
        }

        private boolean insertShopInfo(String shopName, int delCharges, int minOrder){
            try{
                SQLiteDatabase myDatabase = openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
                myDatabase.execSQL ("INSERT or replace INTO shopList VALUES('"+shopId+"','"+shopName+"','"+delCharges+"','"+minOrder+"')");
                myDatabase.close ();
                return true;
            }
            catch (SQLException sqlException){
                sqlException.printStackTrace ();
                return false;
            }
        }
    }

    class addingItemToList implements Runnable{
        @Override
        public void run(){
            try{
                Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork (new HurlStack ());
                sharedPreferences = getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                mRequestQueue = new RequestQueue (cache, network);
                mRequestQueue.start();
                JSONObject jsonVal = new JSONObject ();
                jsonVal.put ("shopId",shopId);
                String userUrl = baseUrl+"itemlist";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, userUrl, jsonVal,  new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try{
                            if(response.getInt ("totalCount")>0){
                                JSONArray dataSet = response.getJSONArray ("data");
                                insertIntoItem(dataSet);
                                runOnUiThread (new Runnable () {
                                    @Override
                                    public void run() {
                                        if(dispRecomend>0){
                                            TextView recomendLayout = findViewById (R.id.recomendview);
                                            recomendLayout.setVisibility (View.VISIBLE);
                                            recomendAdapter adapterRecomend = new recomendAdapter(mCtx,recomendListObject);
                                            RecyclerView recomendView = findViewById (R.id.recomendedshops);
                                            recomendView.setHasFixedSize (false);
                                            recomendView.setNestedScrollingEnabled(false);
                                            recomendView.setLayoutManager (new LinearLayoutManager (mCtx));
                                            recomendView.setAdapter (adapterRecomend);
                                        }
                                        itemListAdapter adapterList = new itemListAdapter (mCtx,itemListObject);
                                        RecyclerView ListView = findViewById (R.id.detailview);
                                        ListView.setHasFixedSize (false);
                                        ListView.setNestedScrollingEnabled(false);
                                        ListView.setLayoutManager (new LinearLayoutManager (mCtx));
                                        ListView.setAdapter (adapterList);
                                        NestedScrollView nestView = findViewById (R.id.nestView);
                                        preloader.setVisibility (View.INVISIBLE);
                                        nestView.setVisibility (View.VISIBLE);
                                        final RelativeLayout shopCardImage = findViewById (R.id.shopImage);
                                        shopCardImage.findFocus ();
                                    }
                                });
                            }
                            else{
                                runOnUiThread (new Runnable () {
                                    @Override
                                    public void run() {
                                        preloader.setVisibility (View.GONE);
                                        noShop1.setVisibility (View.VISIBLE);
                                    }
                                });

                                //goToPrevious ();
                            }
                        }
                        catch (JSONException e2){
                            e2.printStackTrace ();
                            onStop ();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        error.printStackTrace ();
                        onStop ();
                    }
                });
                mRequestQueue.add(jsonObjectRequest);
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }

        private void insertIntoItem(JSONArray dataSet){
            try{
                if(droppedFrom.equals ("shop")){
                    int display = 0;
                    int addonAdded = 0, subsAdded = 0;
                    for(int i = 0;i<dataSet.length ();i++){
                        JSONObject itemMain = dataSet.getJSONObject (i);
                        int displayThis = 0;
                        if(itemMain.getInt ("isActive")>0){
                            JSONArray subItem = itemMain.getJSONArray ("subItems");
                            for(int j=0;j<subItem.length ();j++){
                                JSONObject useSubItem = subItem.getJSONObject (j);
                                int quantity = getItemCount (useSubItem.getInt ("subItemId"));
                                int customize = 0;
                                if(!useSubItem.getString ("addOnInfo").equals ("")){
                                    if(addonAdded==0){
                                        collectingAddonData newRunner = new collectingAddonData();
                                        new Thread (newRunner).start ();
                                        addonAdded = 1;
                                    }
                                    customize = 1;
                                }
                                if(!useSubItem.getString ("subsInfo").equals ("")){
                                    if(subsAdded==0){
                                        collectingSubData newSubRunner = new collectingSubData();
                                        new Thread (newSubRunner).start ();
                                        subsAdded=1;
                                    }
                                    customize = 1;
                                }
                                if(useSubItem.getJSONArray ("quantInfo").length ()>1){
                                    customize = 1;
                                }
                                int hasImage = 0;
                                if(useSubItem.getString ("imgUrl").equals ("1")){
                                    hasImage = 1;
                                }
                                int price = 0;
                                for(int k =0; k<useSubItem.getJSONArray ("quantInfo").length ();k++){
                                    JSONObject quantObject = useSubItem.getJSONArray ("quantInfo").getJSONObject (k);
                                    if(quantObject.getInt ("isActive")==1){
                                        if(k==0){
                                            price = quantObject.getInt ("value");
                                        }
                                        else{
                                            if(price>quantObject.getInt ("value")&&quantObject.getInt ("value")>0){
                                                price = quantObject.getInt ("value");
                                            }
                                        }
                                    }
                                    insertIntoQuantList1 workNew = new insertIntoQuantList1(quantObject.getInt ("quantId"), useSubItem.getInt ("subItemId"), quantObject.getString ("quantity"), quantObject.getInt ("mrp"), quantObject.getInt ("value"), quantObject.getInt ("isActive"));
                                    new Thread (workNew).start ();
                                    //insertIntoQuantList ();
                                }
                                insertIntoItemMaster (useSubItem.getInt ("subItemId"),useSubItem.getString ("Name"),useSubItem.getString ("spclCat"),useSubItem.getInt ("gstRate"),useSubItem.getString ("addOnInfo"),useSubItem.getString ("subsInfo"),useSubItem.getInt ("isActive"),hasImage,customize,quantity,itemMain.getInt ("mainItemId"),price);
                                if(price>0){
                                    String dispPrice = "";
                                    if(customize==0){
                                        dispPrice = "Selling @ Rs. "+price;
                                    }
                                    else{
                                        dispPrice = "Starting from Rs. "+price;
                                    }
                                    if(!useSubItem.getString ("imgUrl").equals ("")){
                                        String imgUrl = newClass.itemUrl+String.valueOf (useSubItem.getInt ("subItemId"))+".jpeg";
                                        recomendListObject.add (new recomendClass(imgUrl,useSubItem.getString ("Name"),itemMain.getString ("Name"),dispPrice,useSubItem.getString ("spclCat"),useSubItem.getInt ("subItemId"),quantity,customize));
                                        dispRecomend = dispRecomend+1;
                                    }
                                    displayThis = displayThis+1;
                                }
                            }
                        }
                        if(displayThis>0){
                            itemListObject.add(new itemListClass(itemMain.getString ("Name"),itemMain.getInt ("mainItemId"))) ;
                            display = display+1;
                        }
                    }
                    stopDBConnection ();
                }
                else{
                    int display = 0;
                    int addonAdded = 0, subsAdded = 0;
                    for(int i = 0;i<dataSet.length ();i++){
                        JSONObject itemMain = dataSet.getJSONObject (i);
                        int displayThis = 0;
                        if(itemMain.getInt ("isActive")>0){
                            JSONArray subItem = itemMain.getJSONArray ("subItems");
                            for(int j=0;j<subItem.length ();j++){
                                JSONObject useSubItem = subItem.getJSONObject (j);
                                int quantity = getItemCount (useSubItem.getInt ("subItemId"));
                                int customize = 0;
                                if(useSubItem.getJSONArray ("quantInfo").length ()>1){
                                    customize = 1;
                                }
                                int hasImage = 0;
                                if(useSubItem.getString ("imgUrl").equals ("1")){
                                    hasImage = 1;
                                }
                                if(!useSubItem.getString ("addOnInfo").equals ("")){
                                    if(addonAdded==0){
                                        collectingAddonData newRunner = new collectingAddonData();
                                        new Thread (newRunner).start ();
                                        addonAdded = 1;
                                    }
                                    customize = 1;
                                }
                                if(!useSubItem.getString ("subsInfo").equals ("")){
                                    if(subsAdded==0){
                                        collectingSubData newSubRunner = new collectingSubData();
                                        new Thread (newSubRunner).start ();
                                        subsAdded=1;
                                    }
                                    int price = 0;
                                    for(int k =0; k<useSubItem.getJSONArray ("quantInfo").length ();k++){
                                        JSONObject quantObject = useSubItem.getJSONArray ("quantInfo").getJSONObject (k);
                                        if(quantObject.getInt ("isActive")==1){
                                            if(k==0){
                                                price = quantObject.getInt ("value");
                                            }
                                            else{
                                                if(price>quantObject.getInt ("value")){
                                                    price = quantObject.getInt ("value");
                                                }
                                            }
                                        }
                                        insertIntoQuantList (quantObject.getInt ("quantId"), useSubItem.getInt ("subItemId"), quantObject.getString ("quantity"), quantObject.getInt ("mrp"), quantObject.getInt ("value"), quantObject.getInt ("isActive"));
                                    }
                                    if(insertIntoItemMaster (useSubItem.getInt ("subItemId"),useSubItem.getString ("Name"),useSubItem.getString ("spclCat"),useSubItem.getInt ("gstRate"),useSubItem.getString ("addOnInfo"),useSubItem.getString ("subsInfo"),useSubItem.getInt ("isActive"),hasImage,customize,quantity,itemMain.getInt ("mainItemId"),price)){
                                        if(price>0){
                                            String dispPrice = "";
                                            if(customize==0){
                                                dispPrice = "Selling @ Rs. "+price;
                                            }
                                            else{
                                                dispPrice = "Starting from Rs. "+price;
                                            }
                                            if(!useSubItem.getString ("imgUrl").equals ("")){
                                                String imgUrl = newClass.itemUrl+String.valueOf (useSubItem.getInt ("subItemId"))+"/.jpeg";
                                                recomendListObject.add (new recomendClass(imgUrl,useSubItem.getString ("Name"),itemMain.getString ("Name"),dispPrice,useSubItem.getString ("spclCat"),useSubItem.getInt ("subItemId"),quantity,customize));
                                                dispRecomend = dispRecomend+1;
                                            }
                                            displayThis = displayThis+1;
                                        }
                                    }
                                }
                            }
                        }
                        if(displayThis>0){
                            itemListObject.add(new itemListClass(itemMain.getString ("Name"),itemMain.getInt ("mainItemId"))) ;
                            display = display+1;
                        }
                    }
                    stopDBConnection ();
                }
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }

        private boolean insertIntoQuantList(int quantId, int itemId, String quantName, int mrp, int value, int isActive){
            insertIntoQuantList1 workNew = new insertIntoQuantList1( quantId,  itemId,  quantName,  mrp,  value,  isActive);
            new Thread (workNew).start ();
            return true;
        }

        private boolean insertIntoItemMaster(int itemId, String itemName, String spclCat, int gstRate, String addonInfo, String subsInfo, int isActive, int hasImage, int isCustom, int itemCount, int mainItemId, int displayPrice){
            insertingIntoItemsMaster newRunner = new insertingIntoItemsMaster( itemId,  itemName,  spclCat,  gstRate,  addonInfo,  subsInfo,  isActive,  hasImage,  isCustom,  itemCount,  mainItemId,  displayPrice);
            new Thread (newRunner).start ();
            return true;
        }

    }

    class insertingIntoItemsMaster implements Runnable{
        int itemId,gstRate,isActive,hasImage,isCustom,itemCount,mainItemId,displayPrice;
        String itemName, spclCat, addonInfo, subsInfo;

        public insertingIntoItemsMaster(int itemId, String itemName, String spclCat, int gstRate, String addonInfo, String subsInfo, int isActive, int hasImage, int isCustom, int itemCount, int mainItemId, int displayPrice) {
            this.itemId = itemId;
            this.gstRate = gstRate;
            this.isActive = isActive;
            this.hasImage = hasImage;
            this.isCustom = isCustom;
            this.itemCount = itemCount;
            this.mainItemId = mainItemId;
            this.displayPrice = displayPrice;
            this.itemName = itemName;
            this.spclCat = spclCat;
            this.addonInfo = addonInfo;
            this.subsInfo = subsInfo;
        }

        @Override
        public void run(){
            insertIntoItemMaster();
        }

        private void insertIntoItemMaster(){
            try{
                SQLiteDatabase myDatabase = openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
                myDatabase.execSQL ("INSERT or replace INTO itemListMaster VALUES("+itemId+",'"+itemName+"','"+spclCat+"','"+gstRate+"','"+addonInfo+"','"+subsInfo+"','"+shopId+"','"+isActive+"','"+hasImage+"','"+isCustom+"','"+itemCount+"','"+mainItemId+"','"+displayPrice+"')");
                myDatabase.close ();
            }
            catch (SQLException sqlException){
                sqlException.printStackTrace ();
            }
        }
    }

    class insertIntoQuantList1 implements Runnable{
        int quantId, itemId,  mrp, value, isActive;
        String quantName;

        public insertIntoQuantList1(int quantId, int itemId, String quantName, int mrp, int value, int isActive) {
            this.quantId = quantId;
            this.itemId = itemId;
            this.mrp = mrp;
            this.value = value;
            this.isActive = isActive;
            this.quantName = quantName;
        }

        @Override
        public void run(){
            insertIntoQuantListUse();
        }

        private boolean insertIntoQuantListUse(){
            try{
                SQLiteDatabase myDatabase = openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
                myDatabase.execSQL ("INSERT or replace INTO quantList VALUES('"+quantId+"','"+itemId+"','"+quantName+"','"+mrp+"','"+value+"','"+isActive+"')");
                myDatabase.close ();
                return true;
            }
            catch (SQLException sqlException){
                sqlException.printStackTrace ();
                return false;
            }
        }

    }

    class collectingSubData implements Runnable{

        @Override
        public void run() {
            getSunAndAddSub();
        }

        private void getSunAndAddSub(){
            try{
                Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork (new HurlStack ());
                sharedPreferences = getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                mRequestQueue = new RequestQueue (cache, network);
                mRequestQueue.start();
                JSONObject jsonVal = new JSONObject ();
                jsonVal.put ("shopId",shopId);
                String userUrl = baseUrl+"shopsubsinfo";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, userUrl, jsonVal,  new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try{
                            int use = 0;
                            for(int i=0;i<response.getJSONArray ("data").length ();i++){
                                JSONObject subsInfo = response.getJSONArray ("data").getJSONObject (i);
                                if(insertIntosubsMaster (subsInfo.getInt ("subscriptionId"),subsInfo.getInt ("noOfDays"),subsInfo.getInt ("suscriptionPrice"),subsInfo.getInt ("isActive"))){
                                    use = use + 1;
                                }
                            }
                            if(use>0){
                                checkAndStoreShopWish();
                            }
                        }
                        catch (JSONException jsonUseException){
                            jsonUseException.printStackTrace ();
                            goToPrevious ();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        error.printStackTrace ();
                    }
                });
                mRequestQueue.add(jsonObjectRequest);
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
                goToPrevious ();
            }
        }

        private boolean insertIntosubsMaster(int subsId, int noDay, int price, int isActive){
            try{
                SQLiteDatabase myDatabase = openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
                myDatabase.execSQL ("INSERT or replace INTO subsMaster VALUES('"+subsId+"','"+noDay+"','"+shopId+"','"+price+"','"+isActive+"')");
                myDatabase.close ();
                return true;
            }
            catch (SQLException sqlException){
                sqlException.printStackTrace ();
                return false;
            }
        }

        private void checkAndStoreShopWish(){
            try{
                Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork (new HurlStack ());
                sharedPreferences = getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                mRequestQueue = new RequestQueue (cache, network);
                mRequestQueue.start();
                JSONObject jsonVal = new JSONObject ();
                jsonVal.put ("shopId",shopId);
                String userUrl = baseUrl+"viewshoptime";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, userUrl, jsonVal,  new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try{
                            if(response.getInt ("totalCount")>0){
                                for(int i=0;i<response.getJSONArray ("Data").length ();i++){
                                    JSONObject shopTime = response.getJSONArray ("Data").getJSONObject (i);
                                    boolean data = insertIntoSubsData (shopTime.getInt ("timeId"), shopTime.getString ("name"), shopTime.getString ("time"), shopTime.getInt ("isActive"));
                                }
                            }
                        }
                        catch (JSONException jsonUseException){
                            jsonUseException.printStackTrace ();
                            goToPrevious ();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        error.printStackTrace ();
                    }
                });
                mRequestQueue.add(jsonObjectRequest);
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
                goToPrevious ();
            }
        }

        private boolean insertIntoSubsData(int timeId, String name, String time , int isActive){
            try{
                SQLiteDatabase myDatabase = openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
                myDatabase.execSQL ("INSERT or replace INTO subsData VALUES('"+timeId+"','"+shopId+"','"+name+"','"+time+"','"+isActive+"')");
                myDatabase.close ();
                return true;
            }
            catch (SQLException sqlException){
                sqlException.printStackTrace ();
                return false;
            }
        }
    }

    class collectingAddonData implements Runnable{
        @Override
        public void run() {
            getAddonSetAddon();
        }

        private void getAddonSetAddon(){
            try{
                Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork (new HurlStack ());
                sharedPreferences = getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                mRequestQueue = new RequestQueue (cache, network);
                mRequestQueue.start();
                JSONObject jsonVal = new JSONObject ();
                jsonVal.put ("shopId",shopId);
                String userUrl = baseUrl+"shopaddoninfo";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, userUrl, jsonVal,  new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try{
                            for(int i=0;i<response.getJSONArray ("data").length ();i++){
                                JSONObject shopAddData = response.getJSONArray ("data").getJSONObject (i);
                                if(insertIntoAddonMaster (shopAddData.getInt("groupId"),shopAddData.getString ("groupName"),shopAddData.getInt ("isMultiSelect"),shopAddData.getInt ("groupCharge"),shopAddData.getInt ("isActiveFl"))){
                                    for(int j=0;j<shopAddData.getJSONArray ("itemInfo").length ();j++){
                                        JSONObject itemInfo = shopAddData.getJSONArray ("itemInfo").getJSONObject (j);
                                        insertIntoAddonList (itemInfo.getInt ("addOnItemId"), itemInfo.getString ("addOnItemName"), shopAddData.getInt ("groupId"), itemInfo.getInt ("isActiveFl"));
                                    }
                                }
                            }
                        }
                        catch (JSONException jsonUseException){
                            jsonUseException.printStackTrace ();
                            goToPrevious ();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        error.printStackTrace ();
                    }
                });
                mRequestQueue.add(jsonObjectRequest);
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
                goToPrevious ();
            }
        }

        private boolean insertIntoAddonMaster(int groupId, String groupName, int isMultiSelect, int price, int isActive){
            try{
                SQLiteDatabase myDatabase = openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
                myDatabase.execSQL ("INSERT or replace INTO addonMaster VALUES('"+groupId+"','"+groupName+"','"+shopId+"','"+isMultiSelect+"','"+price+"','"+isActive+"')");
                myDatabase.close ();
                return true;
            }
            catch (SQLException sqlException){
                sqlException.printStackTrace ();
                return false;
            }
        }

        private void insertIntoAddonList(int itemid, String addOnName, int groupId, int isActive){
            try{
                SQLiteDatabase myDatabase = openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
                myDatabase.execSQL ("INSERT or replace INTO addOnList VALUES('"+itemid+"','"+addOnName+"','"+groupId+"','"+isActive+"')");
                myDatabase.close ();
            }
            catch (SQLException sqlException){
                sqlException.printStackTrace ();
            }
        }
    }

    @Override
    public void onBackPressed()
    {
        goToPrevious ();
        super.onBackPressed();  // optional depending on your needs
    }

    public void goToPrevious(){
        Intent intent = new Intent(this, homePage.class);
        startActivity (intent);
        finish ();
    }

    public void changeShopImage(String shopUrl){
        workingFornNewImage runner = new workingFornNewImage(shopUrl);
        new Thread (runner).start ();
    }

    class workingFornNewImage implements Runnable{
        String shopUrl;

        private workingFornNewImage(String shopUrl) {
            this.shopUrl = shopUrl;
        }

        @Override
        public void run(){
            try{
                URL imageUrl = new URL(shopUrl);
                Bitmap bitmap = BitmapFactory.decodeStream(imageUrl.openConnection().getInputStream());
                int height = bitmap.getHeight();
                int width = bitmap.getWidth();
                final int IMAGE_MAX_SIZE = 1200000; // 1.2MP
                double y = Math.sqrt(IMAGE_MAX_SIZE
                        / (((double) width) / height));
                double x = (y / height) * width;

                Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, (int) x,
                        (int) y, true);
                final RelativeLayout shopCardImage = findViewById (R.id.shopImage);
                final BitmapDrawable backgorund =  new BitmapDrawable(getResources(), scaledBitmap);
                runOnUiThread (new Runnable () {
                    @Override
                    public void run() {
                        shopCardImage.setBackground (backgorund);
                    }
                });
            }
            catch (IOException e2){
                e2.printStackTrace ();
            }
        }
    }

    public boolean initializeDb(){
        try{
            SQLiteDatabase myDatabase = openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
            myDatabase.execSQL ("CREATE TABLE IF NOT EXISTS itemListMaster(itemId INTEGER PRIMARY KEY, itemName VARCHAR, spclCat VARCHAR, gstRate INTEGER,addonInfo VARCHAR, subsInfo VARCHAR, shopId INTEGER, isActive INTEGER, hasImage INTEGER, isCustomize INTEGER, itemCount INTEGER, mainItemId INTEGER, displayPrice INTEGER )");
            myDatabase.execSQL ("CREATE TABLE IF NOT EXISTS quantList(quantId INTEGER PRIMARY KEY, itemId INTEGER, quantName VARCHAR, mrp INTEGER, value INTEGER, isActive INTEGER)");
            myDatabase.execSQL ("CREATE TABLE IF NOT EXISTS shopList(shopId INTEGER PRIMARY KEY, shopName VARCHAR, delCharges INTEGER, minOrder INTEGER)");
            myDatabase.execSQL ("CREATE TABLE IF NOT EXISTS addonMaster(groupId INTEGER PRIMARY KEY, groupName VARCHAR,shopId INTEGER, isMultiSelect INTEGER, price INTEGER, isActive INTEGER )");
            myDatabase.execSQL ("CREATE TABLE IF NOT EXISTS addOnList(addOnItemId INTEGER PRIMARY KEY, addonName VARCHAR, groupId INTEGER, isActive INTEGER)");
            myDatabase.execSQL ("CREATE TABLE IF NOT EXISTS subsMaster(subsId INTEGER PRIMARY KEY, noOfDays INTEGER, shopId INTEGER, price INTEGER, isActive INTEGER)");
            myDatabase.execSQL ("CREATE TABLE IF NOT EXISTS subsData(timeId INTEGER PRIMARY KEY, shopId INTEGER, name VARCHAR, time VARCHAR, isActive INTEGER)");
            myDatabase.execSQL ("CREATE TABLE IF NOT EXISTS cartInfo(cartId INTEGER PRIMARY KEY AUTOINCREMENT, count INTEGER, itemId INTEGER, itemName VARCHAR, quantId INTEGER, addonInfo VARCHAR, subsInfo INTEGER, startDate VARCHAR, startTime VARCHAR, cost REAL, gst REAL, subsCharge INTEGER )");
            myDatabase.close ();
            return true;
        }
        catch (SQLException sqlException){
            sqlException.printStackTrace ();
            return false;
        }
    }

    public int getItemCount(int itemId){
        SQLiteDatabase myDatabase = openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        Cursor resultSet = myDatabase.rawQuery ("SELECT itemCount from itemListMaster where itemId='"+itemId+"'",null);
        if(resultSet.getCount ()>0){
            resultSet.moveToFirst ();
            myDatabase.close ();
            return resultSet.getInt (0);
        }
        else{
            myDatabase.close ();
            return 0;
        }
    }

    public void stopDBConnection(){
        SQLiteDatabase myDatabase = openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        myDatabase.close ();
    }

    public void createNewCustomOrder(int itemId){
        workingForNewOrder newRunner = new workingForNewOrder(itemId);
        new Thread (newRunner).start ();
    }

    public void workWithItemPlus(int itemId){
        Cursor resultSet = getItemMasterDetails(itemId);
        resultSet.moveToFirst ();
        int quant = resultSet.getInt (10);
        Cursor quantResults = getQuantId (itemId);
        if(resultSet.getInt(9)>0){
            createNewCustomOrder (itemId);
            stopDBConnection ();
        }
        else{
            quantResults.moveToFirst ();
            int val = quantResults.getInt (4);
            int gstPer = resultSet.getInt (3);
            float gstCalc = (gstPer/100.0f)*val;
            float itemValue = gstCalc+val;
            updateTotalValue (itemValue);
            int cartId = insertIntoCartInfoDefault(1, itemId, resultSet.getString (1),quantResults.getInt (0), "", 0, "", "", itemValue, gstCalc);
            updateItemCountMaster (itemId, quant+1);
            if(cartId>0) {
                updateDisplayView(quant,itemId);
            }
        }
    }

    public void workWithAddOn(final int itemId, final int cartId, String [] addOnList){
        List<addOnMaster> listAddOnMaster = new ArrayList<> ();
        int showAddon = 0;
        for(int i =0;i<addOnList.length;i++){
            SharedPreferences sharedPreferences = mCtx.getSharedPreferences("userInfo", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit ();
            String workingInfo = itemId+","+cartId;
            editor.putString ("workingInfo",workingInfo);
            String checkName = "groupAdd"+addOnList[i];
            editor.putString (checkName,"").apply ();
            int displaySubList = 0;
            Cursor getAddonMasterData = getAddonMasterInfo (Integer.parseInt (addOnList[i]));
            Cursor getAddonListData = getAddonListData (Integer.parseInt (addOnList[i]));
            getAddonListData.moveToFirst ();
            List<addOnList> listAddonData = new ArrayList<> ();
            getAddonMasterData.moveToFirst ();
            while (!getAddonListData.isAfterLast ()){
                if(getAddonListData.getInt (3)>0){
                    listAddonData.add (new addOnList (getAddonListData.getString (1),getAddonMasterData.getInt (3),getAddonMasterData.getInt (4),getAddonListData.getInt (0),getAddonMasterData.getInt (0)));
                    displaySubList = displaySubList+1;
                    getAddonListData.moveToNext ();
                }
            }
            if(getAddonMasterData.getInt(5)>0){
                if(displaySubList>0){
                    listAddOnMaster.add(new addOnMaster (getAddonMasterData.getString (1),getAddonMasterData.getInt (4),getAddonMasterData.getInt (5),getAddonMasterData.getInt(3),getAddonMasterData.getInt (0),listAddonData));
                    showAddon = showAddon + 1;
                }
            }
        }
        if(showAddon>0){
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(mCtx);
            View mView = ((Activity) mCtx).getLayoutInflater().inflate(R.layout.aadoninfo, null);
            mBuilder.setView(mView);
            final AlertDialog dialog = mBuilder.create();
            RecyclerView quantPrint = mView.findViewById (R.id.displayAddOn);
            addOnMasterAdapter adapter = new addOnMasterAdapter (this,listAddOnMaster, mView);
            quantPrint.setHasFixedSize (false);
            quantPrint.setLayoutManager (new LinearLayoutManager (mCtx));
            quantPrint.setAdapter (adapter);
            dialog.setCancelable(false);
            TextView closeDialButton = mView.findViewById (R.id.closeQuant);
            closeDialButton.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    dialog.dismiss ();
                    interSubs ();
                }
            });
            TextView addButton = mView.findViewById (R.id.AddAddon);
            addButton.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    finalizeAddOn(dialog, itemId);
                }
            });
            dialog.show();
        }
        else{
            workWithSubscription(itemId,cartId);
        }
    }

    public void finalizeAddOn(AlertDialog dialog, int itemId){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit ();
        String [] useInfo = sharedPreferences.getString ("workingInfo","").split (",");
        int position = Integer.parseInt (useInfo[0]);
        int cartId = Integer.parseInt (useInfo[1]);
        editor.remove ("workingInfo").apply ();
        try{
            Cursor itemInfo = getItemMasterDetails (itemId);
            itemInfo.moveToFirst ();
            String addOnList = itemInfo.getString (4);
            if(addOnList.equals ("")){
                dialog.dismiss ();
            }
            else{
                String [] addList = addOnList.split (",");
                JSONArray finalAddList = new JSONArray ();
                float totalCost = 0f;
                for(int i=0;i<addList.length;i++){
                    JSONArray subAddList = new JSONArray ();
                    JSONObject finalSubList = new JSONObject ();
                    String checkName = "groupAdd"+addList[i];
                    if(!sharedPreferences.getString (checkName,"").equals ("")){
                        String[] storedInfo = sharedPreferences.getString (checkName,"").split (",");
                        Cursor addOnMaster = getAddonMasterInfo (Integer.parseInt (addList[i]));
                        addOnMaster.moveToFirst ();
                        if((addOnMaster.getInt (3)==0)&&(storedInfo.length==1)){
                            JSONObject insertList = new JSONObject ();
                            insertList.put("subAddId",storedInfo[0]);
                            totalCost = totalCost+addOnMaster.getInt (4);
                            subAddList.put(insertList);
                            finalSubList.put("itemId",subAddList);
                            finalSubList.put("addOnId",addOnMaster.getInt (0));
                            finalSubList.put("quantity",1);
                            finalAddList.put(finalSubList);
                        }
                        else if((addOnMaster.getInt (3)==1)&&(storedInfo.length>0)){
                            int quant = 0;
                            for(int j =0;j<storedInfo.length;j++){
                                JSONObject insertList = new JSONObject ();
                                insertList.put("subAddId",storedInfo[j]);
                                quant = quant+1;
                                subAddList.put(insertList);
                            }
                            totalCost = totalCost+addOnMaster.getInt (4)*quant;
                            finalSubList.put("itemId",subAddList);
                            finalSubList.put("addOnId",addOnMaster.getInt (0));
                            finalSubList.put("quantity",quant);
                            finalAddList.put(finalSubList);
                        }
                    }
                }
                updateTotalValue (totalCost);
                updateAddOnQuant (finalAddList.toString (),cartId,totalCost);
                dialog.dismiss ();
                workWithSubscription (position,cartId);
            }
        }
        catch (JSONException jsonException){
            jsonException.printStackTrace ();
        }
    }

    public float getCartCost(int cartId){
        SQLiteDatabase myDatabase = mCtx.openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        Cursor resultSet = myDatabase.rawQuery ("SELECT cost from cartInfo where cartId='"+cartId+"'",null);
        resultSet.moveToFirst ();
        float returnFloat = resultSet.getFloat (0);
        myDatabase.close ();
        return returnFloat;
    }

    public void updateAddOnQuant(String addInfo, int cartId, float amountAdd){
        float cost = getCartCost (cartId);
        cost = cost+amountAdd;
        SQLiteDatabase myDatabase = mCtx.openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        myDatabase.execSQL ("UPDATE cartInfo set addonInfo='"+addInfo+"',cost='"+cost+"' where cartId='"+cartId+"'");
        myDatabase.close ();
    }

    public void interSubs(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit ();
        String [] useInfo = sharedPreferences.getString ("workingInfo","").split (",");
        int position = Integer.parseInt (useInfo[0]);
        int cartId = Integer.parseInt (useInfo[1]);
        editor.remove ("workingInfo").apply ();
        workWithSubscription (position,cartId);
    }

    public Cursor getAddonListData(int groupId){
        SQLiteDatabase myDatabase = mCtx.openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        Cursor resultSet = myDatabase.rawQuery ("SELECT * from addOnList where groupId='"+groupId+"'",null);
        return resultSet;
    }

    class workingForNewOrder implements Runnable{
        int itemId;

        public workingForNewOrder(int itemId) {
            this.itemId = itemId;
        }

        @Override
        public void run(){
            final Cursor resultSet = getItemMasterDetails(itemId);
            resultSet.moveToFirst ();
            int quant = resultSet.getInt (10);
            final Cursor quantResults = getQuantId (itemId);
            if(resultSet.getInt(9)>0) {
                int ifQuant = quantResults.getCount ();
                if(ifQuant>1){
                    runOnUiThread (new Runnable () {
                        @Override
                        public void run() {
                            AlertDialog.Builder mBuilder = new AlertDialog.Builder(mCtx);
                            View mView = getLayoutInflater().inflate(R.layout.select_quantity, null);
                            mBuilder.setView(mView);
                            final AlertDialog dialog = mBuilder.create();
                            RecyclerView quantPrint = mView.findViewById (R.id.showQuant);
                            quantResults.moveToFirst ();
                            List<quantClass> quantList= new ArrayList<> ();
                            while(!quantResults.isAfterLast ()){
                                if(quantResults.getInt (5)>0){
                                    quantList.add (new quantClass (itemId,quantResults.getInt (0),quantResults.getInt(4),resultSet.getInt (3),quantResults.getInt (3),quantResults.getString (2)));
                                }
                                quantResults.moveToNext ();
                            }
                            quantViewAdapter adapter = new quantViewAdapter (mCtx,quantList,dialog);
                            quantPrint.setHasFixedSize (false);
                            quantPrint.setLayoutManager (new LinearLayoutManager (mCtx));
                            quantPrint.setAdapter (adapter);
                            dialog.setCancelable(false);
                            TextView closeDialButton = mView.findViewById (R.id.closeQuant);
                            closeDialButton.setOnClickListener (new View.OnClickListener () {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss ();
                                }
                            });
                            dialog.show();
                        }
                    });
                }
                else{
                    quantResults.moveToFirst ();
                    int val = quantResults.getInt (4);
                    int gstPer = resultSet.getInt (3);
                    float gstCalc = (gstPer/100.0f)*val;
                    float itemValue = gstCalc+val;
                    updateTotalValue (itemValue);
                    int cartId = insertIntoCartInfoDefault(1, itemId, resultSet.getString (1),quantResults.getInt (0), "", 0, "", "", itemValue, gstCalc);
                    updateItemCountMaster (itemId, quant+1);
                    if(cartId>0) {
                        updateDisplayView (quant,itemId);
                        if(!resultSet.getString (4).equals ("")){
                            String [] addOnList = resultSet.getString (4).split (",");
                            workWithAddOn (itemId,cartId,addOnList);
                        }
                        else if(!resultSet.getString (5).equals ("")){
                            workWithSubscription(itemId,cartId);
                        }
                    }
                }
            }
            stopDBConnection ();
        }


    }

    public void deductExistingOrder(int itemId){
        Cursor resultSet = getItemMasterDetails(itemId);
        resultSet.moveToFirst ();
        if(resultSet.getInt(9)>0){
            displayExistingCart (itemId);
        }
        else{
            decrementCountView (itemId);
        }
    }

    public void decrementCountView(int itemId){
        Cursor resultSet = getItemMasterDetails(itemId);
        resultSet.moveToFirst ();
        Cursor cartInfo = getCartInfo (itemId);
        cartInfo.moveToFirst ();
        if(cartInfo.getInt (1)>1){
            int cartId = cartInfo.getInt (0);
            int quantUse = cartInfo.getInt (1)-1;
            decrementCountCart (cartId,quantUse);
            Cursor itemListInfo = getItemInfo (itemId);
            itemListInfo.moveToFirst ();
            int quant = itemListInfo.getInt (10)-1;
            updateItemCountMaster (itemId,quant);
            View rootView = ((Activity)mCtx).findViewById(R.id.content);
            String itemCount = "itemCOunt"+itemId;
            TextView itemCOunt = rootView.findViewWithTag (itemCount);
            if(currentOpenTab==resultSet.getInt (11)){
                itemCOunt.setText (String.valueOf (quant));
            }
            if(itemListInfo.getInt (8)>0){
                itemCount = "recomenQuant"+itemId;
                itemCOunt = rootView.findViewWithTag (itemCount);
                itemCOunt.setText (String.valueOf (quant));
            }
            updateTotalValueNegative(cartInfo.getInt (9));
        }
        else{
            updateTotalValueNegative(cartInfo.getFloat (9)+cartInfo.getInt (11));
            Cursor itemListInfo = getItemInfo (itemId);
            itemListInfo.moveToFirst ();
            int quant = itemListInfo.getInt (10)-1;
            updateItemCountMaster (itemId,quant);
            int cartId = cartInfo.getInt (0);
            deleteCartInfo (cartId);
            String plusButTag = "plusBut"+itemId;
            String visTag = "vis"+itemId;
            String itemCount = "itemCOunt"+itemId;
            View rootView = ((Activity)mCtx).findViewById(R.id.content);
            LinearLayout viewLayout = rootView.findViewWithTag (plusButTag);
            TextView addBut = rootView.findViewWithTag (visTag);
            TextView itemCOunt = rootView.findViewWithTag (itemCount);
            if(currentOpenTab==resultSet.getInt (11)){
                itemCOunt.setText (String.valueOf (quant));
                addBut.setVisibility (View.VISIBLE);
                viewLayout.setVisibility (View.GONE);
            }
            if(itemListInfo.getInt (8)>0){
                plusButTag = "plusButRecomend"+itemId;
                visTag = "visRecomend"+itemId;
                itemCount = "recomenQuant"+itemId;
                viewLayout = rootView.findViewWithTag (plusButTag);
                addBut = rootView.findViewWithTag (visTag);
                itemCOunt = rootView.findViewWithTag (itemCount);
                itemCOunt.setText (String.valueOf (quant));
                addBut.setVisibility (View.VISIBLE);
                viewLayout.setVisibility (View.GONE);
            }
        }
    }

    public void deleteCartInfo(int cartId){
        SQLiteDatabase myDatabase = mCtx.openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        myDatabase.execSQL ("delete from cartInfo where cartId='"+cartId+"'");
        myDatabase.close ();
    }


    public void decrementCountCart(int cartId, int countNew){
        SQLiteDatabase myDatabase = mCtx.openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        myDatabase.execSQL ("UPDATE cartInfo set count='"+countNew+"' where cartId='"+cartId+"'");
        myDatabase.close ();
    }

    public Cursor getQuantId(int itemId){
        SQLiteDatabase myDatabase = openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        Cursor resultSet = myDatabase.rawQuery ("SELECT * from quantList where itemId='"+itemId+"'",null);
        return resultSet;
    }

    public Cursor getItemMasterDetails(int itemId){
        SQLiteDatabase myDatabase = openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        return myDatabase.rawQuery ("SELECT * from itemListMaster where itemId='"+itemId+"'",null);
    }

    public void updateItemCountMaster(int itemId, int quant){
        SQLiteDatabase myDatabase = openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        myDatabase.execSQL ("UPDATE itemListMaster set itemCount = '"+quant+"' where itemId='"+itemId+"'");
        myDatabase.close ();
    }

    public int insertIntoCartInfoDefault(int count, int itemId, String itemName, int quantId, String addonInfo, int subsInfo, String startDate, String startTime, float cost, float gst){
        try{
            SQLiteDatabase myDatabase = openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
            myDatabase.execSQL ("INSERT or replace INTO cartInfo(count, itemId, itemName, quantId, addonInfo, subsInfo, startDate, startTime, cost, gst)VALUES('"+count+"','"+itemId+"','"+itemName+"','"+quantId+"','"+addonInfo+"','"+subsInfo+"','"+startDate+"','"+startTime+"','"+cost+"','"+gst+"')");
            Cursor resultSet = myDatabase.rawQuery ("SELECT * from cartInfo", null);
            resultSet.moveToLast ();
            int cartId = resultSet.getInt (0);
            myDatabase.close ();
            return cartId;
        }
        catch (android.database.SQLException sqlException){
            sqlException.printStackTrace ();
            return 0;
        }
    }

    public Cursor getAddonMasterInfo(int groupId){
        SQLiteDatabase myDatabase = mCtx.openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        Cursor resultSet = myDatabase.rawQuery ("SELECT * from addonMaster where groupId='"+groupId+"'",null);
        return resultSet;
    }

    public  void addExistingOrder(int itemId){
        Cursor resultSet = getItemMasterDetails(itemId);
        resultSet.moveToFirst ();
        if(resultSet.getInt(9)>0){
            displayExistingCart (itemId);
        }
        else{
            Cursor cartInfo = getCartInfo (itemId);
            cartInfo.moveToFirst ();
            int cartId = cartInfo.getInt (0);
            int quantUse = cartInfo.getInt (1)+1;
            incrementCountCart (cartId,quantUse);
            Cursor itemListInfo = getItemInfo (itemId);
            itemListInfo.moveToFirst ();
            int quant = itemListInfo.getInt (10);
            updateItemCountMaster (itemId,quant+1);
            updateDisplayView(quant,itemId);
            updateTotalValue(cartInfo.getInt (9));
        }
    }

    public void incrementCountCart(int cartId, int quant){
        SQLiteDatabase myDatabase = mCtx.openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        myDatabase.execSQL ("UPDATE cartInfo set count='"+quant+"' where cartId='"+cartId+"'");
        myDatabase.close ();
    }

    public void displayExistingCart(final int itemId){
        try{
            Cursor cartInfo = getCartInfo (itemId);
            if(cartInfo.getCount ()>0){
                cartInfo.moveToFirst ();
                List<eachItemCardClass> displayList = new ArrayList<> ();
                while(!cartInfo.isAfterLast ()){
                    List<displayHorizontalClass> subAddList = new ArrayList<> ();
                    if(!cartInfo.getString (5).equals ("")){
                        JSONArray addOnList = new JSONArray (cartInfo.getString (5));
                        for(int i=0;i<addOnList.length ();i++){
                            List<displaySubAddOnListClass> subAddonList = new ArrayList<> ();
                            JSONArray itemList = addOnList.getJSONObject (i).getJSONArray ("itemId");
                            for(int j=0;j<itemList.length ();j++){
                                int subAddId = itemList.getJSONObject (j).getInt ("subAddId");
                                String addName = getAddName(subAddId);
                                subAddonList.add (new displaySubAddOnListClass (addName));
                            }
                            if(subAddonList.size ()>0){
                                String groupName = getGroupName(addOnList.getJSONObject (i).getInt ("addOnId"));
                                subAddList.add (new displayHorizontalClass (subAddonList,groupName));
                            }
                        }
                    }
                    String unitName = getUnitName(cartInfo.getInt(4));
                    int noOfDays = 0;
                    int subsAmount = 0;
                    if(cartInfo.getInt(6)>0){
                        noOfDays = getDaysCount(cartInfo.getInt(6));
                        subsAmount = getSubsPrice (cartInfo.getInt(6));
                    }
                    displayList.add (new eachItemCardClass (subAddList,cartInfo.getString (3),unitName,cartInfo.getString (7),noOfDays,subsAmount,cartInfo.getInt(9),cartInfo.getInt(1),cartInfo.getInt(0)));
                    cartInfo.moveToNext ();
                }
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(mCtx);
                View mView = ((Activity) mCtx).getLayoutInflater().inflate(R.layout.customquant, null);
                mBuilder.setView(mView);
                final AlertDialog dialog = mBuilder.create();
                eachItemAdapter adapter = new eachItemAdapter(mCtx,displayList,mView,dialog);
                RecyclerView quantPrint = mView.findViewById (R.id.displayQuantInfoFinal);
                quantPrint.setHasFixedSize (false);
                quantPrint.setLayoutManager (new LinearLayoutManager (mCtx));
                quantPrint.setAdapter (adapter);
                TextView closeBox = mView.findViewById (R.id.addButton);
                closeBox.setOnClickListener (new View.OnClickListener () {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss ();
                    }
                });
                TextView addNew = mView.findViewById (R.id.addNewQuant);
                addNew.setOnClickListener (new View.OnClickListener () {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss ();
                        createNewCustomOrder (itemId);
                    }
                });
                dialog.show ();
            }
            else{
                workWithItemPlus (itemId);
            }
        }
        catch (JSONException jsonException){
            jsonException.printStackTrace ();
        }
    }

    public int getSubsPrice(int subsId){
        SQLiteDatabase myDatabase = mCtx.openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        Cursor resultSet = myDatabase.rawQuery ("SELECT price from subsMaster where subsId='"+subsId +"'",null);
        resultSet.moveToFirst ();
        return resultSet.getInt (0);
    }

    public int getDaysCount(int subsId){
        SQLiteDatabase myDatabase = mCtx.openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        Cursor resultSet = myDatabase.rawQuery ("SELECT noOfDays from subsMaster where subsId='"+subsId +"'",null);
        resultSet.moveToFirst ();
        return resultSet.getInt (0);
    }

    public String getUnitName(int quantId){
        SQLiteDatabase myDatabase = mCtx.openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        Cursor resultSet = myDatabase.rawQuery ("SELECT quantName from quantList where quantId='"+quantId+"'",null);
        resultSet.moveToFirst ();
        return resultSet.getString (0);
    }

    public String getGroupName(int subAddId){
        SQLiteDatabase myDatabase = mCtx.openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        Cursor resultSet = myDatabase.rawQuery ("SELECT groupName from addonMaster where groupId='"+subAddId+"'",null);
        resultSet.moveToFirst ();
        return resultSet.getString (0);
    }

    public String getAddName(int subAddId){
        SQLiteDatabase myDatabase = mCtx.openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        Cursor resultSet = myDatabase.rawQuery ("SELECT addonName from addOnList where addOnItemId='"+subAddId+"'",null);
        resultSet.moveToFirst ();
        return resultSet.getString (0);
    }

    public void updateTotalValueNegative(float addAmount){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        float totalAmount = sharedPreferences.getFloat ("totalBill",0.0f);
        totalAmount = totalAmount - addAmount;
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putFloat ("totalBill",totalAmount).apply ();
        if(totalAmount==0.0f){
            TextView showAmount = findViewById (R.id.totalSum);
            String amount = "Rs : "+totalAmount;
            showAmount.setText (amount);
            CardView showNavigation = findViewById (R.id.displayNavigation);
            showNavigation.setVisibility (View.GONE);
        }
        else{
            TextView showAmount = findViewById (R.id.totalSum);
            String amount = "Rs : "+totalAmount;
            showAmount.setText (amount);
        }
    }

    public void updateTotalValue(float addAmount){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        float totalAmount = sharedPreferences.getFloat ("totalBill",0.0f);
        totalAmount = totalAmount + addAmount;
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if(sharedPreferences.getFloat ("totalBill",0.0f)==0.0f){
            TextView showAmount = findViewById (R.id.totalSum);
            String amount = "\u20B9 "+totalAmount;
            showAmount.setText (amount);
            CardView showNavigation = findViewById (R.id.displayNavigation);
            showNavigation.setVisibility (View.VISIBLE);
        }
        else{
            TextView showAmount = findViewById (R.id.totalSum);
            String amount = "Rs : "+totalAmount;
            showAmount.setText (amount);
        }
        editor.putFloat ("totalBill",totalAmount).apply ();
    }

    public Cursor getCartInfo(int cartId){
        SQLiteDatabase myDatabase = mCtx.openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        Cursor resultSet = myDatabase.rawQuery ("SELECT * from cartInfo where itemId='"+cartId+"'",null);
        return resultSet;
    }

    public void updateDisplayView(int quant, int itemId){
        ViewGroup view = findViewById (R.id.content);
        Cursor resultSet = getItemMasterDetails(itemId);
        resultSet.moveToFirst ();
        if(quant==0){
            if(resultSet.getInt (8)>0){
                String plusButTag = "plusButRecomend"+itemId;
                String visTag = "visRecomend"+itemId;
                String itemCount = "recomenQuant"+itemId;
                LinearLayout viewLayout = view.findViewWithTag (plusButTag);
                TextView addBut = view.findViewWithTag (visTag);
                TextView itemCOunt = view.findViewWithTag (itemCount);
                itemCOunt.setText (String.valueOf (quant+1));
                addBut.setVisibility (View.GONE);
                viewLayout.setVisibility (View.VISIBLE);
            }
        }
        else{
            if(resultSet.getInt (8)>0){
                String itemCount = "recomenQuant"+itemId;
                TextView itemCOunt = view.findViewWithTag (itemCount);
                itemCOunt.setText (String.valueOf (quant+1));
            }
        }
        if(currentOpenTab==resultSet.getInt (11)){
            if(quant==0){
                String plusButTag = "plusBut"+itemId;
                String visTag = "vis"+itemId;
                String itemCount = "itemCOunt"+itemId;
                LinearLayout viewLayout = view.findViewWithTag (plusButTag);
                TextView addBut = view.findViewWithTag (visTag);
                TextView itemCOunt = view.findViewWithTag (itemCount);
                itemCOunt.setText (String.valueOf (quant+1));
                addBut.setVisibility (View.GONE);
                viewLayout.setVisibility (View.VISIBLE);
            }
            else if(quant>0){
                String itemCount = "itemCOunt"+itemId;
                TextView itemCOunt = view.findViewWithTag (itemCount);
                itemCOunt.setText (String.valueOf (quant+1));
            }
        }
    }

    public void workWithSubscription(int itemId, final int cartId){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit ();
        editor.putString ("selectedDate","").apply ();
        editor.putString ("selectedTime","").apply ();
        Cursor itemInfo = getItemInfo (itemId);
        itemInfo.moveToFirst ();
        String subsList = itemInfo.getString (5);
        if(!subsList.equals ("")){
            String[] subsData = subsList.split (",");
            List<String> list = new ArrayList<>();
            List<Integer> subIdList = new ArrayList<> ();
            list.add("Select number of days");
            for(int i=0;i<subsData.length;i++){
                Cursor subsMasterInfo = getSubsMasterInfo (Integer.parseInt (subsData[i]));
                subsMasterInfo.moveToFirst ();
                if(subsMasterInfo.getInt (4)>0){
                    String displayName = subsMasterInfo.getInt (1)+" @ "+subsMasterInfo.getInt (3);
                    list.add (displayName);
                    subIdList.add (subsMasterInfo.getInt (0));
                }
            }
            List<String> timeList = new ArrayList<> ();
            List<Integer> timeIdList = new ArrayList<> ();
            int displayTime=0;
            if(list.size ()>0){
                Cursor checkTimeList = getSubsPreList (itemInfo.getInt (6));
                if(checkTimeList.getCount ()>0){
                    checkTimeList.moveToFirst ();
                    while(!checkTimeList.isAfterLast ()){
                        if(checkTimeList.getInt (4)>0){
                            String displayName = checkTimeList.getString (2)+ " @ "+checkTimeList.getString (4);
                            timeList.add (displayName);
                            timeIdList.add (checkTimeList.getInt (0));
                            displayTime = 1;
                        }
                        checkTimeList.moveToNext ();
                    }
                }
            }
            final List<String> timeUseList = timeList;
            final List<Integer> itemIdUse = timeIdList;
            timeUseList.add("Select from time");
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(mCtx);
            View mView = ((Activity) mCtx).getLayoutInflater().inflate(R.layout.subsdialog, null);
            mBuilder.setView(mView);
            final AlertDialog dialog = mBuilder.create();
            final Spinner spinner1 = mView.findViewById (R.id.spinner1);
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(mCtx,android.R.layout.simple_spinner_item, list);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner1.setAdapter(dataAdapter);
            final Spinner spinner2 = mView.findViewById (R.id.spinner2);
            if(displayTime>0){
                ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<>(mCtx,android.R.layout.simple_spinner_item, list);
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner2.setAdapter(dataAdapter2);
                TextView openClock = mView.findViewById (R.id.openClock);
                openClock.setVisibility (View.GONE);
                spinner2.setVisibility (View.VISIBLE);
            }
            final List<String> dateList = list;
            final List<Integer> subsLis = subIdList;
            TextView closeSubs = mView.findViewById (R.id.closeSubs);
            closeSubs.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    dialog.dismiss ();
                }
            });
            TextView addSub = mView.findViewById (R.id.addSubs);
            final int displayTimeValue = displayTime;
            addSub.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    SharedPreferences sharedPreferences = mCtx.getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                    int isWorking = spinner1.getSelectedItemPosition ();
                    if(isWorking>0){
                        isWorking = isWorking - 1;
                        String selectedDate = sharedPreferences.getString ("selectedDate","");
                        if(selectedDate.equals ("")){
                            Toast.makeText(mCtx, "Kindly select a start date", Toast.LENGTH_SHORT).show();
                        }
                        else{
                            if(displayTimeValue>0){
                                int isTimeSelected = spinner2.getSelectedItemPosition ();
                                if(isTimeSelected>0){
                                    isTimeSelected = isTimeSelected-1;
                                    int subsId = subsLis.get (isWorking);
                                    int timeId = itemIdUse.get (isTimeSelected);
                                    String timeValue = getTime (timeId);
                                    Cursor subsMasterInfo = getSubsMasterInfo(subsId);
                                    subsMasterInfo.moveToFirst ();
                                    float cost = (subsMasterInfo.getInt(1)*getCartCost (cartId))-getCartCost (cartId)+subsMasterInfo.getInt(3);
                                    updateCartSubsInfo(cartId,selectedDate,timeValue,subsMasterInfo.getInt(1),subsId,subsMasterInfo.getInt(3));
                                    updateTotalValue(cost);
                                    dialog.dismiss ();
                                }
                                else{
                                    Toast.makeText(mCtx, "Kindly select delivery time", Toast.LENGTH_SHORT).show();
                                }
                            }
                            else{
                                String selectedTime = sharedPreferences.getString ("selectedTime","");
                                if(selectedTime.equals ("")){
                                    Toast.makeText(mCtx, "Kindly select delivery time", Toast.LENGTH_SHORT).show();
                                }
                                else{
                                    int subsId = subsLis.get (isWorking);
                                    Cursor subsMasterInfo = getSubsMasterInfo(subsId);
                                    subsMasterInfo.moveToFirst ();
                                    float cost = (subsMasterInfo.getInt(1)*getCartCost (cartId))-getCartCost (cartId)+subsMasterInfo.getInt(3);
                                    updateCartSubsInfo(cartId,selectedDate,selectedTime,subsMasterInfo.getInt(1),subsId,subsMasterInfo.getInt (3));
                                    updateTotalValue(cost);
                                    dialog.dismiss ();
                                }
                            }
                        }
                    }
                    else{
                        Toast.makeText(mCtx, "Kindly select number of days", Toast.LENGTH_SHORT).show();
                    }
                }
            });
            final TextView openDate =  mView.findViewById(R.id.openDate);
            final TextView openClock = mView.findViewById(R.id.openClock);
            final Calendar dateSelected = Calendar.getInstance();
            final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    dateSelected.set(Calendar.YEAR, year);
                    dateSelected.set(Calendar.MONTH, month);
                    dateSelected.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    String myFormat = "yyyy-MM-dd"; //In which you need put here
                    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.ITALIAN);
                    openDate.setText(sdf.format(dateSelected.getTime ()));
                    SharedPreferences sharedPreferences = mCtx.getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit ();
                    String timeList = sdf.format(dateSelected.getTime ());
                    editor.putString ("selectedDate",timeList).apply ();
                }
            };
            openDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DatePickerDialog dpd =  new DatePickerDialog(mCtx, date, dateSelected
                            .get(Calendar.YEAR), dateSelected.get(Calendar.MONTH),
                            dateSelected.get(Calendar.DAY_OF_MONTH));
                    DatePicker dp = dpd.getDatePicker();
                    dp.setMinDate(dateSelected.getTimeInMillis());
                    dpd.show();
                }
            });
            openClock.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Calendar mcurrentTime = Calendar.getInstance();
                    int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                    int minute = mcurrentTime.get(Calendar.MINUTE);
                    TimePickerDialog mTimePicker;
                    mTimePicker = new TimePickerDialog(mCtx, new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                            openClock.setText( selectedHour + ":" + selectedMinute);
                            SharedPreferences sharedPreferences = mCtx.getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPreferences.edit ();
                            String timeList = selectedHour + ":" + selectedMinute +":00";
                            editor.putString ("selectedTime",timeList).apply ();
                        }
                    }, hour, minute, true);//Yes 24 hour time
                    mTimePicker.setTitle("Select Time");
                    mTimePicker.show();
                }
            });
            dialog.show ();
        }
    }

    public Cursor getSubsMasterInfo(int subsId){
        SQLiteDatabase myDatabase = mCtx.openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        Cursor resultSet = myDatabase.rawQuery ("SELECT * from subsMaster where subsId='"+subsId+"'",null);
        return resultSet;
    }

    public Cursor getItemInfo(int itemId){
        SQLiteDatabase myDatabase = mCtx.openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        Cursor resultSet = myDatabase.rawQuery ("SELECT * from itemListMaster where itemId='"+itemId+"'",null);
        return resultSet;
    }

    public void updateCartSubsInfo(int cartId, String startDate, String startTime, int amount, int subsId, int subsCharge){
        SQLiteDatabase myDatabase = mCtx.openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        float cost = getCartCost (cartId);
        cost = cost*amount;
        myDatabase.execSQL ("UPDATE cartInfo set subsInfo='"+subsId+"',startDate='"+startDate+"',startTime='"+startTime+"',cost='"+cost+"',subsCharge='"+subsCharge+"' where cartId='"+cartId+"'");
    }

    public Cursor getSubsPreList(int shopId){
        SQLiteDatabase myDatabase = mCtx.openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        Cursor resultSet = myDatabase.rawQuery ("SELECT * from subsData where shopId='"+shopId+"'",null);
        return resultSet;
    }

    public String getTime(int timeId){
        SQLiteDatabase myDatabase = mCtx.openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        Cursor resultSet = myDatabase.rawQuery ("SELECT * from subsData where timeId='"+timeId+"'",null);
        resultSet.moveToFirst ();
        String retName = resultSet.getString (4);
        myDatabase.close ();
        return retName;
    }

    public void goToCart(View view){
        if(sharedPreferences.getFloat ("totalBill",0.0f)>0.0f){
            if(sharedPreferences.getString ("sessionInfo","").equals ("")){
                Intent intent = new Intent (this, loginSignup.class);
                intent.putExtra("nextPage","cart");
                startActivity (intent);
                finish ();
            }
            else{
                if(sharedPreferences.getInt ("addId",0)==0){
                    Intent intent = new Intent(this, manageAddress.class);
                    intent.putExtra("nextPage","cart");
                    startActivity(intent);
                    finish ();
                }
                else{
                    Intent intent = new Intent(this, cartPage1.class);
                    startActivity (intent);
                    finish ();
                }
            }
        }
    }

    public void noShopReload(View view){
        goToPrevious ();
    }

    public void onCall(long telNumber1) {
        String telUse = "tel:"+telNumber1;
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse(telUse));
        startActivity(intent);
        finish ();
    }
}
