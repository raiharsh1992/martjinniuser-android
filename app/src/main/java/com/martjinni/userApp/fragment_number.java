package com.martjinni.userApp;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.RequestQueue;

import org.json.JSONException;


public class fragment_number extends Fragment {

    TextView fragmentAction;
    EditText number;
    String num;
    public RequestQueue mrequest;
    volleyCall newClass = new volleyCall();
    public String baseUrl = newClass.baseUrl;
    public fragment_number() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_restore_number, container, false);
        final EditText numberField = view.findViewById(R.id.mobnumberrestore);

        fragmentAction = view.findViewById(R.id.sessionactions);
        fragmentAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    num = numberField.getText().toString();
                    Log.d("Phonenum",num);
                    ((changepassword)getActivity()).validatenumber(num);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        return view;
    }

}
