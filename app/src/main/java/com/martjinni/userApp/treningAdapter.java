package com.martjinni.userApp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class treningAdapter extends RecyclerView.Adapter<treningAdapter.trendingViewHolder>{

    private Context mCtx;
    private List<trending_view> trendList;

    public treningAdapter(Context mCtx, List<trending_view> trendList) {
        this.mCtx = mCtx;
        this.trendList = trendList;
    }

    @NonNull
    @Override
    public trendingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from (mCtx);
        View view = inflater.inflate (R.layout.trend_card,null);
        return new trendingViewHolder (view);
    }

    @Override
    public void onBindViewHolder(@NonNull trendingViewHolder holder, int position) {
        trending_view trend = trendList.get (position);
        trend_card_view_adapter adapter = new trend_card_view_adapter (mCtx,trend.getUseInfo ());
        holder.recyclerViewTend.setHasFixedSize (false);
        holder.recyclerViewTend.setLayoutManager (new LinearLayoutManager (mCtx, LinearLayoutManager.HORIZONTAL, false));
        holder.recyclerViewTend.setAdapter (adapter);
        holder.catName.setText (trend.getCatName ());
    }

    @Override
    public int getItemCount() {
        return trendList.size ();
    }

    class trendingViewHolder extends RecyclerView.ViewHolder{
        TextView catName;
        RecyclerView recyclerViewTend;
        public trendingViewHolder(View itemView){
            super(itemView);
            catName = itemView.findViewById (R.id.catName);
            recyclerViewTend = itemView.findViewById (R.id.trend_card);
        }
    }
}
