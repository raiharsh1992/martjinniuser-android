package com.martjinni.userApp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

public class changepassword extends AppCompatActivity {
    String returnPage;
    final fragment_number fnum = new fragment_number();
    final fragment_getotp fotp = new fragment_getotp();
    final fragment_getpassword fgetpass = new fragment_getpassword();

    public RequestQueue mRequestQueue;
    volleyCall newClass = new volleyCall();
    public String baseUrl = newClass.baseUrl;
    String number = "";
    String Otp = "";
    String pass1 = "";
    String pass2 = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_changepassword);
        openFragment(fnum);
        returnPage = (getIntent ().getStringExtra ("nextPage"));
    }
    private void openFragment(final Fragment fragment){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(android.R.anim.slide_out_right, android.R.anim.slide_in_left);
        if(fragment == fnum){
            transaction.replace(R.id.sessioncontainer, fragment,"FRAG");
        }
        else if (fragment==fotp){
            transaction.replace(R.id.sessioncontainer, fragment, "FRAG");
        }
        else{
            transaction.replace(R.id.sessioncontainer, fragment,"FRAG");
        }
        transaction.commit();
    }

    public void redirect(){
        if(returnPage.equals ("account")){
            Intent intent = new Intent(changepassword.this, accountPage.class);
            startActivity(intent);
            finish();
        }
        else{
            Intent intent = new Intent(changepassword.this, loginSignup.class);
            intent.putExtra ("nextPage",returnPage);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onBackPressed(){
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag ("FRAG");
        if(fragment==fnum){
            redirect ();
        }
        else if (fragment==fotp){
            openFragment(fnum);
        }
        else{
            openFragment (fotp);
        }
    }

    public void alertshow(String message){
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(changepassword.this);
        View mView = getLayoutInflater().inflate(R.layout.alertdialog, null);
        TextView messageshow = mView.findViewById(R.id.errormessage);
        messageshow.setText(message);
        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        dialog.show();
        TextView close = mView.findViewById(R.id.modalclose);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
    }



    public void generateOtp(){
        try {
            Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024);
            Network network = new BasicNetwork(new HurlStack());
            mRequestQueue = new RequestQueue(cache, network);
            mRequestQueue.start();
            JSONObject jsonVal = new JSONObject();
            jsonVal.put("phoneNumber", Double.parseDouble(number));
            jsonVal.put("userType", "CUST");
            jsonVal.put("userNeed", "CHNGPWD");
            String userUrl = baseUrl + "generateotp";
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, userUrl, jsonVal, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Toast.makeText(changepassword.this,"OTP generated. Please check your phone.", Toast.LENGTH_LONG).show();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });
            mRequestQueue.add(jsonObjectRequest);
        } catch (JSONException jsonException) {
            jsonException.printStackTrace();
        }

    }

    public void validatenumber(final String num) throws JSONException {
        SharedPreferences sharedPreferences = getSharedPreferences("userInfo", Context.MODE_PRIVATE);
      if(sharedPreferences.getString("sessionInfo", "").equals("")){

        //Not logged in, valisdate phone with API
          if(num.length()==10){
              try{
                  Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024);
                  Network network = new BasicNetwork(new HurlStack());
                  mRequestQueue = new RequestQueue(cache, network);
                  mRequestQueue.start();
                  JSONObject jsonVal = new JSONObject ();
                  jsonVal.put ("phoneNumber", Double.parseDouble(num));
                  jsonVal.put("userType", "CUST");
                  String userUrl = baseUrl+"validphone";
                  JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, userUrl, jsonVal,  new Response.Listener<JSONObject>() {
                      @Override
                      public void onResponse(JSONObject response) {
                          Toast.makeText(getApplicationContext(), "Please enter a valid Number ", Toast.LENGTH_LONG).show();
                      }
                  }, new Response.ErrorListener() {
                      @Override
                      public void onErrorResponse(VolleyError error) {
                          Toast.makeText(getApplicationContext(), "Otp has been sent to your Number", Toast.LENGTH_LONG).show();
                           number = num;
                          generateOtp();
                          openFragment(fotp);

                      }
                  });
                  mRequestQueue.add(jsonObjectRequest);
              }
              catch (JSONException jsonException){
                  jsonException.printStackTrace ();
              }
          }
          else{
              Toast.makeText(getApplicationContext(),"Enter a valid Number of 10 Digit ", Toast.LENGTH_LONG).show();

          }
      }
      else {
          //Logged in, validate if phonenumber is same as sessioninfo.
          JSONObject sessionInfo = new JSONObject(sharedPreferences.getString("sessionInfo", ""));
          String sessionNum = "";
          Log.d("Phonenumber", num);

          sessionNum = sessionInfo.getString("phoneNumber");

          if (!sessionNum.equals(num)) {
              Toast.makeText(this, "The entered number does not match with your registered number. ", Toast.LENGTH_LONG).show();
          } else {
              number = num;
              Toast.makeText(getApplicationContext(), "Otp has been sent to your Number", Toast.LENGTH_LONG).show();
              generateOtp ();
              openFragment(fotp);
          }
      }
      }

    public void validateotp(final String enteredotp){
        Double otpgot = Double.parseDouble(enteredotp);
          try {
              Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024);
              Network network = new BasicNetwork(new HurlStack());
              mRequestQueue = new RequestQueue(cache, network);
              mRequestQueue.start();
              JSONObject jsonVal = new JSONObject();
              jsonVal.put("phoneNumber", Double.parseDouble(number));
              jsonVal.put("userType", "CUST");
              jsonVal.put("otp", otpgot);
              String userUrl = baseUrl + "isvalidchangeotp";
              JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, userUrl, jsonVal, new Response.Listener<JSONObject>() {
                  @Override
                  public void onResponse(JSONObject response) {
                      Log.i("responseotp", response.toString());
                      String res="";
                      try {
                          res = response.getString("Data");
                      } catch (JSONException e) {
                          e.printStackTrace();
                      }
                      if(res.equals("false")){
                          Toast.makeText(changepassword.this, "Wrong OTP entered", Toast.LENGTH_LONG).show();

                      }
                      else {
                          Otp = enteredotp;
                          Toast.makeText(changepassword.this, "Enter Your new password", Toast.LENGTH_LONG).show();
                            openFragment(fgetpass);
                      }
                  }
              }, new Response.ErrorListener() {
                  @Override
                  public void onErrorResponse(VolleyError error) {
                      Toast.makeText(changepassword.this, "Wrong otp entered ", Toast.LENGTH_LONG).show();
                      error.printStackTrace();
                      Log.i("responseotperror", error.toString());
                  }
              });
              mRequestQueue.add(jsonObjectRequest);
          } catch (JSONException jsonException) {
              jsonException.printStackTrace();
          }
    }
    public void changepass(final String pass){
        try {
            Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024);
            Network network = new BasicNetwork(new HurlStack());
            mRequestQueue = new RequestQueue(cache, network);
            mRequestQueue.start();
            JSONObject jsonVal = new JSONObject();
            jsonVal.put("phoneNumber", Double.parseDouble(number));
            jsonVal.put("userType", "CUST");
            jsonVal.put("otp", Otp);
            jsonVal.put("password", pass);
            String userUrl = baseUrl + "chngpwd";
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, userUrl, jsonVal, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    String res;
                    try {
                        res = response.getString("Data");
                        if(res.equals("Update successful"))
                        {
                            Toast.makeText(changepassword.this, "Password updated successfully!", Toast.LENGTH_LONG).show ();
                            if(returnPage.equals ("account")){
                                Intent intent = new Intent(changepassword.this, accountPage.class);
                                startActivity(intent);
                                finish();
                            }
                            else{
                                Intent intent = new Intent(changepassword.this, loginSignup.class);
                                intent.putExtra ("nextPage",returnPage);
                                startActivity(intent);
                                finish();
                            }
                        }
                        else
                        {
                            Toast.makeText(changepassword.this,"Issue while changing password.", Toast.LENGTH_SHORT).show ();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(changepassword.this, "Issue while changing password.", Toast.LENGTH_LONG).show();
                    error.printStackTrace();
                    Log.i("responseotperror", error.toString());
                }
            });
            mRequestQueue.add(jsonObjectRequest);
        }
        catch (JSONException jsonException) {
            jsonException.printStackTrace();
        }
    }


}
