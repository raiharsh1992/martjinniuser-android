package com.martjinni.userApp;

public class itemCardClass {
    String itemName,displayPrice,vegNon;
    int subItemId,itemCount,customIf;

    public itemCardClass(String itemName, String displayPrice, int subItemId, String vegNon, int itemCount, int customIf) {
        this.itemName = itemName;
        this.displayPrice = displayPrice;
        this.subItemId = subItemId;
        this.vegNon = vegNon;
        this.itemCount = itemCount;
        this.customIf = customIf;
    }

    public String getItemName() {
        return itemName;
    }

    public String getDisplayPrice() {
        return displayPrice;
    }

    public int getSubItemId() {
        return subItemId;
    }

    public String getVegNon() {
        return vegNon;
    }

    public int getItemCount() {
        return itemCount;
    }

    public int getCustomIf() {
        return customIf;
    }
}
