package com.martjinni.userApp;

public class shopCard {
    private String minCharge,delCharge,shopName,shopCat,shopLocation,imgUrl;
    private float rating;
    int shopId,status;

    public shopCard(String shopName, String shopCat, String shopLocation, String imgUrl, String minCharge, String delCharge, float rating, int shopId, int status) {
        this.shopName = shopName;
        this.shopCat = shopCat;
        this.shopLocation = shopLocation;
        this.imgUrl = imgUrl;
        this.minCharge = minCharge;
        this.delCharge = delCharge;
        this.rating = rating;
        this.shopId = shopId;
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    public int getShopId() {
        return shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public String getShopCat() {
        return shopCat;
    }

    public String getShopLocation() {
        return shopLocation;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public String getMinCharge() {
        return minCharge;
    }

    public String getDelCharge() {
        return delCharge;
    }

    public float getRating() {
        return rating;
    }
}
