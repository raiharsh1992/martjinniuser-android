package com.martjinni.userApp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class quantViewAdapter extends RecyclerView.Adapter<quantViewAdapter.quantViewHolder>{

    Context mCtx;
    List<quantClass> quantList;
    AlertDialog dialog;

    public quantViewAdapter(Context mCtx, List<quantClass> quantList,AlertDialog dialog) {
        this.mCtx = mCtx;
        this.quantList = quantList;
        this.dialog=dialog;
    }

    @NonNull
    @Override
    public quantViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from (mCtx);
        View view = inflater.inflate (R.layout.quantlist,null);
        return new quantViewHolder (view);
    }

    @Override
    public void onBindViewHolder(@NonNull quantViewHolder holder, int position) {
        quantClass quantUse = quantList.get (position);
        holder.quantName.setText (quantUse.getQuantName ());
        holder.valueText.setText (("\u20B9 " + String.valueOf (quantUse.getValueQuant ())));
        holder.mrpText.setText (("\u20B9 " + String.valueOf (quantUse.getMrp ())));
    }

    @Override
    public int getItemCount() {
        return quantList.size ();
    }

    class quantViewHolder extends RecyclerView.ViewHolder{
        TextView quantName, valueText, mrpText, addButton;
        public  quantViewHolder(final View itemView){
            super(itemView);
            quantName = itemView.findViewById (R.id.quantName);
            mrpText = itemView.findViewById (R.id.itemMrp);
            valueText = itemView.findViewById (R.id.itemValue);
            addButton = itemView.findViewById (R.id.addButton);
            addButton.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition ();
                    workWithQuantity(position);
                }
            });
        }
    }

    public void workWithQuantity(final int position){
        quantClass quantUse = quantList.get (position);
        Cursor resultSet = getItemInfo (quantUse.getItemId ());
        resultSet.moveToFirst ();
        int quant = resultSet.getInt (10);
        int val = quantUse.getValueQuant ();
        int gstPer = quantUse.getGst ();
        float gstCalc = (gstPer/100.0f)*val;
        float itemValue = gstCalc + val;
        int cartId = insertIntoCartInfoDefault(1, quantUse.getItemId (), resultSet.getString (1),quantUse.getQuantId (), "", 0, "", "", itemValue, gstCalc);
        updateTotalValue (itemValue);
        if(cartId>0){
            int useQuant = quant;
            updateItemCountMaster (quantUse.getItemId (),useQuant+1);
            ((itemsPage)mCtx).updateDisplayView (useQuant,quantUse.getItemId ());
            if(!resultSet.getString (4).equals ("")){
                String [] addOnList = resultSet.getString (4).split (",");
                ((itemsPage)mCtx).workWithAddOn (quantUse.getItemId (),cartId,addOnList);
                dialog.dismiss ();
            }
            else if(!resultSet.getString (5).equals ("")){
                ((itemsPage)mCtx).workWithSubscription(quantUse.getItemId (),cartId);
                dialog.dismiss ();
            }
            else {
                dialog.dismiss ();
            }
        }

    }

    public void updateTotalValue(float addAmount){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        float totalAmount = sharedPreferences.getFloat ("totalBill",0.0f);
        totalAmount = totalAmount + addAmount;
        View rootView = ((Activity)mCtx).findViewById(R.id.content);
        if(sharedPreferences.getFloat ("totalBill",0.0f)==0.0f){
            TextView showAmount = rootView.findViewById (R.id.totalSum);
            String amount = "Rs : "+totalAmount;
            showAmount.setText (amount);
            CardView showNavigation = rootView.findViewById (R.id.displayNavigation);
            showNavigation.setVisibility (View.VISIBLE);
        }
        else{
            TextView showAmount = rootView.findViewById (R.id.totalSum);
            String amount = "Rs : "+totalAmount;
            showAmount.setText (amount);
        }
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putFloat ("totalBill",totalAmount).apply ();
    }

    public int insertIntoCartInfoDefault(int count, int itemId, String itemName, int quantId, String addonInfo, int subsInfo, String startDate, String startTime, float cost, float gst){
        try{
            SQLiteDatabase myDatabase = mCtx.openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
            myDatabase.execSQL ("INSERT or replace INTO cartInfo(count, itemId, itemName, quantId, addonInfo, subsInfo, startDate, startTime, cost, gst)VALUES('"+count+"','"+itemId+"','"+itemName+"','"+quantId+"','"+addonInfo+"','"+subsInfo+"','"+startDate+"','"+startTime+"','"+cost+"','"+gst+"')");
            Cursor resultSet = myDatabase.rawQuery ("SELECT * from cartInfo", null);
            resultSet.moveToLast ();
            return resultSet.getInt (0);
        }
        catch (android.database.SQLException sqlException){
            sqlException.printStackTrace ();
            return 0;
        }
    }

    public Cursor getItemInfo(int itemId){
        SQLiteDatabase myDatabase = mCtx.openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        Cursor resultSet = myDatabase.rawQuery ("SELECT * from itemListMaster where itemId='"+itemId+"'",null);
        return resultSet;
    }

    public void updateItemCountMaster(int itemId, int quant){
        SQLiteDatabase myDatabase = mCtx.openOrCreateDatabase("martjinniItems",MODE_PRIVATE,null);
        myDatabase.execSQL ("UPDATE itemListMaster set itemCount = '"+quant+"' where itemId='"+itemId+"'");
    }

}
