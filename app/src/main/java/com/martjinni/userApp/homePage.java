package com.martjinni.userApp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.darwindeveloper.horizontalscrollmenulibrary.custom_views.HorizontalScrollMenuView;
import com.darwindeveloper.horizontalscrollmenulibrary.extras.MenuItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import pl.droidsonroids.gif.GifImageView;

public class homePage extends AppCompatActivity {
    List<shopCard> shopList1;
    List<trending_view> trendingViewList;
    public RequestQueue mRequestQueue;
    public SharedPreferences sharedPreferences;
    volleyCall newClass = new volleyCall ();
    public String baseUrl = newClass.baseUrl;
    public HorizontalScrollMenuView menu;
    RecyclerView forDisplayingTrend;
    Context mCtx;
    public GifImageView loader;
    public Toolbar myToolbar;
    private volatile boolean isWorkingOnSomething = false;
    public ImageView menuNot;
    public shopCardAdapter adapter;
    private static final String TAG = "MainActivity";// Will show the string "data" that holds the results

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_home_page);
        mCtx = this;
        sharedPreferences = getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        menu = findViewById(R.id.menu);
        initMenu();
        myToolbar=findViewById(R.id.my_toolbar);
        final String statusDel = sharedPreferences.getString("addressName", "");
        myToolbar.setTitle (statusDel);
        myToolbar.setTitleTextColor (Color.WHITE);
        setSupportActionBar(myToolbar);
        getSupportActionBar ().setDisplayHomeAsUpEnabled (false);
        forDisplayingTrend = findViewById (R.id.my_recycler_view);
        forDisplayingTrend.setHasFixedSize (false);
        forDisplayingTrend.setLayoutManager (new LinearLayoutManager (this));
        forDisplayingTrend.setNestedScrollingEnabled (false);
        forDisplayingTrend.addOnScrollListener(new CustomScrollListener());
        shopList1 = new ArrayList<> ();
        adapter = new shopCardAdapter (mCtx,shopList1);
        loader = findViewById (R.id.preloader);
        menuNot = findViewById (R.id.menu_not);
        menuNot.setVisibility (View.INVISIBLE);
        myToolbar.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                goToHome();
            }
        });
        if(haveNetworkConnection()){
            proceedWithInternet ();
        }
        else clearView ("netWork");
    }

    private void retryInternet(){
        loader.setVisibility (View.VISIBLE);
        shopList1 = new ArrayList<> ();
        trendingViewList = new ArrayList<> ();
        treningAdapter adapterTrend = new treningAdapter (mCtx,trendingViewList);
        forDisplayingTrend.setAdapter (adapterTrend);
        adapter = new shopCardAdapter (mCtx,shopList1);
        forDisplayingTrend.setAdapter (adapter);
        if(haveNetworkConnection()){
            proceedWithInternet ();
        }
        else clearView ("netWork");
    }

    private void proceedWithInternet(){
        handlingInternet runner = new handlingInternet ();
        new Thread (runner).start ();
    }

    private void initMenu() {
        menu.addItem("Trending",R.drawable.ic_trending_up_black_24dp, true);
        menu.addItem("Shops",R.drawable.ic_store_mall_directory_black_24dp, false);
        menu.addItem("Services",R.drawable.ic_build_black_24dp, false);
        menu.addItem("Subscription",R.drawable.ic_date_range_black_24dp, false);
        menu.setOnHSMenuClickListener(new HorizontalScrollMenuView.OnHSMenuClickListener() {
            @Override
            public void onHSMClick(MenuItem menuItem, int position) {
                if((menuItem.getText ()).equals ("Trending")){
                    if(isWorkingOnSomething) {
                        inFlateView ("trend");
                        isWorkingOnSomething = false;
                        menu.setItemSelected (position);
                    }
                    else{
                        Toast.makeText (getApplicationContext (), "Please be patient system is working for you", Toast.LENGTH_SHORT).show ();
                    }
                }
                else if((menuItem.getText ()).equals ("Shops")){
                    if(isWorkingOnSomething){
                        inFlateView ("shop");
                        isWorkingOnSomething = false;
                        menu.setItemSelected (position);
                    }
                    else{
                        Toast.makeText (getApplicationContext (), "Please be patient system is working for you", Toast.LENGTH_SHORT).show ();
                    }
                }
                else if((menuItem.getText ()).equals ("Services")){
                    if(isWorkingOnSomething){
                        inFlateView ("service");
                        isWorkingOnSomething = false;
                        menu.setItemSelected (position);
                    }
                    else{
                        Toast.makeText (getApplicationContext (), "Please be patient system is working for you", Toast.LENGTH_SHORT).show ();
                    }
                }
                else if((menuItem.getText ()).equals ("Subscription")){
                    if(isWorkingOnSomething){
                        isWorkingOnSomething = false;
                        inFlateView ("subs");
                        menu.setItemSelected (position);
                    }
                    else{
                        Toast.makeText (getApplicationContext (), "Please be patient system is working for you", Toast.LENGTH_SHORT).show ();
                    }
                }
                else if((menuItem.getText ()).equals ("Emergency")){
                    if(isWorkingOnSomething){
                        isWorkingOnSomething = false;
                        inFlateView ("emergency");
                        menu.setItemSelected (position);
                    }
                    else{
                        Toast.makeText (getApplicationContext (), "Please be patient system is working for you", Toast.LENGTH_SHORT).show ();
                    }
                }
            }
        });
    }

    public void goToHome(){
        Intent intent = new Intent(this, defaultAddress.class);
        intent.putExtra("nextPage","home");
        startActivity(intent);
        finish ();
    }

    public void exit(){
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(1);
        super.onBackPressed ();
    }

    @Override
    public void onBackPressed()
    {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);
        View mView = getLayoutInflater().inflate(R.layout.alert_dialog2, null);
        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        TextView errorMessage = mView.findViewById (R.id.errormessage);
        String displayMessage = "Do you wish to exit?";
        errorMessage.setText (displayMessage);
        TextView continueView = mView.findViewById (R.id.modalclose);
        continueView.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                dialog.dismiss ();
                exit ();
            }
        });
        TextView cancelView = mView.findViewById (R.id.modalContinue);
        cancelView.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                dialog.dismiss ();
            }
        });
        dialog.show ();
    }


    public void clearView(String reason){
        RelativeLayout layout= findViewById(R.id.home_layout);
        layout.setBackgroundResource(0);
        Button butn = findViewById (R.id.button2);
        butn.setVisibility (View.INVISIBLE);
        butn = findViewById (R.id.button3);
        butn.setVisibility (View.INVISIBLE);
        menu.setVisibility (View.INVISIBLE);
        shopList1 = new ArrayList<> ();
        adapter = new shopCardAdapter (mCtx,shopList1);
        forDisplayingTrend.setAdapter (adapter);
        if(reason.equals ("netWork")){
            myToolbar.setVisibility(View.GONE);
            ImageView menuUse = findViewById (R.id.menu_not1);
            menuUse.setImageResource (R.drawable.layer_1);
            menuUse.setScaleType (ImageView.ScaleType.FIT_XY);
            menuUse.setVisibility (View.VISIBLE);
            Button dispButn = findViewById (R.id.button2);
            dispButn.setVisibility (View.VISIBLE);
            dispButn.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    retryInternet ();
                }
            });
        }
        else if(reason.equals ("noShop")){
            myToolbar.setVisibility(View.GONE);
            Button dispButn = findViewById (R.id.button3);
            ImageView menuUse = findViewById (R.id.menu_not1);
            menuUse.setImageResource (R.drawable.not_in_region_404);
            menuUse.setScaleType (ImageView.ScaleType.FIT_XY);
            menuUse.setVisibility (View.VISIBLE);
            dispButn.setVisibility (View.VISIBLE);
            dispButn.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    redirectAddress ();
                }
            });
        }
        else if(reason.equals ("allClose")){

            Button dispButn = findViewById (R.id.button2);
            dispButn.setVisibility (View.VISIBLE);
            ImageView menuUse = findViewById (R.id.menu_not1);
            menuUse.setImageResource (R.drawable.layer_0);
            menuUse.setScaleType (ImageView.ScaleType.FIT_XY);
            menuUse.setVisibility (View.VISIBLE);
            myToolbar.setVisibility(View.VISIBLE);
            dispButn.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    retryInternet ();
                }
            });
        }
        loader.setVisibility (View.GONE);
    }

    private boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    class handlingInternet implements Runnable{
        @Override
        public void run(){
            String delVal = sharedPreferences.getString("delVal", "");
            if(delVal.equals ("")){
                redirectAddress();
            }
            else{
                if(!(sharedPreferences.getString ("viewTypeFilter","")).equals ("")){
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.remove ("viewTypeFilter").apply ();
                    final RelativeLayout layout = findViewById (R.id.home_layout);
                    final Button butn = findViewById (R.id.button2);
                    shopList1 = new ArrayList<> ();
                    forDisplayingTrend.setAdapter (adapter);
                    updateMenuSelected();
                    final FloatingActionButton fab = findViewById (R.id.fab);
                    final FloatingActionButton fab2 = findViewById (R.id.fab2);
                    insertIntoReclye();
                    runOnUiThread (new Runnable () {
                        @Override
                        public void run() {
                            layout.setBackgroundResource(0);
                            myToolbar.setVisibility (View.VISIBLE);
                            menu.setVisibility (View.VISIBLE);
                            fab.setVisibility (View.VISIBLE);
                            fab2.setVisibility (View.VISIBLE);
                            butn.setVisibility (View.INVISIBLE);
                        }
                    });
                }
                else{
                    String[] separated = delVal.split(",");
                    JSONObject jsonVal = new JSONObject ();
                    try{
                        Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                        Network network = new BasicNetwork (new HurlStack ());
                        mRequestQueue = new RequestQueue (cache, network);
                        mRequestQueue.start();
                        jsonVal.put("lattitude", Double.parseDouble (separated[0]));
                        jsonVal.put("longitude",Double.parseDouble (separated[1]));
                        String finalUrl = baseUrl+"trendlist";
                        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, finalUrl, jsonVal,  new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try{
                                    if((response.getInt ("totalCount"))>0){
                                        if((response.getInt ("openCount")>0)){
                                            final RelativeLayout layout = findViewById (R.id.home_layout);
                                            final Button butn = findViewById (R.id.button2);
                                            runOnUiThread (new Runnable () {
                                                @Override
                                                public void run() {
                                                    layout.setBackgroundResource(0);
                                                    myToolbar.setVisibility (View.VISIBLE);
                                                    menu.setVisibility (View.VISIBLE);
                                                    butn.setVisibility (View.INVISIBLE);
                                                }
                                            });
                                            if(createView (response)){
                                                inFlateView("trend");
                                            }
                                            else{
                                                clearView ("netWork");
                                            }
                                        }
                                        else{
                                            clearView ("allClose");
                                        }
                                    }
                                    else{
                                        clearView ("noShop");
                                    }
                                }
                                catch (JSONException e2){
                                    e2.printStackTrace ();
                                    clearView ("netWork");
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                error.printStackTrace ();
                            }

                        });
                        Log.i(TAG,String .valueOf (jsonVal));
                        mRequestQueue.add(jsonObjectRequest);
                    }
                    catch (Exception e){
                        e.printStackTrace ();
                    }
                }

            }
        }

        private boolean createView(JSONObject response){
            try{
                SQLiteDatabase myDatabase = openOrCreateDatabase("martjinni",MODE_PRIVATE,null);
                myDatabase.execSQL ("DROP TABLE IF EXISTS mjCat");
                myDatabase.execSQL ("DROP TABLE IF EXISTS catList");
                myDatabase.execSQL ("CREATE TABLE IF NOT EXISTS mjCat(mjId INTEGER, mjName VARCHAR, viewType VARCHAR)");
                myDatabase.execSQL ("CREATE TABLE IF NOT EXISTS catList(catId INTEGER, catName VARCHAR, mjId INTEGER, shopList VARCHAR, viewType VARCHAR)");
                JSONObject shopListUse =  response.getJSONObject ("shops");
                JSONObject varShops = shopListUse.getJSONObject("shopList");
                JSONArray userJson = varShops.getJSONArray ("open");
                String shopListOpen = "";
                for(int i =0;i<userJson.length ();i++){
                    if(shopListOpen.equals ("")){
                        shopListOpen = userJson.getString (i);
                    }
                    else{
                        shopListOpen = shopListOpen+","+ userJson.getString (i);
                    }
                }
                userJson = varShops.getJSONArray ("close");
                for(int i =0;i<userJson.length ();i++){
                    if(shopListOpen.equals ("")){
                        shopListOpen = userJson.getString (i);
                    }
                    else{
                        shopListOpen = shopListOpen+","+ userJson.getString (i);
                    }
                }
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString ("shops",shopListOpen).apply ();
                varShops = shopListUse.getJSONObject ("filterData");
                if(varShops.getInt ("Data")>0){
                    JSONArray filterInfo = varShops.getJSONArray ("mjAvailable");
                    for(int i=0;i<filterInfo.length ();i++){
                        JSONObject mjInfo = filterInfo.getJSONObject (i);
                        int mjId = mjInfo.getInt ("mjId");
                        String mjName = mjInfo.getString ("mjName");
                        myDatabase.execSQL ("INSERT or replace INTO mjCat VALUES ("+mjId+",'"+mjName+"','shop');");
                    }
                    filterInfo = varShops.getJSONArray ("catAvailable");
                    for(int i=0;i<filterInfo.length ();i++){
                        JSONObject catInfo = filterInfo.getJSONObject (i);
                        int catId = catInfo.getInt ("catId");
                        String catName = catInfo.getString ("catName");
                        int mjId = catInfo.getInt ("catMJ");
                        String shopList = catInfo.getJSONArray ("shopList").toString ();
                        myDatabase.execSQL ("INSERT or replace INTO catList VALUES("+catId+",'"+catName+"',"+mjId+",'"+shopList+"','shop');");
                    }
                }

                shopListUse =  response.getJSONObject ("service");
                varShops = shopListUse.getJSONObject("shopList");
                userJson = varShops.getJSONArray ("open");
                shopListOpen = "";
                for(int i =0;i<userJson.length ();i++){
                    if(shopListOpen.equals ("")){
                        shopListOpen = userJson.getString (i);
                    }
                    else{
                        shopListOpen = shopListOpen+","+ userJson.getString (i);
                    }
                }
                userJson = varShops.getJSONArray ("close");
                for(int i =0;i<userJson.length ();i++){
                    if(shopListOpen.equals ("")){
                        shopListOpen = userJson.getString (i);
                    }
                    else{
                        shopListOpen = shopListOpen+","+ userJson.getString (i);
                    }
                }
                editor.putString ("service",shopListOpen).apply ();
                varShops = shopListUse.getJSONObject ("filterData");
                if(varShops.getInt ("Data")>0){
                    JSONArray filterInfo = varShops.getJSONArray ("mjAvailable");
                    for(int i=0;i<filterInfo.length ();i++){
                        JSONObject mjInfo = filterInfo.getJSONObject (i);
                        int mjId = mjInfo.getInt ("mjId");
                        String mjName = mjInfo.getString ("mjName");
                        myDatabase.execSQL ("INSERT or replace INTO mjCat VALUES ("+mjId+",'"+mjName+"','service');");
                    }
                    filterInfo = varShops.getJSONArray ("catAvailable");
                    for(int i=0;i<filterInfo.length ();i++){
                        JSONObject catInfo = filterInfo.getJSONObject (i);
                        int catId = catInfo.getInt ("catId");
                        String catName = catInfo.getString ("catName");
                        int mjId = catInfo.getInt ("catMJ");
                        String shopList = catInfo.getJSONArray ("shopList").toString ();
                        myDatabase.execSQL ("INSERT or replace INTO catList VALUES("+catId+",'"+catName+"',"+mjId+",'"+shopList+"','service');");
                    }
                }

                shopListUse =  response.getJSONObject ("subs");
                varShops = shopListUse.getJSONObject("shopList");
                shopListOpen = "";
                userJson = varShops.getJSONArray ("open");
                for(int i =0;i<userJson.length ();i++){
                    if(shopListOpen.equals ("")){
                        shopListOpen = userJson.getString (i);
                    }
                    else{
                        shopListOpen = shopListOpen+","+ userJson.getString (i);
                    }
                }
                userJson = varShops.getJSONArray ("close");
                for(int i =0;i<userJson.length ();i++){
                    if(shopListOpen.equals ("")){
                        shopListOpen = userJson.getString (i);
                    }
                    else{
                        shopListOpen = shopListOpen+","+ userJson.getString (i);
                    }
                }
                editor.putString ("subs",shopListOpen).apply ();
                varShops = shopListUse.getJSONObject ("filterData");
                if(varShops.getInt ("Data")>0){
                    JSONArray filterInfo = varShops.getJSONArray ("mjAvailable");
                    for(int i=0;i<filterInfo.length ();i++){
                        JSONObject mjInfo = filterInfo.getJSONObject (i);
                        int mjId = mjInfo.getInt ("mjId");
                        String mjName = mjInfo.getString ("mjName");
                        myDatabase.execSQL ("INSERT or replace INTO mjCat  VALUES("+mjId+",'"+mjName+"','subs');");
                    }
                    filterInfo = varShops.getJSONArray ("catAvailable");
                    for(int i=0;i<filterInfo.length ();i++){
                        JSONObject catInfo = filterInfo.getJSONObject (i);
                        int catId = catInfo.getInt ("catId");
                        String catName = catInfo.getString ("catName");
                        int mjId = catInfo.getInt ("catMJ");
                        String shopList = catInfo.getJSONArray ("shopList").toString ();
                        myDatabase.execSQL ("INSERT or replace INTO catList VALUES("+catId+",'"+catName+"','"+mjId+"','"+shopList+"','subs');");
                    }
                }
                myDatabase.close ();
                return true;
            }
            catch (JSONException e){
                e.printStackTrace ();
                return  false;
            }
        }
    }



    public void inFlateView(String pageSet){
        loader.setVisibility (View.VISIBLE);
        shopList1 = new ArrayList<> ();
        adapter = new shopCardAdapter (mCtx,shopList1);
        forDisplayingTrend.setAdapter (adapter);
        inflatingView runner = new inflatingView (pageSet);
        isWorkingOnSomething = false;
        new Thread (runner).start ();
        ImageView menUse = findViewById (R.id.menu_not1);
        menUse.setVisibility (View.INVISIBLE);
    }

    class inflatingView implements Runnable{
        String pageSet;
        public inflatingView(String pageSet) {
            this.pageSet = pageSet;
        }
        @Override
        public void run(){
            isWorkingOnSomething = false;
            cancelAll ();
            if(pageSet.equals ("trend")){
                sharedPreferences.edit().putString ("viewType","trend").apply ();
                final FloatingActionButton fab = findViewById (R.id.fab);
                final FloatingActionButton fab2 = findViewById (R.id.fab2);
                runOnUiThread (new Runnable () {
                    @Override
                    public void run() {
                        fab2.setVisibility (View.INVISIBLE);
                        fab.setVisibility (View.INVISIBLE);
                        menuNot.setVisibility (View.INVISIBLE);
                    }
                });
                displayTrending();
            }
            else if(pageSet.equals ("shop")){
                runOnUiThread (new Runnable () {
                    @Override
                    public void run() {
                        menuNot.setVisibility (View.INVISIBLE);
                    }
                });
                String shopList = sharedPreferences.getString ("shops","");
                if(shopList.equals ("")){
                    runOnUiThread (new Runnable () {
                        @Override
                        public void run() {
                            menuNot.setVisibility (View.VISIBLE);
                            menuNot.setScaleType (ImageView.ScaleType.FIT_XY);
                            loader.setVisibility (View.INVISIBLE);
                            isWorkingOnSomething = true;
                        }
                    });
                }
                else{
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString ("activeList",shopList).apply ();
                    editor.putString ("viewType","shop").apply ();
                    insertIntoReclye ();
                }
            }
            else if(pageSet.equals ("service")){
                runOnUiThread (new Runnable () {
                    @Override
                    public void run() {
                        menuNot.setVisibility (View.INVISIBLE);
                    }
                });
                String shopList = sharedPreferences.getString ("service","");
                if(shopList.equals ("")){
                    runOnUiThread (new Runnable () {
                        @Override
                        public void run() {
                            menuNot.setVisibility (View.VISIBLE);
                            menuNot.setScaleType (ImageView.ScaleType.FIT_XY);
                            loader.setVisibility (View.INVISIBLE);
                            isWorkingOnSomething = true;
                        }
                    });
                }
                else{
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString ("activeList",shopList).apply ();
                    editor.putString ("viewType","service").apply ();
                    insertIntoReclye ();
                }
            }
            else if(pageSet.equals ("subs")){
                runOnUiThread (new Runnable () {
                    @Override
                    public void run() {
                        menuNot.setVisibility (View.INVISIBLE);
                    }
                });
                String shopList = sharedPreferences.getString ("subs","");
                if(shopList.equals ("")){
                    runOnUiThread (new Runnable () {
                        @Override
                        public void run() {
                            menuNot.setVisibility (View.VISIBLE);
                            menuNot.setScaleType (ImageView.ScaleType.FIT_XY);
                            loader.setVisibility (View.INVISIBLE);
                            isWorkingOnSomething = true;
                        }
                    });
                }
                else{
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString ("activeList",shopList).apply ();
                    editor.putString ("viewType","subs").apply ();
                    insertIntoReclye ();
                }
            }
        }
    }

    public void displayTrending(){
        workingOnDisplayingTrending runner = new workingOnDisplayingTrending ();
        new Thread (runner).start ();
    }

    class workingOnDisplayingTrending implements Runnable{
        SQLiteDatabase myDatabase = openOrCreateDatabase("martjinni",MODE_PRIVATE,null);
        @Override
        public void run(){
            trendingViewList = new ArrayList<> ();
            Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
            Network network = new BasicNetwork (new HurlStack ());
            mRequestQueue = new RequestQueue (cache, network);
            mRequestQueue.start();
            int i = 0;
            final Cursor resultSet = myDatabase.rawQuery ("SELECT * from catList where viewType='shop' ORDER BY catId",null);
            if(resultSet.getCount ()>0){
                resultSet.moveToFirst ();
                int workingLast = resultSet.getCount ();
                if(workingLast>8){
                    workingLast = 8;
                }
                while(i<=workingLast){
                    JSONArray shopArray = new JSONArray ();
                    if(resultSet.getString (3).equals ("[]")){
                        resultSet.moveToNext ();
                        i++;
                    }
                    else{
                        final String catName = resultSet.getString (1);
                        String[] useShopList = (resultSet.getString (3).substring (1,resultSet.getString (3).length ()-1)).split (",");
                        int loveBite = useShopList.length;
                        if(useShopList.length>5) loveBite=5;
                        for(int j=0;j<loveBite;j++){
                            shopArray.put(Integer.valueOf (useShopList[j]));
                        }
                        String userUrl = baseUrl+"getshopinfo";
                        JSONObject jsonVal = new JSONObject ();
                        try{
                            jsonVal.put("shops",shopArray);
                        }
                        catch (JSONException jE){
                            jE.printStackTrace ();
                        }
                        int isDisplaying = 0;
                        if(i==workingLast||i==workingLast-1){
                            isDisplaying = 1;
                        }
                        final int displayObject = isDisplaying;
                        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, userUrl, jsonVal,  new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                insertIntoTrend (response,catName,displayObject);
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // TODO: Handle error
                                clearView ("netWork");
                            }
                        });
                        mRequestQueue.add(jsonObjectRequest);
                        i=i+2;
                        resultSet.moveToNext ();
                    }
                    if(resultSet.isAfterLast ()){
                        break;
                    }
                }
            }
            i = 0;
            final Cursor resultSet1 = myDatabase.rawQuery ("SELECT * from catList where viewType='service' order by catId",null);
            if(resultSet1.getCount ()>0){
                resultSet1.moveToFirst ();
                int workingLast = resultSet1.getCount ();
                if(workingLast>8){
                    workingLast = 8;
                }
                while(i<=workingLast){
                    JSONArray shopArray = new JSONArray ();
                    if(resultSet1.getString (3).equals ("[]")){
                        resultSet1.moveToNext ();
                        i++;
                        if(i>workingLast) {
                            treningAdapter adapter = new treningAdapter (mCtx,trendingViewList);
                            addTrendAdapter (adapter);
                        }
                    }
                    else{
                        final String catName = resultSet1.getString (1);
                        String[] useShopList = (resultSet1.getString (3).substring (1,resultSet1.getString (3).length ()-1)).split (",");
                        int loveBite = useShopList.length;
                        if(useShopList.length>5) {
                            loveBite=5;
                        }
                        for(int j=0;j<loveBite;j++){
                            shopArray.put(Integer.valueOf (useShopList[j]));
                        }
                        String userUrl = baseUrl+"getshopinfo";
                        JSONObject jsonVal = new JSONObject ();
                        try{
                            jsonVal.put("shops",shopArray);
                        }
                        catch (JSONException jE){
                            jE.printStackTrace ();
                        }
                        int isDisplaying = 0;
                        if(i==workingLast||i==workingLast-1){
                            isDisplaying = 1;
                        }
                        final int displayObject = isDisplaying;
                        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, userUrl, jsonVal,  new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                insertIntoTrend (response,catName,displayObject);
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                clearView ("netWork");
                            }
                        });
                        mRequestQueue.add(jsonObjectRequest);
                        i=i+2;
                        resultSet1.moveToNext ();
                    }
                    if(resultSet1.isAfterLast ()){
                        treningAdapter adapter = new treningAdapter (mCtx,trendingViewList);
                        addTrendAdapter (adapter);
                        break;
                    }
                }
            }
            else{
                treningAdapter adapter = new treningAdapter (mCtx,trendingViewList);
                addTrendAdapter (adapter);
            }
        }

        private void insertIntoTrend(JSONObject response, String catName, int isLast){
            try{
                final List <trend_card_view> shopListUse = new ArrayList<> ();
                Log.i(TAG,response.toString ());
                if(response.getInt ("count")>0){
                    JSONArray useInfo = response.getJSONArray ("Data");
                    for(int i =0;i<useInfo.length ();i++){
                        JSONObject shopJson = useInfo.getJSONObject (i);
                        String shopUrl;
                        if(shopJson.getInt ("imageUploaded")==1){
                            shopUrl = newClass.shopUrl+shopJson.getInt ("shopId")+".jpeg";
                        }
                        else{
                            shopUrl = "https://www.martjinni.com/images/noImage.gif";
                        }
                        String minCharge = "";
                        if(shopJson.getString ("viewType").equals ("shop")){
                            minCharge = "Del Free after : "+shopJson.getInt ("minOrderValue");
                        }
                        float useRating = Float.parseFloat (shopJson.getString ("rating"));
                        int shopId = Integer.parseInt (shopJson.getString ("shopId"));
                        if(shopJson.getInt("isOpen")==1){
                            shopListUse.add(new trend_card_view (shopUrl,shopJson.getString ("shopName"),minCharge,useRating,shopId));
                        }
                    }
                    trendingViewList.add(new trending_view (catName,shopListUse));
                    if(isLast==1){
                        treningAdapter adapter = new treningAdapter (mCtx,trendingViewList);
                        addTrendAdapter (adapter);
                    }
                }

            }
            catch (JSONException e2){
                e2.printStackTrace ();
                clearView ("netWork");
            }
        }

        private void addTrendAdapter(final treningAdapter adapter){
            myDatabase.close ();
            runOnUiThread (new Runnable () {
                @Override
                public void run() {
                    Log.i("WOW","YAHA");
                    loader.setVisibility (View.GONE);
                    forDisplayingTrend.setAdapter (adapter);
                    isWorkingOnSomething = true;
                }
            });
        }

    }

    public void updateMenuSelected(){
        String viewTYpe = sharedPreferences.getString ("viewType","");
        if(viewTYpe.equals ("shop")){
            menu.setItemSelected (1);
        }
        else if(viewTYpe.equals ("service")){
            menu.setItemSelected (2);
        }
        else if(viewTYpe.equals ("subs")){
            menu.setItemSelected (3);
        }
    }

    public void showFilters(View view){
        Intent intent = new Intent(this, filter_activity.class);
        startActivity(intent);
        finish ();
    }

    private void redirectAddress(){
        Intent intent = new Intent(this, defaultAddress.class);
        intent.putExtra("nextPage","home");
        startActivity(intent);
        finish ();
    }

    public void clearViewFilter(View view){
        if(sharedPreferences.getString ("viewType","").equals ("shop")){
            sharedPreferences.edit ().putString ("activeList",sharedPreferences.getString ("shops","")).apply ();
            inFlateView ("shop");
        }
        else if(sharedPreferences.getString ("viewType","").equals ("service")){
            sharedPreferences.edit ().putString ("activeList",sharedPreferences.getString ("service","")).apply ();
            inFlateView ("service");
        }
        else{
            sharedPreferences.edit ().putString ("activeList",sharedPreferences.getString ("subs","")).apply ();
            inFlateView ("subs");
        }
        FloatingActionButton fab = findViewById (R.id.fab2);
        fab.setVisibility (View.GONE);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.optionmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item){
        if(item.getItemId ()==R.id.action_account){
            goToAccount ();
        }
        else if(item.getItemId ()==R.id.action_favorite){
            goToCartShop ();
        }
        return true;
    }

    public void goToAccount(){
        Intent intent = new Intent(this, accountPage.class);
        startActivity(intent);
        finish ();
    }

    public void goToCartShop(){
        if(sharedPreferences.getFloat ("totalBill",0.0f)!=0.0f){
            if(sharedPreferences.getString ("sessionInfo","").equals ("")){
                Intent intent = new Intent(this, loginSignup.class);
                intent.putExtra("nextPage","cart");
                startActivity(intent);
                finish ();
            }
            else{
                if(sharedPreferences.getInt ("addId",0)==0){
                    Intent intent = new Intent(this, manageAddress.class);
                    intent.putExtra("nextPage","cart");
                    startActivity(intent);
                    finish ();
                }
                else{
                    Intent intent = new Intent(this, cartPage1.class);
                    startActivity(intent);
                    finish ();
                }
            }
        }
        else{
            Toast.makeText(this,"Cart is empty", Toast.LENGTH_SHORT).show();
        }
    }

    public void insertIntoReclye(){
        insertingIntoRecyle runner = new insertingIntoRecyle();
        new Thread (runner).start ();
    }

    class insertingIntoRecyle implements Runnable{
        @Override
        public void run(){
            Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
            Network network = new BasicNetwork (new HurlStack ());
            mRequestQueue = new RequestQueue (cache, network);
            mRequestQueue.start();
            final String shopListAvail = sharedPreferences.getString ("activeList","");
            String[] shopListUse = shopListAvail.split (",");
            JSONArray shopArray = new JSONArray ();
            String userShopList = "";
            if(!shopListAvail.equals ("")){
                if(!(sharedPreferences.getString ("viewType","")).equals ("trend")){
                    if(!(sharedPreferences.getString ("viewType","")).equals ("emergency")){
                        if(shopListUse.length>0){
                            if(shopListUse.length>5){
                                for(int i = 0;i<5;i++){
                                    if(!((shopListUse[i]).equals ("null")||(shopListUse[i]).equals (""))){
                                        shopArray.put (Integer.valueOf(shopListUse[i]));
                                    }
                                }
                                for(int i = 5; i<shopListUse.length;i++){
                                    if(i==5){
                                        userShopList = String.valueOf (shopListUse[i]);
                                    }
                                    else{
                                        userShopList = userShopList+","+shopListUse[i];
                                    }
                                }
                            }
                            else if(shopListUse.length>0){
                                for(int i =0;i<shopListUse.length;i++){
                                    if(!((shopListUse[i]).equals ("null")||(shopListUse[i]).equals (""))){
                                        shopArray.put (Integer.valueOf(shopListUse[i]));
                                    }
                                }
                            }
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString ("activeList",userShopList).apply ();
                        }
                        else{
                            Toast.makeText (getApplicationContext (), "End of list", Toast.LENGTH_SHORT).show ();
                        }
                        String userUrl = baseUrl+"getshopinfo";
                        JSONObject jsonVal = new JSONObject ();
                        try{
                            jsonVal.put("shops",shopArray);
                        }
                        catch (JSONException jE){
                            jE.printStackTrace ();
                        }
                        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, userUrl, jsonVal,  new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                createWorkBoard (response);
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // TODO: Handle error
                                error.printStackTrace ();
                            }
                        });
                        mRequestQueue.add(jsonObjectRequest);
                    }
                    else{
                        displayEmergency();
                    }
                }
                else{
                    displayTrending();
                }
            }
            else{
                isWorkingOnSomething = true;
            }
        }

        private void createWorkBoard(JSONObject response){
            final FloatingActionButton fab = findViewById (R.id.fab);
            final int oldPosition = shopList1.size ();
            final SQLiteDatabase myDatabase = openOrCreateDatabase("martjinni",MODE_PRIVATE,null);
            final String viewType = sharedPreferences.getString ("viewType","");
            try{
                Log.i(TAG,response.toString ());
                if(response.getInt ("count")>0){
                    JSONArray useInfo = response.getJSONArray ("Data");
                    for(int i =0;i<useInfo.length ();i++){
                        JSONObject shopJson = useInfo.getJSONObject (i);
                        String shopUrl;
                        if(shopJson.getInt ("imageUploaded")==1){
                            shopUrl = newClass.shopUrl+shopJson.getInt ("shopId")+".jpeg";
                            Log.i("img Url",shopUrl);
                        }
                        else{
                            shopUrl = "https://www.martjinni.com/images/noImage.gif";
                        }
                        float useRating = Float.parseFloat (shopJson.getString ("rating"));
                        String delCharge = "";
                        String minCharge = "";
                        if(viewType.equals ("shop")){
                            delCharge = "Delivery charge : "+shopJson.getInt ("delCharges");
                            minCharge = "Del Free after : "+shopJson.getInt ("minOrderValue");
                        }
                        String [] catId = shopJson.getString ("catId").split (",");
                        String catName ="";
                        for(int j = 0;j<catId.length;j++){
                            int catUseId = Integer.valueOf (catId[j]);
                            Cursor resultSet = myDatabase.rawQuery ("SELECT * from catList where catId="+catUseId,null);
                            resultSet.moveToFirst ();
                            if(j==0){
                                catName = resultSet.getString (1);
                            }
                            else{
                                catName = catName+","+resultSet.getString (1);
                            }
                        }
                        String shopName = shopJson.getString ("shopName");
                        String address = shopJson.getString ("address");
                        shopList1.add(new shopCard (shopName,catName,address,shopUrl,minCharge,delCharge,useRating,shopJson.getInt ("shopId"),shopJson.getInt ("isOpen")));
                    }
                    myDatabase.close ();
                    runOnUiThread (new Runnable () {
                        @Override
                        public void run() {
                            if(oldPosition==0){
                                adapter = new shopCardAdapter (mCtx,shopList1);
                                forDisplayingTrend.setAdapter (adapter);
                                loader.setVisibility (View.GONE);
                                fab.setVisibility (View.VISIBLE);
                            }
                            else{
                                adapter.setShopList (shopList1);
                                adapter.notifyDataSetChanged ();
                            }
                        }
                    });
                    isWorkingOnSomething = true;
                }
            }
            catch (JSONException e2){
                e2.printStackTrace ();
                clearView ("netWork");
            }
        }
    }

    public void gotoItems(int shopId){
        Intent intent = new Intent(mCtx, itemsPage.class);
        intent.putExtra("shopId", String.valueOf (shopId));
        startActivity(intent);
        finish ();
    }

    public void displayEmergency(){

    }

    public class CustomScrollListener extends RecyclerView.OnScrollListener {
        public CustomScrollListener() {
        }
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            switch (newState) {
                case RecyclerView.SCROLL_STATE_IDLE:
                    System.out.println("The RecyclerView is not scrolling");
                    break;
                case RecyclerView.SCROLL_STATE_DRAGGING:
                    System.out.println("Scrolling now");
                    break;
                case RecyclerView.SCROLL_STATE_SETTLING:
                    if(!sharedPreferences.getString ("viewType","").equals ("trend")){
                        insertIntoReclye ();
                    }
                    System.out.println("Scroll Settling");
                    break;
            }
        }
    }

    public void cancelAll(){
        mRequestQueue.cancelAll(new RequestQueue.RequestFilter() {
            @Override
            public boolean apply(Request<?> request) {
                return true;
            }
        });
    }
}
