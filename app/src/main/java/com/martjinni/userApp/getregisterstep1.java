package com.martjinni.userApp;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * A simple {@link Fragment} subclass.
 */
public class getregisterstep1 extends Fragment {

    TextView reg1action;
    EditText fullname;
    EditText username;
    EditText pass1;
    EditText pass2;
    EditText email;

    public RequestQueue mRequestQueue;
    volleyCall newClass = new volleyCall ();
    public String baseUrl = newClass.baseUrl;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_getregisterstep1, container, false);
        ((loginSignup)getActivity()).changeHead("Register");
        reg1action = view.findViewById(R.id.reg1action);
        fullname = view.findViewById(R.id.getfullname);
        username = view.findViewById(R.id.getusername);
        pass1 = view.findViewById(R.id.getpass1);
        pass2 = view.findViewById(R.id.getpass2);
        email = view.findViewById (R.id.getemail);

        reg1action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String emailName = email.getText ().toString ();
                final String fullnamestr = fullname.getText().toString();
                final String usernamestr = username.getText().toString();
                final String pass1str = pass1.getText().toString();
                final String pass2str = pass2.getText().toString();
                if(fullnamestr.equals("")){
                    fullname.setError ("PLease enter a valid Full Name");
                }
                else if(!pass2str.equals(pass1str))
                {
                    pass1.setError ("Entered Password Do not match! Please enter again ");
                }
                else if(pass1str.length()<8)
                {
                    pass1.setError("Password Must be larger than 8 characters");
                }
                else if(pass2str.length()>24){
                    pass1.setError ("Password Must be smaller than 24 characters");
                }
                else if(emailName.equals ("")){
                    email.setError ("Email cannot be left blank");
                }
                else if(!isEmailValid (emailName)){
                    email.setError ("In correct E-mail");
                }
                else {
                    if (!usernamestr.equals("")) {
                        try {
                            Cache cache = new DiskBasedCache(getContext().getCacheDir(), 1024 * 1024);
                            Network network = new BasicNetwork(new HurlStack());
                            mRequestQueue = new RequestQueue(cache, network);
                            mRequestQueue.start();
                            JSONObject jsonVal = new JSONObject();
                            jsonVal.put("userName", usernamestr);
                            jsonVal.put("mode", "client");
                            String userUrl = baseUrl + "validateuser";
                            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, userUrl, jsonVal, new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    ((loginSignup)getActivity()).validatereg1(fullnamestr, usernamestr, pass1str, emailName);
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {

                                    Toast.makeText(getContext(), "Username is already taken" + error.toString(), Toast.LENGTH_LONG).show();
                                    error.printStackTrace();
                                }
                            });
                            mRequestQueue.add(jsonObjectRequest);
                        } catch (JSONException jsonException) {
                            jsonException.printStackTrace();
                        }
                    }
                    else{
                        username.setError ("Username is Empty.");
                    }
                }
            }
        });
        return view;
    }

    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

}
