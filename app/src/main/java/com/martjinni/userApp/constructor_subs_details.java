package com.martjinni.userApp;

public class constructor_subs_details {
    private String day, eta, status;

    public constructor_subs_details(String day, String eta, String status) {
        this.day = day;
        this.eta = eta;
        this.status = status;
    }

    public String getDay() {
        return day;
    }

    public String getEta() {
        return eta;
    }

    public String getStatus() {
        return status;
    }
}
