package com.martjinni.userApp;

public class diplayOrderDetailsItemClass {
    private int itemId, withSubs, orderId;
    private String itemName, addOn, price;

    public diplayOrderDetailsItemClass(int itemId, int withSubs, String itemName, String addOn, String price, int orderId) {
        this.itemId = itemId;
        this.withSubs = withSubs;
        this.itemName = itemName;
        this.addOn = addOn;
        this.price = price;
        this.orderId = orderId;
    }

    public int getOrderId() {
        return orderId;
    }

    public int getItemId() {
        return itemId;
    }

    public int getWithSubs() {
        return withSubs;
    }

    public String getItemName() {
        return itemName;
    }

    public String getAddOn() {
        return addOn;
    }

    public String getPrice() {
        return price;
    }
}
