package com.martjinni.userApp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import java.util.List;

public class addOnMasterAdapter  extends RecyclerView.Adapter<addOnMasterAdapter.addOnMasterViewHolder>{

    Context mCtx;
    List<addOnMaster> addOnUseList;
    View mView;

    public addOnMasterAdapter(Context mCtx, List<addOnMaster> addOnUseList, View mView) {
        this.mCtx = mCtx;
        this.addOnUseList = addOnUseList;
        this.mView = mView;
    }

    @NonNull
    @Override
    public addOnMasterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from (mCtx);
        View view = inflater.inflate (R.layout.addonlist,null);
        return new addOnMasterViewHolder (view);
    }

    @Override
    public void onBindViewHolder(@NonNull addOnMasterViewHolder holder, int position) {
        addOnMaster useInformation = addOnUseList.get (position);
        String displayName = useInformation.getAddOnMasterName ()+"@ Rs. "+useInformation.getPrice ();
        holder.displayName.setText (displayName);
        addOnListAdapter adapter = new addOnListAdapter (mCtx,useInformation.getAddOnListUse (),mView);
        holder.printList.setHasFixedSize (false);
        holder.printList.setLayoutManager (new LinearLayoutManager (mCtx));
        holder.printList.setNestedScrollingEnabled(false);
        holder.printList.setAdapter (adapter);
    }

    @Override
    public int getItemCount() {
        return addOnUseList.size ();
    }

    class addOnMasterViewHolder extends RecyclerView.ViewHolder{
        TextView displayName;
        RecyclerView printList;
        public addOnMasterViewHolder(View itemView){
            super(itemView);
            displayName = itemView.findViewById (R.id.addOnCatName);
            printList = itemView.findViewById (R.id.displayListAddOn);
        }
    }
}
