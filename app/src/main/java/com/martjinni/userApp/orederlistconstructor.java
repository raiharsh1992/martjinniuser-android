package com.martjinni.userApp;

public class orederlistconstructor {
    private String Date, OrderStatus;
    private int orderId, shopCount, shopLeft;
    private String totalAmount;
    private int donedelivery;
    private int donepercent;

    public orederlistconstructor(String OrderStatus, String date, int orderId, int shopCount, int shopLeft, String totalAmount) {
        this.Date = date;
        this.orderId = orderId;
        this.shopCount = shopCount;
        this.shopLeft = shopLeft;
        this.totalAmount = totalAmount;
        this.OrderStatus = OrderStatus;

        if(this.shopCount>0){
            this.donedelivery = this.shopCount - this.shopLeft;
            this.donepercent =((this.donedelivery*100/this.shopCount));
        }
        else{
            this.donedelivery = 0;
            this.donepercent = 0;
        }
    }

    public String getDate() {
        String S = Date.substring(0, Math.min(Date.length(), 10));
        return S;
    }


    public int getOrderId() {
        return orderId;
    }


    public int getshopCount() {
        return shopCount;
    }

    public int getWorking() {
        return shopLeft;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public int getDonedelivery(){
        return donedelivery;
    }
    public int getDonepercent(){
        return donepercent;
    }


    public String getOrderStatus() {
        return OrderStatus;
    }
}
