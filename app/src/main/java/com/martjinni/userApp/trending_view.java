package com.martjinni.userApp;

import java.util.List;

public class trending_view {
    private String catName;
    private List<trend_card_view> useInfo;

    public trending_view(String catName, List<trend_card_view> useInfo) {
        this.catName = catName;
        this.useInfo = useInfo;
    }

    public String getCatName() {
        return catName;
    }

    public List<trend_card_view> getUseInfo() {
        return useInfo;
    }
}
