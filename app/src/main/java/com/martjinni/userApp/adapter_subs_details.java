package com.martjinni.userApp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import java.util.List;

public class adapter_subs_details extends RecyclerView.Adapter<adapter_subs_details.MyViewHolder> {

    private List<constructor_subs_details> list;
    public Context mCtx;

    public adapter_subs_details(List<constructor_subs_details> list, Context mCtx) {
        this.list = list;
        this.mCtx = mCtx;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.subsdetailsrecycler, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        constructor_subs_details cons = list.get(position);
        holder.status.setText( cons.getStatus());
        holder.day.setText(cons.getDay());
        holder.eta.setText(cons.getEta());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView day;
        TextView eta;
        TextView status;
        public MyViewHolder(View itemView) {
            super(itemView);

            day = itemView.findViewById(R.id.daycount);
            eta = itemView.findViewById(R.id.eta);
            status = itemView.findViewById(R.id.status);

        }
    }

}
