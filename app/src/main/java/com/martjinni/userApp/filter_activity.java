package com.martjinni.userApp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class filter_activity extends AppCompatActivity {
    public SharedPreferences sharedPreferences;
    List<filterRecyler> filterList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        filterList = new ArrayList<> ();
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_filter_activity);
        sharedPreferences = getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit ();
        editor.remove ("viewTypeFilter").apply ();
        editor.remove ("selctedFilter").apply ();
        String viewType = sharedPreferences.getString ("viewType","");
        if(!(viewType.equals ("trend")||viewType.equals ("emergency")||viewType.equals (""))){
            SQLiteDatabase myDatabase = openOrCreateDatabase("martjinni",MODE_PRIVATE,null);
            Cursor resultSet = myDatabase.rawQuery ("SELECT * from mjCat where viewType='"+viewType+"'", null);
            if(resultSet.isBeforeFirst ()){
                resultSet.moveToFirst ();
                while (!(resultSet.isAfterLast ())){
                    List<filterCheckBox> catList = new ArrayList<> ();
                    int mjId = resultSet.getInt (0);
                    String mjName = resultSet.getString (1);
                    int display = 0;
                    Cursor resultSetCat = myDatabase.rawQuery ("SELECT * from catList where mjId='"+mjId+"'and viewType='"+viewType+"'",null);
                    if(resultSetCat.isBeforeFirst ()){
                        resultSetCat.moveToFirst ();
                        while (!(resultSetCat.isAfterLast ())){
                            if(!(resultSetCat.getString (3).equals ("[]"))){
                                display++;
                                catList.add (new filterCheckBox (resultSetCat.getString (1),String.valueOf (resultSetCat.getString (0))));
                            }
                            resultSetCat.moveToNext ();
                        }
                    }
                    if(display>0){
                        filterList.add(new filterRecyler (mjName,catList));
                    }
                    resultSet.moveToNext ();
                }
                filterSetAdapter adapter = new filterSetAdapter (this,filterList);
                RecyclerView recyclerView = findViewById (R.id.filterList);
                recyclerView.setHasFixedSize (false);
                recyclerView.setLayoutManager (new LinearLayoutManager (this));
                recyclerView.setAdapter (adapter);
            }
            else{
                Toast.makeText (this, "Sorry no filters for the category", Toast.LENGTH_SHORT).show ();
                editor.remove ("viewTypeFilter").apply ();
                Intent intent = new Intent(this, homePage.class);
                startActivity(intent);
                finish ();
            }
        }
        else{
            editor.remove ("viewTypeFilter").apply ();
            Intent intent = new Intent(this, homePage.class);
            startActivity(intent);
            finish ();
        }
    }
    public void applyFilter(View view){
        sharedPreferences = getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        String viewType = sharedPreferences.getString ("viewType","");
        String selectedFilter = sharedPreferences.getString ("selctedFilter","");
        if(!(selectedFilter.equals (""))){
            String[] list = selectedFilter.split (",");
            SQLiteDatabase myDatabase = openOrCreateDatabase("martjinni",MODE_PRIVATE,null);
            for(int i = 0;i<list.length;i++){
                int info = Integer.parseInt (list[i]) ;
                Cursor resultSet = myDatabase.rawQuery ("SELECT * from catList where catId='"+info+"'and viewType='"+viewType+"'",null);
                if(resultSet.isBeforeFirst ()){
                    SharedPreferences.Editor editor = sharedPreferences.edit ();
                    resultSet.moveToFirst ();
                    String shopList = resultSet.getString (3);
                    if(shopList.equals ("[]")){
                        Toast.makeText (this, "No shop in category, please choose other", Toast.LENGTH_SHORT).show ();
                        editor.remove ("selctedFilter").apply ();
                        editor.remove ("viewTypeFilter").apply ();
                    }
                    else{
                        shopList = (resultSet.getString (3).substring (1,resultSet.getString (3).length ()-1));
                        if(i==0){
                            editor.putString ("activeList",shopList).apply ();
                        }
                        else{
                            String shopListUse=sharedPreferences.getString ("activeList","");
                            if(shopListUse.equals ("")){
                                editor.putString ("activeList",shopList).apply ();
                            }
                            else{
                                shopListUse = shopListUse+","+shopList;
                                editor.putString ("activeList",shopListUse).apply ();
                            }
                        }
                    }
                }
            }
            String shopListUse=sharedPreferences.getString ("activeList","");
            if(!(shopListUse.equals (""))){
                String[] finalList = shopListUse.split (",");
                String [] userFinal=new String [finalList.length];
                int place = 0;
                for(int l=0;l<finalList.length;l++){
                    if(!Arrays.asList (userFinal).contains (finalList[l])){
                        userFinal[place]=finalList[l];
                        place++;
                    }
                }
                String finalValue = "";
                for(int l = 0;l<userFinal.length;l++){
                    if(l==0){
                        finalValue = userFinal[l];
                    }
                    else{
                        finalValue = finalValue+","+userFinal[l];
                    }
                }
                SharedPreferences.Editor editor = sharedPreferences.edit ();
                editor.putString ("activeList",finalValue).apply ();
                editor.putString ("viewTypeFilter","true").apply ();
                Intent intent = new Intent(this, homePage.class);
                startActivity(intent);
                finish ();
            }
            else{
                Toast.makeText (this, "No shop in category, please choose other", Toast.LENGTH_SHORT).show ();
                SharedPreferences.Editor editor = sharedPreferences.edit ();
                editor.remove ("activeList").apply ();
                editor.remove ("viewTypeFilter").apply ();
            }

        }
        else{
            Toast.makeText (this, "No filter applied", Toast.LENGTH_SHORT).show ();
            SharedPreferences.Editor editor = sharedPreferences.edit ();
            editor.remove ("viewTypeFilter").apply ();
            editor.remove ("selctedFilter").apply ();
            Intent intent = new Intent(this, homePage.class);
            startActivity(intent);
            finish ();
        }
    }

    public void goBack(View view){
        sharedPreferences = getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit ();
        editor.remove ("viewTypeFilter").apply ();
        Intent intent = new Intent(this, homePage.class);
        startActivity(intent);
        finish ();
    }

    @Override
    public void onBackPressed(){
        sharedPreferences = getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit ();
        editor.remove ("viewTypeFilter").apply ();
        Intent intent = new Intent(this, homePage.class);
        startActivity(intent);
        finish ();
    }
}
